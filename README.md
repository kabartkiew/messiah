# **Application for modelling**

The aim of the application is to enable a user to create their own models and to apply them on their data.
One of the possible use is actuarial modelling.

Messiah currently works under Windows. It doesn't need installation, you can just copy the executable.
To use full functionality of the application you need to have g++ compiler installed on your computer.