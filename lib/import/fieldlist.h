#ifndef FIELDLIST_H
#define FIELDLIST_H

#ifndef INCLUDEDLL
    #include <string>
    #include <vector>
#endif

namespace messiah{

    struct FieldInfo
    {
        std::string Name;
        std::string Type;
        bool ToBeImported;
    };

    class FieldList
    {
    public:
        FieldList();
        ~FieldList();
        std::string Name;
        void AddNewField(std::string sName, std::string sType, bool bToBeImported);
        void RemoveField(int FieldNumber);
        int FieldCount();
        std::string FieldName(int FieldNumber);
        std::string FieldType(int FieldNumber);
        bool FieldToBeImported(int FieldNumber);
        void ChangeName(int FieldNumber, std::string NewName);
        void ChangeType(int FieldNumber, std::string NewType);
        void ChangeToBeImported(int FieldNumber, bool NewToBeImported);
        bool ImportAllFields();
    private:
        std::vector<FieldInfo> vFieldInfo;
    };

}

#endif // FIELDLIST_H
