#ifndef PROPHETDATA_H
#define PROPHETDATA_H

#include <istream>

#include "importdata.h"

namespace messiah{

    class ProphetData : public ImportType
    {
    public:
        ProphetData(ImportData* import_data);
        ~ProphetData();

        int GetFieldList();
        int ConvertData();

    protected:
        std::string GetHeader(std::istream& datafile);
    };

}
#endif // PROPHETDATA_H
