#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <QDir>

#include "importdata.h"
#include "model.h"
#include "createdll/cppdlldata.h"
#include "other/proccessdll.h"

messiah::ImportData::ImportData()
{
    pFieldList = new FieldList();
}

messiah::ImportData::~ImportData()
{
    delete pFieldList;
}

void messiah::ImportData::ChangeOutputPath(std::string newpath)
{
    output_path = newpath;
}

std::string messiah::ImportData::CreateStructure()
{
    std::string structureString;
     structureString = "struct " + pFieldList->Name + "{\n";

 //    bool importAllFields = true;
     for(int i = 0; i < pFieldList->FieldCount(); i++)
     {
         if(pFieldList->FieldToBeImported((i)))
               structureString += pFieldList->FieldType(i) + " " + pFieldList->FieldName(i) + ";\n";
 //        else
 //            importAllFields = false;
     }

    structureString += "};";

    return structureString;
}

messiah::ImportType::ImportType()
{

}

messiah::ImportType::~ImportType()
{

}

int messiah::ImportType::MakeOutDir()
{
    std::string outputdir = pImportData->output_path;
    const char* dir_path = outputdir.c_str();

    QDir dir(dir_path);
    if(!dir.exists())
    {
        dir.mkpath(".");
    }

    return 0;
}

int messiah::ImportType::CreateCppDllMain()
{
    messiah::DataStructure dataStruct(*pImportData->pFieldList);
    CppDllData importDataStruct(&dataStruct);

    std::string filename = pImportData->data_file_name;
    if(!filename.empty())
    {
        std::string::size_type p = filename.rfind('.');
        if(p!=std::string::npos)
            filename.replace(p+1,std::string::npos,"cpp");
    }
    std::ofstream cppfile(filename.c_str());

    cppfile << "#define DLL\n";

    cppfile << "#include <vector>\n";
    cppfile << "#include <ostream>\n";
    cppfile << "#include <windows.h>\n";

    cppfile << "#define POINT_STRUCTURE " << pImportData->pFieldList->Name << "\n";

    cppfile << importDataStruct.Includes;

    cppfile << pImportData->CreateStructure();

    cppfile << "\n";

    cppfile << "extern \"C\" __declspec(dllexport) policy* NewData();\n";

    cppfile << "extern \"C\" __declspec(dllexport) void FreeData(policy* instance);\n";

    cppfile << "extern \"C\" __declspec(dllexport) " << pImportData->pFieldList->Name << " punkty_modelu[];\n";

    cppfile << "extern \"C\" __declspec(dllexport) long punkty_liczba_polis;\n";

    cppfile << "extern \"C\" __declspec(dllexport) messiah::DataStructure* NewDataStructure();\n";

    cppfile << "extern \"C\" __declspec(dllexport) void FreeDataStructure(messiah::DataStructure* instance);\n";

    cppfile << "BOOL APIENTRY DllMain( HINSTANCE hDLL, DWORD dwReason, LPVOID lpReserved)\n"
            << "{ return TRUE;}\n";

    cppfile << "#pragma warning(push)\n"
            << "#pragma warning(once:4305)\n"
            << "#include \"" << pImportData->data_file_name << "\"\n"
            << "#pragma warning(pop)\n";

    cppfile << "policy* NewData()\n"
            << "{\n"
            << "static points<" << pImportData->pFieldList->Name << ">* new_data;\n"
            << "if(!new_data)\n"
            << "new_data = new points<" << pImportData->pFieldList->Name << ">;\n"
            << "return new_data;\n"
            << "\}\n";

    cppfile << "void FreeData(policy* instance)\n"
            << "{ delete instance; }\n";

    cppfile << "long punkty_liczba_polis = sizeof(punkty_modelu)/sizeof(punkty_modelu[0]);\n";

    cppfile << importDataStruct.ExportFunctions;

    cppfile.close();

    return 0;
}

int messiah::ImportType::DoImport(bool DeleteTempFiles)
{
    MakeOutDir();
    ConvertData();
    CreateCppDllMain();
    //add check if cpp was created !!
    if(CreateDll(pImportData->data_file_name, DeleteTempFiles))
        return 1;
    else
        return 0;
}

messiah::FieldList* messiah::NewFieldList(std::string line)
{
FieldList* tempFieldList;

    if(line.empty()) return 0; //header line not found...

    //add a check if there is enough memory
    tempFieldList = new FieldList();

    std::string::size_type p, od = 0;
    do
    {
        p = line.find(',',od);

        tempFieldList->AddNewField(line.substr(od,p-od), "float", true);

        od = p + 1;
    }while(p != std::string::npos);

    return tempFieldList;
}

std::string& messiah::CutDataLine(std::string& line, FieldList *fields)
{
    if(fields)
    {
        std::string::size_type p = 0, q;

        for(int i = 0; i < fields->FieldCount(); i++)
        {
            q = line.find(',', p);

            if(q!=std::string::npos)
                q++;

            if(!fields->FieldToBeImported(i))
            {
                line.erase(p,q-p);
            }
            else
                p = q;

        }
        if(!line.empty() && line[line.length()-1]==',')
            line.erase(line.length()-1, std::string::npos);
    }

    return line;
}
