#ifndef IMPORTDATA_H
#define IMPORTDATA_H

#ifndef INCLUDEDLL
    #include <string>
    #include <vector>
#endif

#include "fieldlist.h"

namespace messiah{

    class ImportData
    {
    public:
        ImportData();
        ~ImportData();

        std::string input_path;
        std::string output_path;
        std::string data_file_name;

        void ChangeOutputPath(std::string newpath);

        FieldList *pFieldList;
        std::string CreateStructure();
    };

    class ImportType{
    public:
        ImportType();
        ~ImportType();
        virtual int GetFieldList() = 0;
        virtual int ConvertData() = 0;
        int MakeOutDir();
        int CreateCppDllMain();
        int DoImport(bool DeleteTempFiles = true);
        ImportData *pImportData;
    };

    FieldList* NewFieldList(std::string line);
    std::string& CutDataLine(std::string& line, FieldList *fields);

}

#endif // IMPORTDATA_H

