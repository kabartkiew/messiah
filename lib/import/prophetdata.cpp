#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;
using std::ofstream;

#include "prophetdata.h"


messiah::ProphetData::ProphetData(ImportData *import_data)
{
    pImportData = import_data;
}

messiah::ProphetData::~ProphetData()
{

}

string messiah::ProphetData::GetHeader(istream &datafile)
{
    //sprawdzić czy datafile jest w ogóle otwarty!!!

    string line;

    char c = 0;
    do{
        if(!datafile.eof())
        {
            getline(datafile,line);
            if(line.length()>0) c = line[0];
        }
        else
        {
            //missing the line starting with '!'
            return "";
        }
    }while(c!='!');

    return line;
}


int messiah::ProphetData::GetFieldList()
{
//dodać sprawdzenie poprawności ścieżki

    ifstream inputfile(pImportData->input_path.c_str());

    string line = GetHeader(inputfile);

    inputfile.close();

    if(line.empty()) return 1; //header line not found...

    string::size_type p = line.find(',');

    if(p != string::npos)
    {
        line = line.substr(p+1);
    }
    else
    {
        //missing ',' in the header line
        return 1;
    }

    pImportData->pFieldList = NewFieldList(line);

    return 0;
}

int messiah::ProphetData::ConvertData()
{
//dodać sprawdzenie poprawności ścieżek i czy pliki się utworzyły - różne wartości żeby zwracała wtedy
//zmienić nazwę punkty_modelu!

    if(!pImportData->pFieldList)
        GetFieldList();

    if(!pImportData->pFieldList)
        return 1;   //header line not found...

    ifstream inputfile(pImportData->input_path.c_str());

//    pImportData->CreateStructureFile();

//    string product = pImportData->structure_name;
    string product = pImportData->pFieldList->Name;

    string punkt_h = pImportData->data_file_name;
    ofstream punkt_j(punkt_h.c_str());

    string line = GetHeader(inputfile);

    string::size_type p;
    char c = '*';
    bool first_line = true;
    while(!inputfile.eof() && c=='*')
    {
        getline(inputfile, line);
        if(line.length()>0) c = line[0]; else c = 0;
        if(c == '*')                    //dodać sprawdzenie, czy liczba pól się zgadza
        {
            if(!first_line)
            {
                punkt_j << ",";
            }
            else
            {
                punkt_j << product << " punkty_modelu[]={";
                first_line = false;
            }

            p = line.find(',');

            if(p != string::npos)
            {
                    line = line.substr(p+1);
            }
            else
            {
                punkt_j.close();
                inputfile.close();
                //missing ',' in the data line
                return 3;
            }

            if(pImportData->pFieldList->ImportAllFields())
                punkt_j << line;
            else
                punkt_j << CutDataLine(line, pImportData->pFieldList);

        }
    }

    if(first_line)
    {
        punkt_j.close();
        inputfile.close();
        //only header line
        return 4;
    }

    punkt_j << "};";

    inputfile.close();

    punkt_j.close();

    return 0;
}
