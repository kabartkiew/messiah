#ifndef CSVDATA_H
#define CSVDATA_H

#include <istream>

#include "importdata.h"

namespace messiah{

    class CsvData : public ImportType
    {
    public:
        CsvData(ImportData* import_data);
        ~CsvData();

        int GetFieldList();
        int ConvertData();
    protected:
        std::string GetHeader(std::istream& datafile);
    };

}
#endif // CSVDATA_H
