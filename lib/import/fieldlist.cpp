#include "fieldlist.h"

messiah::FieldList::FieldList()
{

}

messiah::FieldList::~FieldList()
{
}

void messiah::FieldList::AddNewField(std::string sName, std::string sType, bool bToBeImported)
{
    FieldInfo newfield = FieldInfo();
    newfield.Name = sName;
    newfield.Type = sType;
    newfield.ToBeImported = bToBeImported;
    vFieldInfo.push_back(newfield);
}

void messiah::FieldList::RemoveField(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        vFieldInfo.erase(vFieldInfo.begin() + FieldNumber);
}

int messiah::FieldList::FieldCount()
{
    return vFieldInfo.size();
}

std::string messiah::FieldList::FieldName(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].Name;
}

std::string messiah::FieldList::FieldType(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].Type;
}

bool messiah::FieldList::FieldToBeImported(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].ToBeImported;
}

void messiah::FieldList::ChangeName(int FieldNumber, std::string NewName)
{
    vFieldInfo[FieldNumber].Name = NewName;
}

void messiah::FieldList::ChangeType(int FieldNumber, std::string NewType)
{
    vFieldInfo[FieldNumber].Type = NewType;
}

void messiah::FieldList::ChangeToBeImported(int FieldNumber, bool NewToBeImported)
{
    vFieldInfo[FieldNumber].ToBeImported = NewToBeImported;
}

bool messiah::FieldList::ImportAllFields()
{
    bool All = true;
    for(int i = 0; i < vFieldInfo.size(); i++)
    {
        if(!vFieldInfo[i].ToBeImported) All = false;
    }
    return All;
}

