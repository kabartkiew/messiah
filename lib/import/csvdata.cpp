#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include "csvdata.h"

using namespace std;
using std::ofstream;


messiah::CsvData::CsvData(ImportData* import_data)
{
    pImportData = import_data;
}

messiah::CsvData::~CsvData()
{

}

std::string messiah::CsvData::GetHeader(istream &datafile)
{
    //sprawdzić czy datafile jest w ogóle otwarty!!!

    std::string line;

    if(!datafile.eof())
        getline(datafile,line);
    else
        //empty file...
        return "";

    return line;
}

int messiah::CsvData::GetFieldList()
{
//dodać sprawdzenie poprawności ścieżki

    ifstream inputfile(pImportData->input_path.c_str());

    string line = GetHeader(inputfile);

    inputfile.close();

    if(line.empty()) return 1; //header line not found...

    //int count_fields = std::count(line.begin(), line.end(), ',') + 1;

    //dodać sprawdzenie czy nie zabraknie pamięci
    pImportData->pFieldList = new FieldList();

    //FieldInfo *fields = pImportData->pFieldList->pFieldInfo;
    string::size_type p, od = 0;
    do
    {
        p = line.find(',',od);

        pImportData->pFieldList->AddNewField(line.substr(od,p-od), "float", true);

        od = p + 1;
    }while(p != string::npos);

    return 0;
}

int messiah::CsvData::ConvertData()
{
//add a check if the paths are correct and if the files was created

    if(!pImportData->pFieldList)
        GetFieldList();

    if(!pImportData->pFieldList)
        return 1;   //header line not found...

    ifstream inputfile(pImportData->input_path.c_str());

//    string product = pImportData->structure_name;
    string product = pImportData->pFieldList->Name;

    string punkt_h = pImportData->data_file_name;
    ofstream punkt_j(punkt_h.c_str());

//    punkt_j << pImportData->CreateStructure() << "\n";

    string line = GetHeader(inputfile);

    bool first_line = true;
    while(!inputfile.eof())
    {
        getline(inputfile, line);

        //add a check if the number of fields is correct
        if(!first_line)
        {
            if(!line.empty())
                punkt_j << ",";
        }
        else
        {
            punkt_j << product << " punkty_modelu[]={";
            first_line = false;
        }

        if(pImportData->pFieldList->ImportAllFields())
            punkt_j << line;
        else
            punkt_j << CutDataLine(line, pImportData->pFieldList);
    }

    if(first_line)
    {
        punkt_j.close();
        inputfile.close();
        return 4; //only header line
    }

    punkt_j << "};";

    inputfile.close();

    punkt_j.close();

    return 0;
}
