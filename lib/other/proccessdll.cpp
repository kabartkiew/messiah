#include "proccessdll.h"

int CreateDll(std::string cppfilename, bool deletetemp)
{
    int failed;
    std::string command;

    if(!cppfilename.empty())
    {
        std::string::size_type p = cppfilename.rfind('.');
        if(p!=std::string::npos)
            cppfilename.erase(p,std::string::npos);

        command = "g++ -c \"";
        command.append(cppfilename);
        command.append(".cpp\" -o \"");
        command.append(cppfilename);
        command.append(".o\"");

        if(!system(command.c_str()))
        {   
            command = "g++ -shared -o \"";
            command.append(cppfilename);
            command.append(".dll\" \"");
            command.append(cppfilename);
            command.append(".o\"");
            failed = system(command.c_str());
        }
        else
            failed = 1;
    }
    else
        failed = 1;

    if(deletetemp)
    {
        command = "rm " + cppfilename + ".cpp " + cppfilename + ".h " + cppfilename + ".o";
        system(command.c_str());
    }

    return failed;
}

