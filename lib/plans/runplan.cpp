#include <string>

#include "runplan.h"

messiah::RunInfo::RunInfo()
{

}

messiah::RunInfo::RunInfo(size_t Size) : ToBeRun(false)
{
    std::vector<std::string> EmptyVec(Size, "");
    Value = EmptyVec;
}

messiah::RunInfo::~RunInfo()
{

}

messiah::RunPlan::RunPlan()
{

}

messiah::RunPlan::~RunPlan()
{

}

void messiah::RunPlan::AddAttribute(std::string attribute)
{
    for(size_t i = 0; i < AttributeList.size(); i++)
    {
        if(AttributeList[i] == attribute)
            return;
    }
    AttributeList.push_back(attribute);
    for(size_t i = 0; i < RunList.size(); i++)
    {
        RunList[i].Value.push_back("");
    }
}

void messiah::RunPlan::AddNewRun()
{
    RunInfo newrun(AttributeList.size());
    RunList.push_back(newrun);
}

void messiah::RunPlan::RemoveRun(int RunNumber)
{
    if(RunNumber >= 0 && RunNumber < RunList.size())
        RunList.erase(RunList.begin() + RunNumber);
}

void messiah::RunPlan::PutToRunInfo(std::string Info, int RunNumber, int AttributeNumber)
{
    if(RunNumber < RunList.size())
        if(AttributeNumber < AttributeList.size())
            RunList[RunNumber].Value[AttributeNumber] = Info;
}

void messiah::RunPlan::PutToRunInfo(std::string Info, int RunNumber, std::string attribute)
{
    if(RunNumber < RunList.size())
        for(size_t i = 0; i < AttributeList.size(); i++)
            if(AttributeList[i] == attribute)
                RunList[RunNumber].Value[i] = Info;
}

void messiah::RunPlan::MarkRunInfo(bool YesNo, int RunNumber)
{
    if(RunNumber < RunList.size())
        RunList[RunNumber].ToBeRun = YesNo;
}

int messiah::RunPlan::AttributeCount()
{
    return AttributeList.size();
}

int messiah::RunPlan::RunCount()
{
    return RunList.size();
}

std::string messiah::RunPlan::Attribute(int AttributeNumber)
{
    if(AttributeNumber < AttributeList.size())
        return AttributeList[AttributeNumber];
    else
        return "";
}

std::string messiah::RunPlan::InfoValue(int RunNumber, int AttributeNumber)
{
    return (RunList[RunNumber]).Value[AttributeNumber];
}

bool messiah::RunPlan::InfoRunMarked(int RunNumber)
{
    return (RunList[RunNumber]).ToBeRun;
}

int messiah::RunPlan::AttributeNo(std::string AttributeName)
{
    for(size_t i = 0; i < AttributeList.size(); i++)
    {
        if(AttributeList[i] == AttributeName)
            return i;
    }
    return -1;
}
