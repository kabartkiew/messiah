#ifndef RUNPLAN_H
#define RUNPLAN_H

#include <string>
#include <vector>

namespace messiah{

    class RunInfo
    {
    public:
        RunInfo();
        RunInfo(size_t Size);
        ~RunInfo();
        std::vector<std::string> Value;
        bool ToBeRun;
    };

    class RunPlan
    {
    public:
        RunPlan();
        ~RunPlan();
        void AddAttribute(std::string attribute);
        void AddNewRun();
        void RemoveRun(int RunNumber);
        void PutToRunInfo(std::string Info, int RunNumber, int AttributeNumber);
        void PutToRunInfo(std::string Info, int RunNumber, std::string attribute);
        void MarkRunInfo(bool YesNo, int RunNumber);
        int AttributeCount();
        int RunCount();
        std::string Attribute(int AttributeNumnber);
        std::string InfoValue(int RunNumber, int AttributeNumber);
        bool InfoRunMarked(int RunNumber);
        int AttributeNo(std::string AttributeName);
    private:
        std::vector<std::string> AttributeList;
        std::vector<RunInfo> RunList;
    };

}

#endif // RUNPLAN_H
