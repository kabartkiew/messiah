#include <string>

#include "executeplan.h"
#include "runplan.h"

messiah::ExecuteInfo::ExecuteInfo(std::string run_name) : Name(run_name), Progress(0), Finished(false)
{

}

messiah::ExecuteInfo::~ExecuteInfo()
{

}

messiah::ExecutePlan::ExecutePlan()
{

}

messiah::ExecutePlan::ExecutePlan(messiah::RunPlan *pRunPlan)
{
    for(int i =0; i<pRunPlan->RunCount(); i++)
    {
        if(pRunPlan->InfoRunMarked(i))
        {
            AddNewRun(pRunPlan->InfoValue(i, 0)); //tutaj korzystam z tego, że name jest zawsze pierwszym atrybutem,
                                                  //ale trzeba uwzględnić, że nie zawsze musi tak być!!!
        }
    }
}

messiah::ExecutePlan::~ExecutePlan()
{

}

void messiah::ExecutePlan::AddNewRun(std::string run_name)
{
    ExecuteInfo newrun(run_name);
    ExecuteList.push_back(newrun);
}

int messiah::ExecutePlan::RunCount()
{
    return ExecuteList.size();
}

std::string messiah::ExecutePlan::Name(int ExecuteNumber)
{
    if(ExecuteNumber < ExecuteList.size())
        return ExecuteList[ExecuteNumber].Name;
}

char messiah::ExecutePlan::Progress(int ExecuteNumber)
{
    if(ExecuteNumber < ExecuteList.size())
        return ExecuteList[ExecuteNumber].Progress;
}

void messiah::ExecutePlan::UpdateProgress(int ExecuteNumber, char Progress)
{
    if(Progress < 0 )
        Progress = 0;
    if(Progress > 100)
        Progress = 100;
    ExecuteList[ExecuteNumber].Progress = Progress;
    if(Progress == 100)
        ExecuteList[ExecuteNumber].Finished = true;
}

bool messiah::ExecutePlan::Finished(int ExecuteNumber)
{
    if(ExecuteNumber < ExecuteList.size())
        return ExecuteList[ExecuteNumber].Finished;
}
