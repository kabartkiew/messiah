#ifndef EXECUTEPLAN_H
#define EXECUTEPLAN_H

#include <string>
#include <vector>

#include "runplan.h"

namespace messiah{

    class ExecuteInfo
    {
    public:
        ExecuteInfo(std::string run_name);
        ~ExecuteInfo();
        std::string Name;
        char Progress;
        bool Finished;
    };

    class ExecutePlan
    {
    public:
        ExecutePlan();
        ExecutePlan(messiah::RunPlan *pRunPlan);
        ~ExecutePlan();
        void AddNewRun(std::string run_name);
        int RunCount();
        std::string Name(int ExecuteNumber);
        char Progress(int ExecuteNumber);
        bool Finished(int ExecuteNumber);
        void UpdateProgress(int ExecuteNumber, char Progress);
    private:
        std::vector<ExecuteInfo> ExecuteList;
    };

}

#endif // EXECUTEPLAN_H
