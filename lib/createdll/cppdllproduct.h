#ifndef CPPDLLPRODUCT_H
#define CPPDLLPRODUCT_H

#include <string>

#include "model.h"

class CppDllProduct
{
public:
    CppDllProduct(messiah::Model *model);

    std::string TextStream();

private:
    void IncludesStream();
    void DataStructuresStream();
    void CommonStream();
    void BaseHeadersStream();
    void BibHeadersStream();
    void BaseFunctionsStream();
    void ProjTermFunctionsStream();
    void MainStream();
    void ExportFunctionsStream();

    std::string Includes;
    std::string DataStructures;
    std::string Common;
    std::string BaseHeaders;
    std::string BibHeaders;
    std::string BaseFunctions;
    std::string ProjTermFunctions;
    std::string Main;
    std::string ExportFunctions;

    messiah::Model* pModel;
};

#endif // CPPDLLPRODUCT_H
