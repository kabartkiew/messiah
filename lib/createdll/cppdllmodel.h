#ifndef CPPDLLMODEL_H
#define CPPDLLMODEL_H

#include <string>
#include "model.h"

class CppDllModel
{
public:
    CppDllModel(messiah::Model* model);

    std::string TextStream();

private:
    void IncludesStream();
    void MainStream();
    void DataStructuresStream();
    void ValuesStream();
    void FunctionsStream();
    void FunctionsTStream();
    void SettingStream();
    void ExportFunctionsStream();

    std::string Includes;
    std::string Main;
    std::string DataStructures;
    std::string Values;
    std::string Functions;
    std::string FunctionsT;
    std::string Setting;
    std::string ExportFunctions;

    messiah::Model* pModel;
};

#endif // CPPDLLMODEL_H
