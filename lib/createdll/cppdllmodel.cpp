#include <string>
#include <fstream>
#include <streambuf>

#include "cppdllmodel.h"
#include "model.h"

CppDllModel::CppDllModel(messiah::Model* model) : pModel(model)
{
    IncludesStream();
    MainStream();
    DataStructuresStream();
    ValuesStream();
    FunctionsStream();
    FunctionsTStream();
    SettingStream();
    ExportFunctionsStream();
}

std::string CppDllModel::TextStream()
{
    std::string Text;

    Text += Includes;
    Text += "\n";

    Text += Main;
    Text += "\n";

    Text += DataStructures + "\n";
    Text += "\n";

    Text += Values +"\n";
    Text += "\n";

    Text += Functions +"\n";
    Text += "\n";

    Text += FunctionsT +"\n";
    Text += "\n";

    Text += Setting +"\n";
    Text += "\n";

    Text += ExportFunctions + "\n";
    Text += "\n";

    return Text;
}

void CppDllModel::IncludesStream()
{
    Includes.clear();

    Includes += "#include <windows.h>\n";
    Includes += "#include <string>\n";
    Includes += "#include <vector>\n";

    #include "append/txt/includemodel.txt"

}

void CppDllModel::MainStream()
{
    Main.clear();

    Main += "extern \"C\" __declspec(dllexport) messiah::Model* NewModel();\n";
    Main += "extern \"C\" __declspec(dllexport) void FreeModel(messiah::Model* instance);\n";
    Main += "BOOL APIENTRY DllMain( HINSTANCE hDLL, DWORD dwReason, LPVOID lpReserved)\n";
    Main += "{ return TRUE;}\n";
}

void CppDllModel::DataStructuresStream()
{
    DataStructures.clear();

    for(int i=0; i < pModel->Data.Count(); i++)
    {
        DataStructures += "const messiah::Variable ";
        DataStructures += pModel->Data.StructName(i).c_str();
        DataStructures += "[] = {\n";

        for(int j=0; j < pModel->Data.Structure(i)->Count(); j++)
        {
            DataStructures += "\"";
            DataStructures += pModel->Data.Structure(i)->VarName(j).c_str();
            DataStructures += "\", \"";
            DataStructures += pModel->Data.Structure(i)->VarType(j).c_str();
            DataStructures += "\"";
            if(j != pModel->Data.Structure(i)->Count()-1)
                DataStructures += ",\n";
            else
                DataStructures += "};\n";
        }
    }
}

void CppDllModel::ValuesStream()
{
    Values.clear();

    for(int i=0; i < pModel->ValueSet.Count(); i++)
    {
        Values += "const messiah::Value ";
        Values += pModel->ValueSet.Name(i).c_str();
        Values += " = {\"" + pModel->ValueSet.Name(i) + "\", \"" + pModel->ValueSet.Type(i) + "\", ";
        Values += "\"" + pModel->ValueSet.Text(i) + "\"};\n";
    }
}

void CppDllModel::FunctionsStream()
{
    Functions.clear();

    for(int i=0; i < pModel->FunctionSet.Count(); i++)
    {
        Functions += "const messiah::Function ";
        Functions += pModel->FunctionSet.FunctionName(i).c_str();
        Functions += " = {\"" + pModel->FunctionSet.FunctionName(i) + "\", \"" + pModel->FunctionSet.FunctionType(i) + "\", ";
        Functions += "\"" + pModel->FunctionSet.FunctionText(i) + "\", ";
        Functions += "\"" + pModel->FunctionSet.FunctionResultType(i) + "\"";
        Functions += "};\n";
    }
}

void CppDllModel::FunctionsTStream()
{
    FunctionsT.clear();

    for(int i=0; i < pModel->FunctionTSet.Count(); i++)
    {
        FunctionsT += "const messiah::FunctionT ";
        FunctionsT += pModel->FunctionTSet.FunctionTName(i).c_str();
        FunctionsT += " = {\"" + pModel->FunctionTSet.FunctionTName(i) + "\", \"" + pModel->FunctionTSet.FunctionTType(i) + "\", ";
        FunctionsT += "\"" + pModel->FunctionTSet.FunctionTText(i) + "\", ";
        FunctionsT += "\"" + pModel->FunctionTSet.FunctionTResultType(i) + "\"";
        FunctionsT += "};\n";
    }
}

void CppDllModel::SettingStream()
{
    Setting.clear();

}

void CppDllModel::ExportFunctionsStream()
{
    ExportFunctions.clear();

    ExportFunctions += "messiah::Model* NewModel()\n";
    ExportFunctions += "{\n";
    ExportFunctions += "messiah::Model* new_model = new messiah::Model;\n";

    for(int i=0; i < pModel->Data.Count(); i++)
    {
        std::string structname = pModel->Data.StructName(i).c_str();
        ExportFunctions += "new_model->AddData(messiah::DataStructure(\"" + structname + "\", ";
        ExportFunctions += structname + ", sizeof(" + structname + ")/sizeof(messiah::Variable)));\n";
    }

    for(int i=0; i < pModel->ValueSet.Count(); i++)
    {
        ExportFunctions += "new_model->AddValue(" + pModel->ValueSet.Name(i) + ");\n";
    }

    for(int i=0; i < pModel->FunctionSet.Count(); i++)
    {
        ExportFunctions += "new_model->AddFunction(" + pModel->FunctionSet.FunctionName(i) + ");\n";
    }

    for(int i=0; i < pModel->FunctionTSet.Count(); i++)
    {
        ExportFunctions += "new_model->AddFunctionT(" + pModel->FunctionTSet.FunctionTName(i) + ");\n";
    }

    //in future move definitions of the setting functions into SettingStream part        
    ExportFunctions += "new_model->setDurationStartMonth = \"" + pModel->setDurationStartMonth +"\";\n";
    ExportFunctions += "new_model->setProjTermFromZeroMonth = \"" + pModel->setProjTermFromZeroMonth +"\";\n";
    //

    ExportFunctions += "return new_model;\n";
    ExportFunctions += "}\n";

    ExportFunctions += "void FreeModel(messiah::Model* instance)\n";
    ExportFunctions += "{ delete instance; }\n";
}
