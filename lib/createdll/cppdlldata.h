#ifndef CPPDLLDATA_H
#define CPPDLLDATA_H

#include "model.h"

class CppDllData
{
public:
    CppDllData(messiah::DataStructure* datastruct);

    std::string Includes;
    std::string ExportFunctions;

private:
    void StructureStream();
    void IncludesStream();
    void ExportFunctionsStream();

    std::string Main;
    std::string Structure;


    messiah::DataStructure* pDataStruct;
};

#endif // CPPDLLDATA_H
