#include <string>
#include <fstream>
#include <streambuf>

#include "cppdlldata.h"

CppDllData::CppDllData(messiah::DataStructure *datastruct) : pDataStruct(datastruct)
{
    IncludesStream();
    StructureStream();
    ExportFunctionsStream();
}

void CppDllData::IncludesStream()
{
    Includes.clear();

    Includes += "#include <string>\n";
    Includes += "#include <vector>\n";

    #include "append/txt/includemodel.txt"
    #include "append/txt/includeimportdata.txt"
}

void CppDllData::StructureStream()
{
    Structure.clear();

    Structure += "const messiah::Variable ";
    Structure += pDataStruct->Name;
    Structure += "[] = {\n";

    for(int j=0; j < pDataStruct->Count(); j++)
    {
        Structure += "\"";
        Structure += pDataStruct->VarName(j);
        Structure += "\", \"";
        Structure += pDataStruct->VarType(j);
        Structure += "\"";
        if(j != pDataStruct->Count()-1)
            Structure += ",\n";
        else
            Structure += "};\n";
    }
}

void CppDllData::ExportFunctionsStream()
{
    ExportFunctions.clear();

    ExportFunctions += "messiah::DataStructure* NewDataStructure()\n";
    ExportFunctions += "{\n";
    ExportFunctions += Structure;
    ExportFunctions += "messiah::DataStructure* new_datastruct = new messiah::DataStructure(";

    std::string structname = pDataStruct->Name;
    ExportFunctions += "\"" + structname + "\"," + structname;
    ExportFunctions += ",sizeof(" + structname + ")/sizeof(messiah::Variable));\n";

    ExportFunctions += "return new_datastruct;\n";
    ExportFunctions += "}\n";

    ExportFunctions += "void FreeDataStructure(messiah::DataStructure* instance)\n";
    ExportFunctions += "{ delete instance; }\n";
}
