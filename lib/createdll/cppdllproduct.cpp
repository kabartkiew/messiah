#include "cppdllproduct.h"

CppDllProduct::CppDllProduct(messiah::Model *model) : pModel(model)
{
    IncludesStream();
    DataStructuresStream();
    CommonStream();
    BaseHeadersStream();
    BibHeadersStream();
    BaseFunctionsStream();
    ProjTermFunctionsStream();
    MainStream();
    ExportFunctionsStream();
}

std::string CppDllProduct::TextStream()
{
    std::string Text;

    Text += Includes;
    Text += "\n";

    Text += DataStructures + "\n";
    Text += "\n";

    Text += Common +"\n";
    Text += "\n";

    Text += BaseHeaders +"\n";
    Text += "\n";

    Text += BibHeaders +"\n";
    Text += "\n";

    Text += BaseFunctions +"\n";
    Text += "\n";

    Text += ProjTermFunctions +"\n";
    Text += "\n";

    Text += Main +"\n";
    Text += "\n";

    Text += ExportFunctions + "\n";
    Text += "\n";

    return Text;
}

void CppDllProduct::IncludesStream()
{
    Includes.clear();

    Includes.append("#define PRODUCTDLL\n");
    Includes.append("#include <ostream>\n");
    Includes.append("#include <windows.h>\n");
    Includes.append("#include <math.h>\n");

    #include "append/txt/includelibrary1.txt"

    Includes.append("extern \"C\" __declspec(dllexport) base_product* NewProduct();\n");
    Includes.append("extern \"C\" __declspec(dllexport) void FreeProduct(base_product* instance);\n");

    #include "append/txt/includelibrary2.txt"
}

void CppDllProduct::DataStructuresStream()
{
    DataStructures.clear();

    if(pModel->Data.Count()>0)
    {
        int i = 0;
        DataStructures.append("struct ");
        DataStructures.append(pModel->Data.StructName(i).c_str());
        DataStructures.append("{\n");

        for(int j=0; j < pModel->Data.Structure(i)->Count(); j++)
        {
            DataStructures.append(pModel->Data.Structure(i)->VarType(j).c_str());
            DataStructures.append(" ");
            DataStructures.append(pModel->Data.Structure(i)->VarName(j).c_str());
            DataStructures.append(";\n");
        }
        DataStructures.append("};\n");

        //only the first data structure is considered
        DataStructures.append("#define POINT_STRUCTURE ");
        DataStructures.append(pModel->Data.Structure(0)->Name.c_str());
        DataStructures.append("\n");
    }
}

void CppDllProduct::CommonStream()
{
    Common.clear();

    Common.append("#define DATA ((points<POINT_STRUCTURE>*)(NewProduct()->p_data))->p_point\n");
    Common.append("#define PROJ_SETTING NewProduct()->p_settings\n");
}

void CppDllProduct::BaseHeadersStream()
{
    BaseHeaders.clear();

    BaseHeaders.append("namespace library{\n\n");

    for(int i = 0; i < pModel->ValueSet.Count(); i++)
    {
        BaseHeaders += pModel->ValueSet.Type(i);
        BaseHeaders += " base_";
        BaseHeaders += pModel->ValueSet.Name(i);
        BaseHeaders += " = ";
        BaseHeaders += pModel->ValueSet.Text(i);
        BaseHeaders += ";\n";
    }
    BaseHeaders += "\n";

    for(int i = 0; i < pModel->FunctionSet.Count(); i++)
    {
        BaseHeaders += pModel->FunctionSet.FunctionType(i);
        BaseHeaders += " base_";
        BaseHeaders += pModel->FunctionSet.FunctionName(i);
        BaseHeaders += "();\n";
    }
    BaseHeaders += "\n";

    for(int i = 0; i < pModel->FunctionTSet.Count(); i++)
    {
        BaseHeaders += pModel->FunctionTSet.FunctionTType(i);
        BaseHeaders += " base_";
        BaseHeaders += pModel->FunctionTSet.FunctionTName(i);
        BaseHeaders += "(int);\n";
    }
    BaseHeaders += "\n";

    BaseHeaders.append("}\n");
}

void CppDllProduct::BibHeadersStream()
{
    BibHeaders.clear();

    BibHeaders += "namespace library{\n\n";

    for(int i = 0; i < pModel->ValueSet.Count(); i++)
    {
        BibHeaders += "value<";
        BibHeaders += pModel->ValueSet.Type(i);
        BibHeaders += "> ";
        BibHeaders += pModel->ValueSet.Name(i);
        BibHeaders += "(&base_";
        BibHeaders += pModel->ValueSet.Name(i);
        BibHeaders += ");\n";
    }
    BibHeaders += "\n";

    for(int i = 0; i < pModel->FunctionSet.Count(); i++)
    {
        BibHeaders += "function<";
        BibHeaders += pModel->FunctionSet.FunctionType(i);
        BibHeaders += "> ";
        BibHeaders += pModel->FunctionSet.FunctionName(i);
        BibHeaders += "(&base_";
        BibHeaders += pModel->FunctionSet.FunctionName(i);
        BibHeaders += ");\n";
    }
    BibHeaders += "\n";

    for(int i = 0; i < pModel->FunctionTSet.Count(); i++)
    {
        BibHeaders += "function_t<";
        BibHeaders += pModel->FunctionTSet.FunctionTType(i);
        BibHeaders += "> ";
        BibHeaders += pModel->FunctionTSet.FunctionTName(i);
        BibHeaders += "(&base_";
        BibHeaders += pModel->FunctionTSet.FunctionTName(i);
        BibHeaders += ");\n";
    }
    BibHeaders += "\n}\n";
}

void CppDllProduct::BaseFunctionsStream()
{
    BaseFunctions.clear();

    BaseFunctions.append("namespace library{\n\n");

    for(int i = 0; i < pModel->FunctionSet.Count(); i++)
    {
        BaseFunctions += pModel->FunctionSet.FunctionType(i);
        BaseFunctions += " base_";
        BaseFunctions += pModel->FunctionSet.FunctionName(i);
        BaseFunctions += "()\n{\n";
        BaseFunctions += pModel->FunctionSet.FunctionText(i);
        BaseFunctions += "\n}\n\n";
    }

    for(int i = 0; i < pModel->FunctionTSet.Count(); i++)
    {
        BaseFunctions += pModel->FunctionTSet.FunctionTType(i);
        BaseFunctions += " base_";
        BaseFunctions += pModel->FunctionTSet.FunctionTName(i);
        BaseFunctions += "(int t)\n{\n";
        BaseFunctions += pModel->FunctionTSet.FunctionTText(i);
        BaseFunctions += "\n}\n\n";
    }

    BaseFunctions.append("}\n");
}

void CppDllProduct::ProjTermFunctionsStream()
{
    ProjTermFunctions.clear();

    ProjTermFunctions += "int product::set_duration_start_month(){ ";
    ProjTermFunctions += pModel->setDurationStartMonth;
    ProjTermFunctions += " }\n";

    ProjTermFunctions += "int product::set_proj_term_from_zero_month(){";
    ProjTermFunctions += pModel->setProjTermFromZeroMonth;
    ProjTermFunctions += " }\n";
}

void CppDllProduct::MainStream()
{
    Main.clear();

    Main.append("BOOL APIENTRY DllMain( HINSTANCE hDLL, DWORD dwReason, LPVOID lpReserved)\n");
    Main.append("{ return TRUE;}\n\n");

    Main += "structure_value values[] =\n";
    Main += "{\n";
    for(int i = 0; i < pModel->ValueSet.Count(); i++)
    {
        Main += "&library::";
        Main += pModel->ValueSet.Name(i);
        Main += ", \"";
        Main += pModel->ValueSet.Name(i);
        Main += "\"";
        if(i< pModel->ValueSet.Count() -1)
            Main += ",";
        Main += "\n";
    }
    Main += "};\n";
    Main += "\n";

    Main += "structure_function functions[] =\n";
    Main += "{\n";
    for(int i = 0; i < pModel->FunctionSet.Count(); i++)
    {
        Main += "&library::";
        Main += pModel->FunctionSet.FunctionName(i);
        Main += ", ";

        Main += pModel->FunctionSet.FunctionResultType(i);

        Main += ", \"";
        Main += pModel->FunctionSet.FunctionName(i);
        Main += "\"";
        if(i< pModel->FunctionSet.Count() -1)
            Main += ",";
        Main += "\n";
    }
    Main += "};\n";
    Main += "\n";

    Main += "structure_function_t functions_t[] =\n";
    Main += "{\n";
    for(int i = 0; i < pModel->FunctionTSet.Count(); i++)
    {
        Main += "&library::";
        Main += pModel->FunctionTSet.FunctionTName(i);
        Main += ", ";

        Main += pModel->FunctionTSet.FunctionTResultType(i);

        Main += ", \"";
        Main += pModel->FunctionTSet.FunctionTName(i);
        Main += "\"";
        if(i< pModel->FunctionTSet.Count() -1)
            Main += ",";
        Main += "\n";
    }
    Main += "};\n";
    Main += "\n";

    Main.append("base_product* NewProduct()\n");
    Main.append("{\n");
    Main.append("static product* new_product = NULL;\n");
    Main.append("if(!new_product)\n");
    Main.append("new_product = new product(values,\n");
    Main.append("sizeof(values)/sizeof(structure_value),\n");
    Main.append("functions,\n");
    Main.append("sizeof(functions)/sizeof(structure_function),\n");
    Main.append("functions_t,\n");
    Main.append("sizeof(functions_t)/sizeof(structure_function_t));\n");
    Main.append("return new_product;\n");
    Main.append("}\n");
    Main.append("\n");
    Main.append("void FreeProduct(base_product* instance)\n");
    Main.append("{ delete instance; }\n");
}

void CppDllProduct::ExportFunctionsStream()
{

}

