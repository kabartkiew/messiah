#ifndef INCLUDEMODEL_H
#define INCLUDEMODEL_H

namespace messiah{

    struct FieldInfo
    {
        std::string Name;
        std::string Type;
        bool ToBeImported;
    };

    class FieldList
    {
    public:
        FieldList();
        ~FieldList();
        std::string Name;
        void AddNewField(std::string sName, std::string sType, bool bToBeImported);
        void RemoveField(int FieldNumber);
        int FieldCount();
        std::string FieldName(int FieldNumber);
        std::string FieldType(int FieldNumber);
        bool FieldToBeImported(int FieldNumber);
        void ChangeName(int FieldNumber, std::string NewName);
        void ChangeType(int FieldNumber, std::string NewType);
        void ChangeToBeImported(int FieldNumber, bool NewToBeImported);
        bool ImportAllFields();
    private:
        std::vector<FieldInfo> vFieldInfo;
    };

}

namespace messiah{

    struct Variable
    {
        std::string Name;
        std::string Type;
    };

    class DataStructure
    {
    public:
        DataStructure();
        DataStructure(FieldList& field_list);
        DataStructure(std::string nam, const Variable* var_array, int size);
        std::string Name;
        int Count();
        std::string VarName(int VarNumber);
        std::string VarType(int VarNumber);
    private:
        std::vector<Variable> Variables;
    };

    class DataModel
    {
    public:
        DataModel();
        ~DataModel();
        int Count();
        std::string StructName(int DataNumber);
        DataStructure* Structure(int DataNumber);
        void Add(const DataStructure & dataStructure);
        void Remove(int DataNumber);
    private:
        std::vector<DataStructure> Structures;
    };

    template <class variable>
    class VariableModel
    {
    public:
        int Count();
        std::string Name(int Number);
        std::string Type(int Number);
        std::string Text(int Number);
        variable* GetVariable(int Number);
        void Add(const variable & newVariable);
        void Add(std::string nam, std::string typ, std::string tex = "");
        void ChangeName(int Number, std::string NewName);
        void ChangeType(int Number, std::string NewType);
        void ChangeText(int Number, std::string NewText);
        void Remove(int Number);
    private:
        std::vector<variable> Variables;
    };

    template <class variable>
    int VariableModel<variable>::Count()
    {
        return Variables.size();
    }

    template <class variable>
    std::string VariableModel<variable>::Name(int Number)
    {
        return Variables[Number].Name;
    }

    template <class variable>
    std::string VariableModel<variable>::Type(int Number)
    {
        return Variables[Number].Type;
    }

    template <class variable>
    std::string VariableModel<variable>::Text(int Number)
    {
        return Variables[Number].Text;
    }

    template <class variable>
    variable* VariableModel<variable>::GetVariable(int Number)
    {
        return &Variables[Number];
    }

    template <class variable>
    void VariableModel<variable>::Add(const variable & newVariable)
    {
        Variables.push_back(newVariable);
    }

    template <class variable>
    void VariableModel<variable>::Add(std::string nam, std::string typ, std::string tex)
    {
        variable newVariable = {nam, typ, tex};
        Variables.push_back(newVariable);
    }

    template <class variable>
    void VariableModel<variable>::ChangeName(int Number, std::string NewName)
    {
        Variables[Number].Name = NewName;
    }

    template <class variable>
    void VariableModel<variable>::ChangeType(int Number, std::string NewType)
    {
        Variables[Number].Type = NewType;
    }

    template <class variable>
    void VariableModel<variable>::ChangeText(int Number, std::string NewText)
    {
        Variables[Number].Text = NewText;
    }

    template <class variable>
    void VariableModel<variable>::Remove(int Number)
    {
        if(Number >= 0 && Number < Variables.size())
            Variables.erase(Variables.begin() + Number);
    }

    struct Value
    {
        std::string Name;
        std::string Type;
        std::string Text;
    };

    enum total{ first = 0, sum = 1 };

    struct Function
    {
        std::string Name;
        std::string Type;
        std::string Text;
        std::string ResultType;
    };

    class FunctionModel
    {
    public:
        FunctionModel();
        ~FunctionModel();
        int Count();
        std::string FunctionName(int FunctionNumber);
        std::string FunctionType(int FunctionNumber);
        std::string FunctionText(int FunctionNumber);
        std::string FunctionResultType(int FunctionNumber);
        Function* GetFunction(int FunctionNumber);
        void Add(const Function & newFunction);
        void Add(std::string nam, std::string typ, std::string tex = "", std::string sum = "first");
        void ChangeName(int FunctionNumber, std::string NewName);
        void ChangeType(int FunctionNumber, std::string NewType);
        void ChangeText(int FunctionNumber, std::string NewText);
        void ChangeResultType(int FunctionNumber, std::string NewResultType);
        void Remove(int FunctionNumber);
    private:
        std::vector<Function> Functions;
    };

    struct FunctionT
    {
        std::string Name;
        std::string Type;
        std::string Text;
        std::string ResultType;
    };

    class FunctionTModel
    {
    public:
        FunctionTModel();
        ~FunctionTModel();
        int Count();
        std::string FunctionTName(int FunctionTNumber);
        std::string FunctionTType(int FunctionTNumber);
        std::string FunctionTText(int FunctionTNumber);
        std::string FunctionTResultType(int FunctionNumber);
        FunctionT* GetFunctionT(int FunctionTNumber);
        void Add(const FunctionT & newFunctionT);
        void Add(std::string nam, std::string typ, std::string tex = "", std::string sum = "first");
        void ChangeName(int FunctionTNumber, std::string NewName);
        void ChangeType(int FunctionTNumber, std::string NewType);
        void ChangeText(int FunctionTNumber, std::string NewText);
        void ChangeResultType(int FunctionTNumber, std::string NewResultType);
        void Remove(int FunctionTNumber);
    private:
        std::vector<FunctionT> FunctionsT;
    };

    class Model
    {
    public:
        Model();
        ~Model();
        DataModel Data;
        void AddData(const DataStructure & dataStructure);
        VariableModel<Value> ValueSet;
        void AddValue(const Value & newValue);
        FunctionModel FunctionSet;
        void AddFunction(const Function & newFunction);
        FunctionTModel FunctionTSet;
        void AddFunctionT(const FunctionT & newFunctionT);
        std::string setDurationStartMonth;
        std::string setProjTermFromZeroMonth;
    };

}







messiah::DataStructure::DataStructure()
{

}

messiah::DataStructure::DataStructure(FieldList &field_list)
{
    for(int i = 0; i < field_list.FieldCount(); i++)
    {
        if(field_list.FieldToBeImported(i))
        {
            Variable newVar = {field_list.FieldName(i), field_list.FieldType(i)};
            Variables.push_back(newVar);
        }
    }
    Name = field_list.Name;
}

messiah::DataStructure::DataStructure(std::string nam, const Variable* var_array, int size) : Name(nam)
{
    Variables = std::vector<Variable>(var_array, var_array + size);
}

int messiah::DataStructure::Count()
{
    return Variables.size();
}

std::string messiah::DataStructure::VarName(int VarNumber)
{
    if(VarNumber >= 0 && VarNumber < Variables.size())
        return Variables[VarNumber].Name;
}

std::string messiah::DataStructure::VarType(int VarNumber)
{
    if(VarNumber >= 0 && VarNumber < Variables.size())
        return Variables[VarNumber].Type;
}

messiah::DataModel::DataModel()
{

}

messiah::DataModel::~DataModel()
{

}

int messiah::DataModel::Count()
{
    return Structures.size();
}

std::string messiah::DataModel::StructName(int DataNumber)
{
    return Structures[DataNumber].Name;
}

messiah::DataStructure* messiah::DataModel::Structure(int DataNumber)
{
    return &Structures[DataNumber];
}

void messiah::DataModel::Add(const DataStructure & dataStructure)
{
    Structures.push_back(dataStructure);
}

void messiah::DataModel::Remove(int DataNumber)
{
    if(DataNumber >= 0 && DataNumber < Structures.size())
        Structures.erase(Structures.begin() + DataNumber);
}

messiah::FunctionModel::FunctionModel()
{

}

messiah::FunctionModel::~FunctionModel()
{

}

int messiah::FunctionModel::Count()
{
    return Functions.size();
}

std::string messiah::FunctionModel::FunctionName(int FunctionNumber)
{
    return Functions[FunctionNumber].Name;
}

std::string messiah::FunctionModel::FunctionType(int FunctionNumber)
{
    return Functions[FunctionNumber].Type;
}

std::string messiah::FunctionModel::FunctionText(int FunctionNumber)
{
    return Functions[FunctionNumber].Text;
}

std::string messiah::FunctionModel::FunctionResultType(int FunctionNumber)
{
    return Functions[FunctionNumber].ResultType;
}

messiah::Function* messiah::FunctionModel::GetFunction(int FunctionNumber)
{
    return &Functions[FunctionNumber];
}

void messiah::FunctionModel::Add(const Function & newFunction)
{
    Functions.push_back(newFunction);
}

void messiah::FunctionModel::Add(std::string nam, std::string typ, std::string tex, std::string sum)
{
    messiah::Function newFunction = {nam, typ, tex, sum};
    Functions.push_back(newFunction);
}

void messiah::FunctionModel::ChangeName(int FunctionNumber, std::string NewName)
{
    Functions[FunctionNumber].Name = NewName;
}

void messiah::FunctionModel::ChangeType(int FunctionNumber, std::string NewType)
{
    Functions[FunctionNumber].Type = NewType;
}

void messiah::FunctionModel::ChangeText(int FunctionNumber, std::string NewText)
{
    Functions[FunctionNumber].Text = NewText;
}

void messiah::FunctionModel::ChangeResultType(int FunctionNumber, std::string NewResultType)
{
    Functions[FunctionNumber].ResultType = NewResultType;
}

void messiah::FunctionModel::Remove(int FunctionNumber)
{
    if(FunctionNumber >= 0 && FunctionNumber < Functions.size())
        Functions.erase(Functions.begin() + FunctionNumber);
}


messiah::FunctionTModel::FunctionTModel()
{

}

messiah::FunctionTModel::~FunctionTModel()
{

}

int messiah::FunctionTModel::Count()
{
    return FunctionsT.size();
}

std::string messiah::FunctionTModel::FunctionTName(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Name;
}

std::string messiah::FunctionTModel::FunctionTType(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Type;
}

std::string messiah::FunctionTModel::FunctionTText(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Text;
}

std::string messiah::FunctionTModel::FunctionTResultType(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].ResultType;
}

messiah::FunctionT* messiah::FunctionTModel::GetFunctionT(int FunctionTNumber)
{
    return &FunctionsT[FunctionTNumber];
}

void messiah::FunctionTModel::Add(const FunctionT & newFunctionT)
{
    FunctionsT.push_back(newFunctionT);
}

void messiah::FunctionTModel::Add(std::string nam, std::string typ, std::string tex, std::string sum)
{
    messiah::FunctionT newFunctionT = {nam, typ, tex, sum};
    FunctionsT.push_back(newFunctionT);
}

void messiah::FunctionTModel::ChangeName(int FunctionTNumber, std::string NewName)
{
    FunctionsT[FunctionTNumber].Name = NewName;
}

void messiah::FunctionTModel::ChangeType(int FunctionTNumber, std::string NewType)
{
    FunctionsT[FunctionTNumber].Type = NewType;
}

void messiah::FunctionTModel::ChangeText(int FunctionTNumber, std::string NewText)
{
    FunctionsT[FunctionTNumber].Text = NewText;
}

void messiah::FunctionTModel::ChangeResultType(int FunctionTNumber, std::string NewResultType)
{
    FunctionsT[FunctionTNumber].ResultType = NewResultType;
}

void messiah::FunctionTModel::Remove(int FunctionTNumber)
{
    if(FunctionTNumber >= 0 && FunctionTNumber < FunctionsT.size())
        FunctionsT.erase(FunctionsT.begin() + FunctionTNumber);
}

messiah::Model::Model() : setDurationStartMonth("return 0;"), setProjTermFromZeroMonth("return p_settings->proj_term_month;")
{

}

messiah::Model::~Model()
{

}

void messiah::Model::AddData(const DataStructure & dataStructure)
{
    Data.Add(dataStructure);
}

void messiah::Model::AddValue(const Value & newValue)
{
    ValueSet.Add(newValue);
}

void messiah::Model::AddFunction(const Function &newFunction)
{
    FunctionSet.Add(newFunction);
}

void messiah::Model::AddFunctionT(const FunctionT &newFunctionT)
{
    FunctionTSet.Add(newFunctionT);
}

messiah::FieldList::FieldList()
{

}

messiah::FieldList::~FieldList()
{
}

void messiah::FieldList::AddNewField(std::string sName, std::string sType, bool bToBeImported)
{
    FieldInfo newfield = FieldInfo();
    newfield.Name = sName;
    newfield.Type = sType;
    newfield.ToBeImported = bToBeImported;
    vFieldInfo.push_back(newfield);
}

void messiah::FieldList::RemoveField(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        vFieldInfo.erase(vFieldInfo.begin() + FieldNumber);
}

int messiah::FieldList::FieldCount()
{
    return vFieldInfo.size();
}

std::string messiah::FieldList::FieldName(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].Name;
}

std::string messiah::FieldList::FieldType(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].Type;
}

bool messiah::FieldList::FieldToBeImported(int FieldNumber)
{
    if(FieldNumber >= 0 && FieldNumber < vFieldInfo.size())
        return vFieldInfo[FieldNumber].ToBeImported;
}

void messiah::FieldList::ChangeName(int FieldNumber, std::string NewName)
{
    vFieldInfo[FieldNumber].Name = NewName;
}

void messiah::FieldList::ChangeType(int FieldNumber, std::string NewType)
{
    vFieldInfo[FieldNumber].Type = NewType;
}

void messiah::FieldList::ChangeToBeImported(int FieldNumber, bool NewToBeImported)
{
    vFieldInfo[FieldNumber].ToBeImported = NewToBeImported;
}

bool messiah::FieldList::ImportAllFields()
{
    bool All = true;
    for(int i = 0; i < vFieldInfo.size(); i++)
    {
        if(!vFieldInfo[i].ToBeImported) All = false;
    }
    return All;
}

#endif // INCLUDEMODEL_H
