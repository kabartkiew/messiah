#ifndef INCLUDEIMPORTDATA_H
#define INCLUDEIMPORTDATA_H








class base_product;

class policy{
    template<class type> friend class function;
    template<class type> friend class function_t;
    friend class product;
protected:
    unsigned int count;
    unsigned int number;
    int duration_start_month;
    int proj_term_from_zero_month;
    int proj_term_from_start_month;
public:
    policy();
    ~policy();
    virtual void open(HINSTANCE) = 0;
    virtual void next() = 0;
    virtual bool last() = 0;
    void read(base_product* p_product);
};

template <class points_type>
class points: public policy{
public:
    points_type* p_point;
    points() : p_point(0) {}
    points_type* point(){ return p_point; }
    bool last();
    void open(HINSTANCE inst);
    void next();
};

template <class points_type>
void points<points_type>::open(HINSTANCE inst){
    p_point = (points_type*)GetProcAddress(inst, "punkty_modelu");
    count = *(unsigned int*)GetProcAddress(inst, "punkty_liczba_polis");
    number = 1;
}

template <class points_type>
bool points<points_type>::last(){
    if(number == count){ return true; }
    else{ return false; }
}

template <class points_type>
void points<points_type>::next(){
    p_point++;
    number++;
}










namespace messiah{
    class settings
    {
    public:

        int start_year;
        int start_month;

        int proj_term_month;
        int calendar_year(int m)
        {

            return start_year + (start_month + m -1)/12;
        }
        int calendar_month(int m)
        {

            return (start_month + m -1) %12 +1;
        }
        bool check();
    };

}
class base_product{
public:
    policy* p_data;
    messiah::settings* p_settings;
    virtual ~base_product(){}
    void set_data(policy* proj_data);
    virtual void set_settings(messiah::settings& proj_setting) = 0;
    virtual int set_duration_start_month() = 0;
    virtual int set_proj_term_from_zero_month() = 0;
    virtual void calculate_first_policy() = 0;
    virtual void calculate_policy() = 0;
    virtual void save_outcomes(std::ostream& file, char c_precision = 2) = 0;
};

void policy::read(base_product *p_product)
{
    duration_start_month = p_product->set_duration_start_month();

    proj_term_from_zero_month = p_product->set_proj_term_from_zero_month();
    if(proj_term_from_zero_month < 0)
        proj_term_from_zero_month = 0;

    proj_term_from_start_month = proj_term_from_zero_month - duration_start_month;
    if(proj_term_from_start_month > p_product->p_settings->proj_term_month)
        proj_term_from_start_month = p_product->p_settings->proj_term_month;
    if(proj_term_from_start_month < 0)
        proj_term_from_start_month = -1;
}

policy::policy() : count(0), number(0),
                   duration_start_month(0), proj_term_from_zero_month(0), proj_term_from_start_month(0)
{
}

policy::~policy()
{
}






void base_product::set_data(policy* proj_data)
{
    p_data = proj_data;
}

#endif // INCLUDEIMPORTDATA_H
