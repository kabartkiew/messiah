# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary1ini.h"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary1ini.h"



# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.h" 1







# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/attr.h" 1
# 10 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/attr.h"
class base_attr_value
{
    friend std::ostream& operator<<(std::ostream& str, base_attr_value& val);
protected:
    char* name;
    virtual void print(std::ostream& str) const = 0;
public:
    ~base_attr_value(){}
};

enum done{ no, ok, error };

template <class type>
class value;

template <class type>
class function;

template <class type>
class function_t;

enum total{ first = 0, sum = 1 };

template <class type>
class attr_value : virtual public base_attr_value
{
private:
    type* outcome;
public:
    attr_value(value<type>* p_value, char* c_word)
    {
        this->name = c_word;
        outcome = new type;
        *outcome = *p_value;
    }
    ~attr_value()
    {
        delete outcome;
        outcome = 0;
    }
    void print(std::ostream& str) const
    {
        str << name << ", " << *outcome << "\n";
    }
};

class base_attr_function : public base_attr_value
{
public:
 virtual void calculate_first() = 0;
 virtual void calculate() = 0;
};

template <class type>
class attr_function : virtual public base_attr_function {
private:
 function<type>* p_recipe;
    total result_type;
    const int number_of_errors_allowed;
    int number_of_errors_so_far;
    type* outcome;
    done status() const
    {
        if(p_recipe->calculation_finished(result_type))
            if(number_of_errors_so_far <= number_of_errors_allowed)
                return ok;
            else
                return error;
        else
            return no;
    }
    void print(std::ostream& str) const;
public:
    attr_function(function<type>* p_function, total tt, char* c_word)
        : p_recipe(p_function), result_type(tt), number_of_errors_allowed(0), number_of_errors_so_far(0)
  {
            this->name = c_word;
            outcome = new type;
  }
 ~attr_function()
  {
            delete outcome;
  }
    void calculate_first()
        {
            try
            {
                *outcome = *p_recipe;
            }
            catch(...)
            {
                number_of_errors_so_far++;
            }
        }
    void calculate()
  {
            try
            {
                if(result_type){ *outcome += *p_recipe; }
            }
            catch(...)
            {
                number_of_errors_so_far++;
            }
        }
};

template <class type>
void attr_function<type>::print(std::ostream& str) const
{
    str << this->name << ", ";

    switch(status())
    {
    case no:
        str << "no results\n";
        break;

    case ok:
        str << *outcome << "\n";
        break;

    case error:
        str << "error\n";
        break;
    }
}

class base_attr_function_t : public base_attr_value
{
public:
    virtual void prepare(int i_proj_term_month) = 0;
 virtual void calculate_first(int i, int t) = 0;
 virtual void calculate(int i, int t) = 0;
 virtual void nullify(int i) = 0;
};

template <class type>
class attr_function_t : public base_attr_function_t {
private:
 function_t<type>* p_recipe;
    total result_type;
    const int number_of_errors_allowed;
    int number_of_errors_so_far;
 int r_proj_term_month;
    type* outcome;
    done status() const
    {
        if(p_recipe->calculation_finished(result_type))
            if(number_of_errors_so_far <= number_of_errors_allowed)
                return ok;
            else
                return error;
        else
            return no;
    }
    void print(std::ostream& str) const;
public:
    attr_function_t(function_t<type>* p_function_t, total tt, char* c_word);
    ~attr_function_t()
  {
            delete[] outcome;
            outcome = 0;
        }
    void prepare(int i_proj_term_month)
        {
            r_proj_term_month = i_proj_term_month;
            this->outcome = new type[r_proj_term_month + 1];
        }
    void calculate_first(int i, int t)
  {
            try
            {
                outcome[i] = (*p_recipe)[t];
            }
            catch(...)
            {
                if(p_recipe->no_errors())
                    number_of_errors_so_far++;
                p_recipe->failed();
            }
        }
    void calculate(int i, int t)
        {
            try
            {
                if(result_type){ outcome[i] += (*p_recipe)[t]; }

            }
            catch(...)
            {
                if(p_recipe->no_errors())
                    number_of_errors_so_far++;
                p_recipe->failed();
            }
        }
    void nullify(int i)
        {
            outcome[i] = 0;
        }
};

template <class type>
attr_function_t<type>::attr_function_t(function_t<type>* p_function, total tt, char* c_word)
                        : result_type(tt), number_of_errors_allowed(0), number_of_errors_so_far(0)
 {
        this->name = c_word;
        p_recipe = p_function;
        outcome = 0;
    }

template <class type>
void attr_function_t<type>::print(std::ostream& str) const
{
    str << this->name;
    type* p_results = outcome;

    switch(status())
    {
        case no:
            str << "no results";
        break;

        case ok:
            for(int m=0; m <= r_proj_term_month; m++)
            {
                str << ",";
                str << *p_results;
                p_results++;
            }
        break;

        case error:
            str << "error";
        break;
    }

    str << "\n";
}
# 9 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.h" 1



namespace messiah{
# 13 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.h"
    class settings
    {
    public:

        int start_year;
        int start_month;

        int proj_term_month;
        int calendar_year(int m)
        {

            return start_year + (start_month + m -1)/12;
        }
        int calendar_month(int m)
        {

            return (start_month + m -1) %12 +1;
        }
        bool check();
    };

}
# 10 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.h" 1







class base_product;

class policy{
    template<class type> friend class function;
    template<class type> friend class function_t;
    friend class product;
protected:
    unsigned int count;
    unsigned int number;
    int duration_start_month;
    int proj_term_from_zero_month;
    int proj_term_from_start_month;
public:
    policy();
    ~policy();
    virtual void open(HINSTANCE) = 0;
    virtual void next() = 0;
    virtual bool last() = 0;
    void read(base_product* p_product);
};

template <class points_type>
class points: public policy{
public:
    points_type* p_point;
    points() : p_point(0) {}
    points_type* point(){ return p_point; }
    bool last();
    void open(HINSTANCE inst);
    void next();
};

template <class points_type>
void points<points_type>::open(HINSTANCE inst){
    p_point = (points_type*)GetProcAddress(inst, "punkty_modelu");
    count = *(unsigned int*)GetProcAddress(inst, "punkty_liczba_polis");
    number = 1;
}

template <class points_type>
bool points<points_type>::last(){
    if(number == count){ return true; }
    else{ return false; }
}

template <class points_type>
void points<points_type>::next(){
    p_point++;
    number++;
}
# 11 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.h" 2



class base_value;
class base_function;
class base_function_t;

struct structure_value
{
public:
    base_value* p_value;
    char* c_name;
};

class vector_attr_value{
private:
    base_attr_value** attr_values;
    int how_many;
public:
    vector_attr_value(structure_value table[], int number);
    ~vector_attr_value();
    operator base_attr_value**()
        {
            return attr_values;
        }
    void save_outcomes(std::ostream &file);
};

struct structure_function
{
    base_function* p_function;
    total total_type;
    char* c_name;
};

class vector_attr_function{
private:
    base_attr_function** attr_functions;
    int how_many;
public:
    vector_attr_function(structure_function table[], int number);
    ~vector_attr_function();
    operator base_attr_function**()
        {
            return attr_functions;
        }
    void calculate_first_policy();
    void calculate_policy();
    void save_outcomes(std::ostream &file);
};

struct structure_function_t
{
    base_function_t* p_function_t;
    total total_type;
    char* c_nazwa;
};

class vector_attr_function_t{
private:
    base_attr_function_t** attr_functions_t;
    int how_many;
public:
    vector_attr_function_t(structure_function_t table[], int number);
    ~vector_attr_function_t();
    operator base_attr_function_t**()
    {
        return attr_functions_t;
    }
    void prepare(int& i_proj_term_month);
    void calculate_first_policy_varying_t(const int since, const int until, const int t_shift);
    void calculate_policy_varying_t(const int since, const int until, const int t_shift);
    void nullify_results(const int since, const int until);
    void save_outcomes(std::ostream& file);
};



class base_product{
public:
    policy* p_data;
    messiah::settings* p_settings;
    virtual ~base_product(){}
    void set_data(policy* proj_data);
    virtual void set_settings(messiah::settings& proj_setting) = 0;
    virtual int set_duration_start_month() = 0;
    virtual int set_proj_term_from_zero_month() = 0;
    virtual void calculate_first_policy() = 0;
    virtual void calculate_policy() = 0;
    virtual void save_outcomes(std::ostream& file, char c_precision = 2) = 0;
};



class product : virtual public base_product{
private:
    vector_attr_value* v_attr_values;
    vector_attr_function* v_attr_functions;
    vector_attr_function_t* v_attr_functions_t;
    void calculate_first_policy_in_time();
    void calculate_policy_in_time();
public:
    product(structure_value* tabl_struct_values,
            int int_values,
            structure_function* tabl_struct_functions,
            int int_functions,
            structure_function_t* tabl_struct_functions_t,
            int int_functions_t);
    ~product();
    void set_settings(messiah::settings& proj_setting);
    int set_duration_start_month();
    int set_proj_term_from_zero_month();
    void calculate_first_policy();
    void calculate_policy();
    void save_outcomes(std::ostream& file, char c_precision = 2);
};
# 5 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary1ini.h" 2
