# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h"






# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.h" 1
# 8 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.cpp" 1
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.h" 1
# 2 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.cpp" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.h" 1
# 3 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/policy.cpp" 2

void policy::read(base_product *p_product)
{
    duration_start_month = p_product->set_duration_start_month();

    proj_term_from_zero_month = p_product->set_proj_term_from_zero_month();
    if(proj_term_from_zero_month < 0)
        proj_term_from_zero_month = 0;

    proj_term_from_start_month = proj_term_from_zero_month - duration_start_month;
    if(proj_term_from_start_month > p_product->p_settings->proj_term_month)
        proj_term_from_start_month = p_product->p_settings->proj_term_month;
    if(proj_term_from_start_month < 0)
        proj_term_from_start_month = -1;
}

policy::policy() : count(0), number(0),
                   duration_start_month(0), proj_term_from_zero_month(0), proj_term_from_start_month(0)
{
}

policy::~policy()
{
}
# 9 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.h" 1
# 10 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.cpp" 1
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.h" 1
# 2 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/settings.cpp" 2

bool messiah::settings::check()
{
    if(start_year>0 && start_month>0 && proj_term_month>0)
        return true;
    else
        return false;
}
# 11 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h" 2
# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.cpp" 1






# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/func.h" 1



# 1 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/attr.h" 1
# 5 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/func.h" 2





class base_value
{
public:
    virtual base_attr_value* new_attr_value(char* c_word) = 0;
};

template <class type>
class value : public base_value
{
private:
 attr_value<type>* p_attr_value;
    type result;
public:
    value(type* value)
    {
        result = *value;
        p_attr_value = 0;
    }
 ~value()
 {
  delete p_attr_value;
  p_attr_value = 0;
 }
    base_attr_value* new_attr_value(char* c_word);
 operator type()
 {
  return result;
 }
};

template <class type>
base_attr_value* value<type>::new_attr_value(char* c_word)
{
    delete p_attr_value;
    p_attr_value = new attr_value<type>(this, c_word);
    return p_attr_value;
}

class base_function
{
public:
    virtual base_attr_function* new_attr_function(total tt, char* c_word) = 0;
};

template <class type>
class function : public base_function
{
private:
 type (*formula)();
    unsigned int last_calculated_policy_nr;
 attr_function<type>* p_attr_function;
    type result;
public:
    function(type (*x)())
        : formula(x), last_calculated_policy_nr(0), p_attr_function(0) { }
 ~function()
  {
   delete p_attr_function;
   p_attr_function = 0;
  }
    base_attr_function* new_attr_function(total tt, char* c_word);
 operator type();
    bool calculation_finished(total result_type)
    {
        if(((result_type == first) && (last_calculated_policy_nr == 1))
                ||((result_type == sum) && (last_calculated_policy_nr == NewProduct()->p_data->count)))
            return true;
        else
            return false;
    }
};

template <class type>
base_attr_function* function<type>::new_attr_function(total tt, char* c_word)
{
    delete p_attr_function;
    p_attr_function = new attr_function<type>(this, tt, c_word);
    return p_attr_function;
}

template <class type>
function<type>::operator type()
{
    if(last_calculated_policy_nr < NewProduct()->p_data->number)
    {
        last_calculated_policy_nr = NewProduct()->p_data->number;

        result = (*formula)();
    }
    return result;
}

class base_function_t
{
public:
    virtual base_attr_function_t* new_attr_function_t(total tt, char* c_word) = 0;
};

template <class type>
class function_t : public base_function_t
{
private:
 type (*formula)(int);
    unsigned int last_calculated_policy_nr;
 attr_function_t<type>* p_attr_function_t;
    type* result;
    bool* calculated;

    int errors_allowed;
    int nr_of_errors;
public:
    function_t(type (*x)(int))
        : formula(x), last_calculated_policy_nr(0), p_attr_function_t(0),
          result(0), calculated(0), errors_allowed(0), nr_of_errors(0) {}
    ~function_t();
 void start(int i_months);
 void end();
    base_attr_function_t* new_attr_function_t(total tt, char* c_word);
 type operator[](int t);
    bool no_errors()
    {
        if(nr_of_errors > errors_allowed)
            return false;
        else
            return true;
    }
    void failed()
    {
        nr_of_errors++;
    }
    bool calculation_finished(total result_type)
    {
        if(((result_type == first) && (last_calculated_policy_nr == 1))
                ||((result_type == sum) && (last_calculated_policy_nr == NewProduct()->p_data->count)))
            return true;
        else
            return false;
    }
};

template <class type>
function_t<type>::~function_t()
{
    end();
    delete p_attr_function_t;
    p_attr_function_t = 0;
}

template <class type>
void function_t<type>::start(int i_months)
{
    if(!result) delete[] result;
 result = new type[i_months];
    if(!calculated) delete[] calculated;
    bool* p = calculated = new bool[i_months];
    for(int i = 0; i < i_months; i++)
        *p++ = 0;
    nr_of_errors = 0;
}

template <class type>
void function_t<type>::end()
{
 delete[] result;
    result = 0;
    delete[] calculated;
    calculated = 0;
}

template <class type>
base_attr_function_t* function_t<type>::new_attr_function_t(total tt, char* c_word)
{
 delete p_attr_function_t;
    p_attr_function_t = new attr_function_t<type>(this, tt, c_word);
 return p_attr_function_t;
}

template <class type>
type function_t<type>::operator[](int t)
{
    if(last_calculated_policy_nr < NewProduct()->p_data->number)
    {
        start(NewProduct()->p_data->proj_term_from_zero_month+1);
        last_calculated_policy_nr = NewProduct()->p_data->number;
    }
    if(!calculated[t])
    {


            result[t] = (*formula)(t);
            calculated[t] = ok;






    }
    return result[t];
}
# 8 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\../../../../lib/calculations/product.cpp" 2






std::ostream& operator<<(std::ostream& str, base_attr_value& val)
{
    val.print(str);
    return str;
}

vector_attr_value::vector_attr_value(structure_value table[], int number) : how_many(number)
{
    attr_values = new base_attr_value*[number];
    for(int i = 0; i < number; i++)
    {
        attr_values[i] = (table[i].p_value)->new_attr_value(table[i].c_name);
    }
}

vector_attr_value::~vector_attr_value()
{
 delete[] attr_values;
    attr_values = 0;
}

void vector_attr_value::save_outcomes(std::ostream &file)
{
    base_attr_value** pp_attr_value = attr_values;
    for(int f = 0; f < how_many; f++)
    {
        file << **pp_attr_value++;
    }
}

vector_attr_function::vector_attr_function(structure_function table[], int number)
    : how_many(number)
{
 attr_functions = new base_attr_function*[number];
 for(int i = 0; i < number; i++)
 {
        attr_functions[i] = (table[i].p_function)->new_attr_function(table[i].total_type, table[i].c_name);
    }
}

vector_attr_function::~vector_attr_function()
{
 delete[] attr_functions;
    attr_functions = 0;
}

void vector_attr_function::calculate_first_policy()
{
 base_attr_function** pp_attr_function = attr_functions;

    for(int f = 0; f < how_many; f++)
    {
        (*pp_attr_function++)->calculate_first();
    }
}

void vector_attr_function::calculate_policy()
{
 base_attr_function** pp_attr_function = attr_functions;

 for(int f = 0; f < how_many; f++)
    {
        (*pp_attr_function++)->calculate();
    }
}

void vector_attr_function::save_outcomes(std::ostream &file)
{
 base_attr_function** pp_attr_function = attr_functions;
    for(int f = 0; f < how_many; f++)
    {
        file << **pp_attr_function++;
    }
}

vector_attr_function_t::vector_attr_function_t(structure_function_t table[], int number)
    : how_many(number)
{
    attr_functions_t = new base_attr_function_t*[number];
    for(int i = 0; i < number; i++)
    {
        attr_functions_t[i] =
                (table[i].p_function_t)->new_attr_function_t(table[i].total_type, table[i].c_nazwa);
    }
}

vector_attr_function_t::~vector_attr_function_t()
{
 delete[] attr_functions_t;
    attr_functions_t = 0;
}

void vector_attr_function_t::prepare(int& i_proj_term_month)
{
    base_attr_function_t** pp_attr_function_t = attr_functions_t;

    for(int i = 0; i < how_many; i++)
    {
        (*pp_attr_function_t++)->prepare(i_proj_term_month);
    }
}

void vector_attr_function_t::calculate_first_policy_varying_t(const int since, const int until, const int t_shift)
{
base_attr_function_t** pp_attr_function_t;

 int t = since + t_shift;
    for(int i = since; i <= until; i++, t++)
    {
  pp_attr_function_t = attr_functions_t;
  for(int f = 0; f < how_many; f++)
        {
            (*pp_attr_function_t++)->calculate_first(i,t);
        }
     }
}

void vector_attr_function_t::calculate_policy_varying_t(const int since, const int until, const int t_shift)
{
base_attr_function_t** pp_attr_function_t;

 int t = since + t_shift;
    for(int i = since; i <= until; i++, t++)
    {
  pp_attr_function_t = attr_functions_t;
  for(int f = 0; f < how_many; f++)
        {
            (*pp_attr_function_t++)->calculate(i,t);
        }
    }
}

void vector_attr_function_t::nullify_results(const int since, const int until)
{
base_attr_function_t** pp_attr_function_t;

 for(int i = since; i <= until; i++)
    {
  pp_attr_function_t = attr_functions_t;
  for(int f = 0; f < how_many; f++)
        {
                (*pp_attr_function_t++)->nullify(i);
        }
    }
}

void vector_attr_function_t::save_outcomes(std::ostream& file)
{
    base_attr_function_t** pp_attr_function_t = attr_functions_t;
    for(int f = 0; f < how_many; f++)
    {
        file << **pp_attr_function_t++;
    }
}



void base_product::set_data(policy* proj_data)
{
    p_data = proj_data;
}



product::product(structure_value* tabl_struct_values,
        int int_values,
        structure_function* tabl_struct_functions,
        int int_functions,
        structure_function_t* tabl_struct_functions_t,
        int int_functions_t)
{
    v_attr_values = new vector_attr_value(tabl_struct_values, int_values);
    v_attr_functions = new vector_attr_function(tabl_struct_functions, int_functions);
    v_attr_functions_t = new vector_attr_function_t(tabl_struct_functions_t, int_functions_t);
}

product::~product()
{
    delete v_attr_values;
    v_attr_values = 0;
    delete v_attr_functions;
    v_attr_functions = 0;
    delete v_attr_functions_t;
    v_attr_functions_t = 0;
}

void product::set_settings(messiah::settings& proj_setting)
{
    p_settings = &proj_setting;
    v_attr_functions_t->prepare(p_settings->proj_term_month);
}

void product::calculate_first_policy_in_time()
{
    int i=0;

    if( NewProduct()->p_data->duration_start_month < 0)
    {
        v_attr_functions_t->nullify_results(0, -NewProduct()->p_data->duration_start_month-1);
        i = -NewProduct()->p_data->duration_start_month;
    }

    v_attr_functions_t->calculate_first_policy_varying_t(i, NewProduct()->p_data->proj_term_from_start_month, NewProduct()->p_data->duration_start_month);

    v_attr_functions_t->nullify_results(NewProduct()->p_data->proj_term_from_start_month+1, p_settings->proj_term_month);
}

void product::calculate_policy_in_time()
{
    int i=0;

    if( NewProduct()->p_data->duration_start_month < 0)
    {
        i = -NewProduct()->p_data->duration_start_month;
    }

    v_attr_functions_t->calculate_policy_varying_t(i, NewProduct()->p_data->proj_term_from_start_month, NewProduct()->p_data->duration_start_month);
}

void product::calculate_first_policy()
{
    v_attr_functions->calculate_first_policy();

    calculate_first_policy_in_time();
}

void product::calculate_policy()
{
    v_attr_functions->calculate_policy();

    calculate_policy_in_time();
}

void product::save_outcomes(std::ostream& file, char c_precision)
{
    file << "constant values:\n";
    v_attr_values->save_outcomes(file);

    file << "constant functions - results:\n";
    v_attr_functions->save_outcomes(file);

    file << "volatile functions / months";
    for(int m=0; m<= p_settings->proj_term_month; m++)
    {
        file << ","
             << p_settings->calendar_month(m) << "-"
             << p_settings->calendar_year(m);
    }
    file << std::endl;

    v_attr_functions_t->save_outcomes(file);
}
# 12 "D:/PROJEKTY/MESSIAH/messiah-0.0.0-src\\lib\\createdll\\append\\ini\\includelibrary2ini.h" 2
