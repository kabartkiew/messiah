CONFIG += staticlib

QT -= gui

TEMPLATE = lib

DEFINES += LIB_LIBRARY

DEFINES += ROOTPATH=\\\"$$PWD\\\"

SOURCES += \
           calculations/projection.cpp \
           calculations/settings.cpp \
           calculations/model.cpp \
           calculations/policy.cpp \
           calculations/product.cpp \
           calculations/speed.cpp \
           calculations/func.cpp \
           createdll/cppdlldata.cpp \
           createdll/cppdllmodel.cpp \
           createdll/cppdllproduct.cpp \
           import/importdata.cpp \
           import/csvdata.cpp \
           import/prophetdata.cpp \
           import/fieldlist.cpp \
           plans/runplan.cpp \
           plans/executeplan.cpp \
           other/proccessdll.cpp


HEADERS  += \
            calculations/projection.h \
            calculations/settings.h \
            calculations/model.h \
            calculations/policy.h \
            calculations/attr.h \
            calculations/func.h \
            calculations/product.h \
            calculations/speed.h \
            createdll/cppdlldata.h \
            createdll/cppdllmodel.h \
            createdll/cppdllproduct.h \
            import/importdata.h \
            import/csvdata.h \
            import/prophetdata.h \
            import/fieldlist.h \
            plans/runplan.h \
            plans/executeplan.h \
            other/proccessdll.h


INCLUDEPATH += . \
               plans \
               calculations \
               import \
               other
