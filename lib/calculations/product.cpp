#ifndef INCLUDEDLL
    #include <fstream>
    #include <iomanip>
#endif

#include "product.h"
#include "func.h"
#include "policy.h"

#ifdef PRODUCTDLL

#define POLICY_DATA NewProduct()->p_data

std::ostream& operator<<(std::ostream& str, base_attr_value& val)
{
    val.print(str);
    return str;
}

vector_attr_value::vector_attr_value(structure_value table[], int number) : how_many(number)
{
    attr_values = new base_attr_value*[number];
    for(int i = 0; i < number; i++)
    {
        attr_values[i] = (table[i].p_value)->new_attr_value(table[i].c_name);
    }
}

vector_attr_value::~vector_attr_value()
{	
	delete[] attr_values;
    attr_values = 0;
}

void vector_attr_value::save_outcomes(std::ostream &file)
{
    base_attr_value** pp_attr_value = attr_values;
    for(int f = 0; f < how_many; f++)
    {
        file << **pp_attr_value++;
    }
}

vector_attr_function::vector_attr_function(structure_function table[], int number)
    : how_many(number)
{
	attr_functions = new base_attr_function*[number]; 
	for(int i = 0; i < number; i++)
	{	
        attr_functions[i] = (table[i].p_function)->new_attr_function(table[i].total_type, table[i].c_name);
    }
}

vector_attr_function::~vector_attr_function()
{	
	delete[] attr_functions;
    attr_functions = 0;
}

void vector_attr_function::calculate_first_policy()
{
	base_attr_function** pp_attr_function = attr_functions;
    
    for(int f = 0; f < how_many; f++)
    {
        (*pp_attr_function++)->calculate_first();
    }
}

void vector_attr_function::calculate_policy()
{
	base_attr_function** pp_attr_function = attr_functions;
    
	for(int f = 0; f < how_many; f++)
    {
        (*pp_attr_function++)->calculate();
    }
}

void vector_attr_function::save_outcomes(std::ostream &file)
{
	base_attr_function** pp_attr_function = attr_functions;
    for(int f = 0; f < how_many; f++)
    {    
        file << **pp_attr_function++;
    }
}

vector_attr_function_t::vector_attr_function_t(structure_function_t table[], int number)
    : how_many(number)
{	
    attr_functions_t = new base_attr_function_t*[number];
    for(int i = 0; i < number; i++)
    {
        attr_functions_t[i] =
                (table[i].p_function_t)->new_attr_function_t(table[i].total_type, table[i].c_nazwa);
    }
}

vector_attr_function_t::~vector_attr_function_t()
{	
	delete[] attr_functions_t;
    attr_functions_t = 0;
}

void vector_attr_function_t::prepare(int& i_proj_term_month)
{
    base_attr_function_t** pp_attr_function_t = attr_functions_t;

    for(int i = 0; i < how_many; i++)
    {
        (*pp_attr_function_t++)->prepare(i_proj_term_month);
    }
}

void vector_attr_function_t::calculate_first_policy_varying_t(const int since, const int until, const int t_shift)
{
base_attr_function_t** pp_attr_function_t;
	
	int t = since + t_shift;
    for(int i = since; i <= until; i++, t++)
    {
		pp_attr_function_t = attr_functions_t;
		for(int f = 0; f < how_many; f++)
        {   
            (*pp_attr_function_t++)->calculate_first(i,t);
        }
     }
}

void vector_attr_function_t::calculate_policy_varying_t(const int since, const int until, const int t_shift)
{
base_attr_function_t** pp_attr_function_t;
	
	int t = since + t_shift;
    for(int i = since; i <= until; i++, t++)
    {
		pp_attr_function_t = attr_functions_t; 
		for(int f = 0; f < how_many; f++)
        {
            (*pp_attr_function_t++)->calculate(i,t);
        }
    }
}

void vector_attr_function_t::nullify_results(const int since, const int until)
{
base_attr_function_t** pp_attr_function_t;
	
	for(int i = since; i <= until; i++)
    {
		pp_attr_function_t = attr_functions_t;
		for(int f = 0; f < how_many; f++)
        {
                (*pp_attr_function_t++)->nullify(i);
        }
    }
}

void vector_attr_function_t::save_outcomes(std::ostream& file)
{
    base_attr_function_t** pp_attr_function_t = attr_functions_t;
    for(int f = 0; f < how_many; f++)
    {
        file << **pp_attr_function_t++;
    }
}

#endif //PRODUCTDLL

void base_product::set_data(policy* proj_data)
{
    p_data = proj_data;
}

#ifdef PRODUCTDLL

product::product(structure_value* tabl_struct_values,
        int int_values,
        structure_function* tabl_struct_functions,
        int int_functions,
        structure_function_t* tabl_struct_functions_t,
        int int_functions_t)
{
    v_attr_values = new vector_attr_value(tabl_struct_values, int_values);
    v_attr_functions = new vector_attr_function(tabl_struct_functions, int_functions);
    v_attr_functions_t = new vector_attr_function_t(tabl_struct_functions_t, int_functions_t);
}

product::~product()
{	
    delete v_attr_values;
    v_attr_values = 0;
    delete v_attr_functions;
    v_attr_functions = 0;
    delete v_attr_functions_t;
    v_attr_functions_t = 0;
}

void product::set_settings(messiah::settings& proj_setting)
{
    p_settings = &proj_setting;
    v_attr_functions_t->prepare(p_settings->proj_term_month);
}

void product::calculate_first_policy_in_time()
{    
    int i=0;

    if( POLICY_DATA->duration_start_month < 0)
    {
        v_attr_functions_t->nullify_results(0, -POLICY_DATA->duration_start_month-1);
        i = -POLICY_DATA->duration_start_month;
    }
	
    v_attr_functions_t->calculate_first_policy_varying_t(i, POLICY_DATA->proj_term_from_start_month, POLICY_DATA->duration_start_month);
			
    v_attr_functions_t->nullify_results(POLICY_DATA->proj_term_from_start_month+1, p_settings->proj_term_month);
}

void product::calculate_policy_in_time()
{    
    int i=0;
    
    if( POLICY_DATA->duration_start_month < 0)
    {
        i = -POLICY_DATA->duration_start_month;
    }

    v_attr_functions_t->calculate_policy_varying_t(i, POLICY_DATA->proj_term_from_start_month, POLICY_DATA->duration_start_month);
}

void product::calculate_first_policy()
{
    v_attr_functions->calculate_first_policy();

    calculate_first_policy_in_time();
}

void product::calculate_policy()
{
    v_attr_functions->calculate_policy();
         
    calculate_policy_in_time();
}

void product::save_outcomes(std::ostream& file, char c_precision)
{
    file << "constant values:\n";
    v_attr_values->save_outcomes(file);

    file << "constant functions - results:\n";
    v_attr_functions->save_outcomes(file);

    file << "volatile functions / months";
    for(int m=0; m<= p_settings->proj_term_month; m++)
    {
        file << ","
             << p_settings->calendar_month(m) << "-"
             << p_settings->calendar_year(m);
    }
    file << std::endl;

    v_attr_functions_t->save_outcomes(file);
}

#endif //PRODUCTDLL
