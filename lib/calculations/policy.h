#ifndef POLICY_H
#define POLICY_H

#ifndef INCLUDEDLL
    #include <windows.h>
#endif

class base_product;

class policy{
    template<class type> friend class function;
    template<class type> friend class function_t;
    friend class product;
protected:
    unsigned int count;
    unsigned int number;
    int duration_start_month;
    int proj_term_from_zero_month;
    int proj_term_from_start_month;
public:
    policy();
    ~policy();
    virtual void open(HINSTANCE) = 0;
    virtual void next() = 0;
    virtual bool last() = 0;
    void read(base_product* p_product);
};
 
template <class points_type>
class points:   public policy{
public:
    points_type* p_point;
    points() : p_point(0) {}
    points_type* point(){ return p_point; }
    bool last();
    void open(HINSTANCE inst);
    void next();
};

template <class points_type>
void points<points_type>::open(HINSTANCE inst){
    p_point = (points_type*)GetProcAddress(inst, "punkty_modelu");
    count = *(unsigned int*)GetProcAddress(inst, "punkty_liczba_polis");
    number = 1;
}

template <class points_type>
bool points<points_type>::last(){
    if(number == count){ return true; }
    else{	return false;	}
}

template <class points_type>
void points<points_type>::next(){
    p_point++;
    number++;
}

#endif // POLICY_H
