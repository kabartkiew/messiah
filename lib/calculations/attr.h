#ifndef ATTR_H
#define ATTR_H

#ifndef INCLUDEDLL
    #include <fstream>
#endif

#ifdef PRODUCTDLL

class base_attr_value
{
    friend std::ostream& operator<<(std::ostream& str, base_attr_value& val);
protected:
    char* name;
    virtual void print(std::ostream& str) const = 0;
public:
    ~base_attr_value(){}
};

enum done{ no, ok, error };

template <class type>
class value;

template <class type>
class function;

template <class type>
class function_t;

enum total{ first = 0, sum = 1 };

template <class type>
class attr_value : virtual public base_attr_value
{
private:
    type* outcome;
public:
    attr_value(value<type>* p_value, char* c_word)
    {
        this->name = c_word;
        outcome = new type;
        *outcome = *p_value;
    }
    ~attr_value()
    {
        delete outcome;
        outcome = 0;
    }
    void print(std::ostream& str) const
    {
        str << name << ", " << *outcome << "\n";
    }
};

class base_attr_function : public base_attr_value
{
public:
	virtual void calculate_first() = 0;
	virtual void calculate() = 0;
};

template <class type>
class attr_function : virtual public base_attr_function {
private:
	function<type>* p_recipe;
    total result_type;
    const int number_of_errors_allowed;
    int number_of_errors_so_far;
    type* outcome;
    done status() const
    {
        if(p_recipe->calculation_finished(result_type))
            if(number_of_errors_so_far <= number_of_errors_allowed)
                return ok;
            else
                return error;
        else
            return no;
    }
    void print(std::ostream& str) const;
public:
    attr_function(function<type>* p_function, total tt, char* c_word)
        : p_recipe(p_function), result_type(tt), number_of_errors_allowed(0), number_of_errors_so_far(0)
		{	
            this->name = c_word;
            outcome = new type;
		}
	~attr_function()
		{
            delete outcome;
		}
    void calculate_first()
        { 
            try
            {
                *outcome = *p_recipe;
            }
            catch(...)
            {
                number_of_errors_so_far++;
            }
        }
    void calculate()
		{
            try
            {
                if(result_type){ *outcome += *p_recipe; }
            }
            catch(...)
            {
                number_of_errors_so_far++;
            }
        }
};

template <class type>
void attr_function<type>::print(std::ostream& str) const
{
    str << this->name << ", ";

    switch(status())
    {
    case no:
        str << "no results\n";
        break;

    case ok:
        str << *outcome << "\n";
        break;

    case error:
        str << "error\n";
        break;
    }
}

class base_attr_function_t : public base_attr_value
{
public:
    virtual void prepare(int i_proj_term_month) = 0;
	virtual void calculate_first(int i, int t) = 0;
	virtual void calculate(int i, int t) = 0;
	virtual void nullify(int i) = 0;
};

template <class type>
class attr_function_t : public base_attr_function_t {
private:
	function_t<type>* p_recipe;
    total result_type;
    const int number_of_errors_allowed;
    int number_of_errors_so_far;
	int r_proj_term_month;
    type* outcome;
    done status() const
    {
        if(p_recipe->calculation_finished(result_type))
            if(number_of_errors_so_far <= number_of_errors_allowed)
                return ok;
            else
                return error;
        else
            return no;
    }
    void print(std::ostream& str) const;
public:
    attr_function_t(function_t<type>* p_function_t, total tt, char* c_word);
    ~attr_function_t()
		{
            delete[] outcome;
            outcome = 0;
        }
    void prepare(int i_proj_term_month)
        {
            r_proj_term_month = i_proj_term_month;
            this->outcome = new type[r_proj_term_month + 1];
        }
    void calculate_first(int i, int t)
		{ 
            try
            {
                outcome[i] = (*p_recipe)[t];
            }
            catch(...)
            {
                if(p_recipe->no_errors())
                    number_of_errors_so_far++;
                p_recipe->failed();
            }
        }
    void calculate(int i, int t)
        { 
            try
            {
                if(result_type){ outcome[i] += (*p_recipe)[t]; }

            }
            catch(...)
            {
                if(p_recipe->no_errors())
                    number_of_errors_so_far++;
                p_recipe->failed();
            }
        }
    void nullify(int i)
        {
            outcome[i] = 0;
        }
};

template <class type>
attr_function_t<type>::attr_function_t(function_t<type>* p_function, total tt, char* c_word)
                        : result_type(tt), number_of_errors_allowed(0), number_of_errors_so_far(0)
	{   
        this->name = c_word;
        p_recipe = p_function;
        outcome = 0;
    }

template <class type>
void attr_function_t<type>::print(std::ostream& str) const
{
    str << this->name;
    type* p_results = outcome;

    switch(status())
    {
        case no:
            str << "no results";
        break;

        case ok:
            for(int m=0; m <= r_proj_term_month; m++)
            {
                str << ",";
                str << *p_results;
                p_results++;
            }
        break;

        case error:
            str << "error";
        break;
    }

    str << "\n";
}

#endif //PRODUCTDLL

#endif //ATTR_H
