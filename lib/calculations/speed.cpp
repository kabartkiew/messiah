#include "speed.h"

#include <iostream>

double speed::meter::PCFreq = 0.0;

bool speed::meter::watch_mode = false;

std::ofstream* speed::meter::output = 0;

speed::meter::meter(const char* name)
{
    if( watch_mode )
    {
        watch = true;

        meter_name = std::string(name);

        QueryPerformanceCounter( (LARGE_INTEGER *)&time_start );
    }
    else
    {
        watch = false;
    }
}

speed::meter::~meter()
{
    if( watch )
    {
        __int64 t;
        QueryPerformanceCounter( (LARGE_INTEGER *)&t );
        t -= time_start;

        std::cout << std::endl << meter_name << " wykonano w " << t/PCFreq << " ms" << std::endl; // wynik podany w milisekundach
    }

    if(output)
        output->close();
}

void speed::meter::on(std::ofstream &file)
{
    output = &file;
    LARGE_INTEGER li;
    QueryPerformanceFrequency(&li);
    PCFreq = double(li.QuadPart)/1000.0;

    watch_mode = true;
}
