#ifndef PROJECTION_H
#define PROJECTION_H

#include <string>

#include "settings.h"
#include "policy.h"
#include "product.h"

namespace messiah{

    class Projection
    {
    public:
        Projection();
        ~Projection();

        void setStartDate(int year, int month);
        void setProjTerm(int months);

        int perform(std::string& s_model, std::string& s_data, std::string &s_log, std::string& s_results);

    private:
        void run();

        void calculate(base_product *ptr_product);

        void save(base_product* ptr_product, std::string& file_path);

        HINSTANCE hDllData;
        HINSTANCE hDllModel;

        messiah::settings run_settings;
        policy* policy_data;

        std::string model_dll_path;
        std::string data_dll_path;
        std::string results_csv_path;
        std::string log_txt_path;
        std::ofstream* log;
    };

}

#endif //PROJECTION_H
