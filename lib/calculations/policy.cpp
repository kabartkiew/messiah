#include "policy.h"
#include "product.h"

void policy::read(base_product *p_product)
{
    duration_start_month = p_product->set_duration_start_month();

    proj_term_from_zero_month = p_product->set_proj_term_from_zero_month();
    if(proj_term_from_zero_month < 0)
        proj_term_from_zero_month = 0;

    proj_term_from_start_month = proj_term_from_zero_month - duration_start_month;
    if(proj_term_from_start_month > p_product->p_settings->proj_term_month)
        proj_term_from_start_month = p_product->p_settings->proj_term_month;
    if(proj_term_from_start_month < 0)
        proj_term_from_start_month = -1;
}

policy::policy() : count(0), number(0),
                   duration_start_month(0), proj_term_from_zero_month(0), proj_term_from_start_month(0)
{
}

policy::~policy()
{
}
