#ifndef BIB_H
#define BIB_H

//#include "func.h"

//class base_bib_value
//{
//public:
//	virtual base_value* new_value() = 0;
//};

//template <class type>
//class bib_value : public base_bib_value
//{
//private:
//	type* p_ini;
//	value<type>* p_value;
//public:
//	bib_value(type* x)
//	{
//		p_ini = x;
//		p_value = 0;
//	}
//	~bib_value()
//	{
//		delete p_value;
//		p_value = 0;
//	}
//	base_value* new_value()
//	{
//		p_value = new value<type>(p_ini);
//		return p_value;
//	}
//	operator type();
//};

//template <class type>
//bib_value<type>::operator type()
//{
//	if(p_value == 0)
//		{
//			p_value = new value<type>(p_ini);
//		}
//		return *p_value;
//}

//class base_bib_function
//{
//public:
//    virtual base_function* new_function() = 0;
//};

//template <class type>
//class bib_function : public base_bib_function
//{
//private:
//	type (*p_ini)();
//	function<type>* p_function;
//public:
//    bib_function(type (*x)())
//	{
//		p_ini = x;
//		p_function = 0;
//	}
//	~bib_function()
//	{
//		delete p_function;
//		p_function = 0;
//	}
//    base_function* new_function()
//	{
//        p_function = new function<type>(p_ini);
//		return p_function;
//	}
//	operator type();
//};

//template <class type>
//bib_function<type>::operator type()
//{
//	if(p_function == 0)
//	{
//        p_function = new function<type>(p_ini);
//	}
//	return *p_function;
//}

//class base_bib_function_t
//{
//public:
//    virtual base_function_t* new_function_t() = 0;
//};

//template <class type>
//class bib_function_t : public base_bib_function_t
//{
//private:
//	type (*p_ini)(int);
//	function_t<type>* p_function_t;
//public:
//	bib_function_t(type (*x)(int))
//	{
//		p_ini = x;
//		p_function_t = 0;
//	}
//	~bib_function_t()
//	{
//		delete p_function_t;
//		p_function_t = 0;
//	}
//    base_function_t* new_function_t()
//	{
//        p_function_t = new function_t<type>(p_ini);
//		return p_function_t;
//	}
//	type operator[](int t);
//};

//template <class type>
//type bib_function_t<type>::operator[](int t)
//{
//	if(p_function_t == 0)
//	{
//		p_function_t = new function_t<type>(p_ini);
//	}
//	return (*p_function_t)[t];
//}

#endif
