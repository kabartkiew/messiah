#ifndef PRODUCT_H
#define PRODUCT_H

#ifndef INCLUDEDLL
    #include <ostream>
#endif

#include "attr.h"
#include "settings.h"
#include "policy.h"

#ifdef PRODUCTDLL

class base_value;
class base_function;
class base_function_t;

struct structure_value
{
public:
    base_value* p_value;
    char* c_name;
};

class vector_attr_value{
private:
    base_attr_value** attr_values;
    int how_many;
public:
    vector_attr_value(structure_value table[], int number);
    ~vector_attr_value();
    operator base_attr_value**()
        {
            return attr_values;
        }
    void save_outcomes(std::ostream &file);
};

struct structure_function
{
    base_function* p_function;
    total total_type;
    char* c_name;
};

class vector_attr_function{
private:
    base_attr_function** attr_functions;
    int how_many;
public:
    vector_attr_function(structure_function table[], int number);
    ~vector_attr_function();
    operator base_attr_function**()
        {
            return attr_functions;
        }
    void calculate_first_policy();
    void calculate_policy();
    void save_outcomes(std::ostream &file);
};

struct structure_function_t
{
    base_function_t* p_function_t;
    total total_type;
    char* c_nazwa;
};

class vector_attr_function_t{
private:
    base_attr_function_t** attr_functions_t;
    int how_many;
public:
    vector_attr_function_t(structure_function_t table[], int number);
    ~vector_attr_function_t();
    operator base_attr_function_t**()
    {
        return attr_functions_t;
    }
    void prepare(int& i_proj_term_month);
    void calculate_first_policy_varying_t(const int since, const int until, const int t_shift);
    void calculate_policy_varying_t(const int since, const int until, const int t_shift);
    void nullify_results(const int since, const int until);
    void save_outcomes(std::ostream& file);
};

#endif //PRODUCTDLL

class base_product{
public:
    policy* p_data;
    messiah::settings* p_settings;
    virtual ~base_product(){}
    void set_data(policy* proj_data);
    virtual void set_settings(messiah::settings& proj_setting) = 0;
    virtual int set_duration_start_month() = 0;
    virtual int set_proj_term_from_zero_month() = 0;
    virtual void calculate_first_policy() = 0;
    virtual void calculate_policy() = 0;
    virtual void save_outcomes(std::ostream& file, char c_precision = 2) = 0;
};

#ifdef PRODUCTDLL

class product : virtual public base_product{
private:
    vector_attr_value* v_attr_values;
    vector_attr_function* v_attr_functions;
    vector_attr_function_t* v_attr_functions_t;
    void calculate_first_policy_in_time();
    void calculate_policy_in_time();
public:
    product(structure_value* tabl_struct_values,
            int int_values,
            structure_function* tabl_struct_functions,
            int int_functions,
            structure_function_t* tabl_struct_functions_t,
            int int_functions_t);
    ~product();
    void set_settings(messiah::settings& proj_setting);
    int set_duration_start_month();
    int set_proj_term_from_zero_month();
    void calculate_first_policy();
    void calculate_policy();
    void save_outcomes(std::ostream& file, char c_precision = 2);
};

#endif //PRODUCTDLL

#endif // PRODUCT_H
