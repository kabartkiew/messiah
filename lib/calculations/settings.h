#ifndef SETTINGS_H
#define SETTINGS_H

namespace messiah{

//    class date
//    {
//    public:
//        int year;
//        int month;
//    };

    class settings
    {
    public:
//        date start_date;
        int start_year;
        int start_month;

        int proj_term_month;
        int calendar_year(int m)
        {
            //return start_date.year + (start_date.month + m -1)/12;
            return start_year + (start_month + m -1)/12;
        }
        int calendar_month(int m)
        {
            //return (start_date.month + m -1) %12 +1;
            return (start_month + m -1) %12 +1;
        }
        bool check();
    };

}

#endif // SETTINGS_H
