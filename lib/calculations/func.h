#ifndef FUNC_H
#define FUNC_H

#include "attr.h"

#ifdef PRODUCTDLL

#define POLICY_DATA NewProduct()->p_data

class base_value
{
public:
    virtual base_attr_value* new_attr_value(char* c_word) = 0;
};

template <class type>
class value : public base_value
{
private:
	attr_value<type>* p_attr_value;
    type result;
public:
    value(type* value)
    {
        result = *value;
        p_attr_value = 0;
    }
	~value()
	{
		delete p_attr_value;
		p_attr_value = 0;
	}
    base_attr_value* new_attr_value(char* c_word);
	operator type()
	{
		return result;
	}
};

template <class type>
base_attr_value* value<type>::new_attr_value(char* c_word)
{
    delete p_attr_value;
    p_attr_value = new attr_value<type>(this, c_word);
    return p_attr_value;
}

class base_function
{
public:
    virtual base_attr_function* new_attr_function(total tt, char* c_word) = 0;
};

template <class type>
class function : public base_function
{
private:
	type (*formula)();
    unsigned int last_calculated_policy_nr;
	attr_function<type>* p_attr_function;
    type result;
public:
    function(type (*x)())
        : formula(x), last_calculated_policy_nr(0), p_attr_function(0) { }
	~function()
		{
			delete p_attr_function;
			p_attr_function = 0;
		}
    base_attr_function* new_attr_function(total tt, char* c_word);
	operator type();
    bool calculation_finished(total result_type)
    {
        if(((result_type == first) && (last_calculated_policy_nr == 1))
                ||((result_type == sum) && (last_calculated_policy_nr == POLICY_DATA->count)))
            return true;
        else
            return false;
    }
};

template <class type>
base_attr_function* function<type>::new_attr_function(total tt, char* c_word)
{
    delete p_attr_function;
    p_attr_function = new attr_function<type>(this, tt, c_word);
    return p_attr_function;
}

template <class type>
function<type>::operator type()
{
    if(last_calculated_policy_nr < POLICY_DATA->number)
    {
        last_calculated_policy_nr = POLICY_DATA->number;

        result = (*formula)();
    }
    return result;
}

class base_function_t
{
public:
    virtual base_attr_function_t* new_attr_function_t(total tt, char* c_word) = 0;
};

template <class type>
class function_t : public base_function_t
{
private:
	type (*formula)(int);
    unsigned int last_calculated_policy_nr;
	attr_function_t<type>* p_attr_function_t;
    type* result;
    bool* calculated;
//    done* calculated;
    int errors_allowed;
    int nr_of_errors;
public:
    function_t(type (*x)(int))
        : formula(x), last_calculated_policy_nr(0), p_attr_function_t(0),
          result(0), calculated(0), errors_allowed(0), nr_of_errors(0) {}
    ~function_t();
	void start(int i_months);
	void end();
    base_attr_function_t* new_attr_function_t(total tt, char* c_word);
	type operator[](int t);
    bool no_errors()
    {
        if(nr_of_errors > errors_allowed)
            return false;
        else
            return true;
    }
    void failed()
    {
        nr_of_errors++;
    }
    bool calculation_finished(total result_type)
    {
        if(((result_type == first) && (last_calculated_policy_nr == 1))
                ||((result_type == sum) && (last_calculated_policy_nr == POLICY_DATA->count)))
            return true;
        else
            return false;
    }
};

template <class type>
function_t<type>::~function_t()
{
    end();
    delete p_attr_function_t;
    p_attr_function_t = 0;
}

template <class type>
void function_t<type>::start(int i_months)
{	
    if(!result) delete[] result;
	result = new type[i_months];
    if(!calculated) delete[] calculated;
    bool* p = calculated = new bool[i_months];
    for(int i = 0; i < i_months; i++)
        *p++ = 0;
    nr_of_errors = 0;
}

template <class type>
void function_t<type>::end()
{	
	delete[] result;
    result = 0;
    delete[] calculated;
    calculated = 0;
}

template <class type>
base_attr_function_t* function_t<type>::new_attr_function_t(total tt, char* c_word)
{
	delete p_attr_function_t;
    p_attr_function_t = new attr_function_t<type>(this, tt, c_word);
	return p_attr_function_t;
}

template <class type>
type function_t<type>::operator[](int t)
{
    if(last_calculated_policy_nr < POLICY_DATA->number)
    {
        start(POLICY_DATA->proj_term_from_zero_month+1);
        last_calculated_policy_nr = POLICY_DATA->number;
    }
    if(!calculated[t])
    {
//        try
//        {
            result[t] = (*formula)(t);
            calculated[t] = ok;
//        }
//        catch(...)
//        {
//            calculated[t] = error;
//            throw;
//        }
    }
    return result[t];
}

#endif //PRODUCTDLL

#endif //FUNC_H
