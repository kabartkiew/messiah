#ifndef MODEL
#define MODEL

#ifndef INCLUDEDLL
    #include <string>
    #include <vector>
#endif

#include "../import/fieldlist.h"

namespace messiah{

    struct Variable
    {
        std::string Name;
        std::string Type;
    };

    class DataStructure
    {
    public:
        DataStructure();
        DataStructure(FieldList& field_list);
        DataStructure(std::string nam, const Variable* var_array, int size);
        std::string Name;
        int Count();
        std::string VarName(int VarNumber);
        std::string VarType(int VarNumber);
    private:
        std::vector<Variable> Variables;
    };

    class DataModel
    {
    public:
        DataModel();
        ~DataModel();
        int Count();
        std::string StructName(int DataNumber);
        DataStructure* Structure(int DataNumber);
        void Add(const DataStructure & dataStructure);
        void Remove(int DataNumber);
    private:
        std::vector<DataStructure> Structures;
    };

    template <class variable>
    class VariableModel
    {
    public:
        int Count();
        std::string Name(int Number);
        std::string Type(int Number);
        std::string Text(int Number);
        variable* GetVariable(int Number);
        void Add(const variable & newVariable);
        void Add(std::string nam, std::string typ, std::string tex = "");
        void ChangeName(int Number, std::string NewName);
        void ChangeType(int Number, std::string NewType);
        void ChangeText(int Number, std::string NewText);
        void Remove(int Number);
    private:
        std::vector<variable> Variables;
    };

    template <class variable>
    int VariableModel<variable>::Count()
    {
        return Variables.size();
    }

    template <class variable>
    std::string VariableModel<variable>::Name(int Number)
    {
        return Variables[Number].Name;
    }

    template <class variable>
    std::string VariableModel<variable>::Type(int Number)
    {
        return Variables[Number].Type;
    }

    template <class variable>
    std::string VariableModel<variable>::Text(int Number)
    {
        return Variables[Number].Text;
    }

    template <class variable>
    variable* VariableModel<variable>::GetVariable(int Number)
    {
        return &Variables[Number];
    }

    template <class variable>
    void VariableModel<variable>::Add(const variable & newVariable)
    {
        Variables.push_back(newVariable);
    }

    template <class variable>
    void VariableModel<variable>::Add(std::string nam, std::string typ, std::string tex)
    {
        variable newVariable = {nam, typ, tex};
        Variables.push_back(newVariable);
    }

    template <class variable>
    void VariableModel<variable>::ChangeName(int Number, std::string NewName)
    {
        Variables[Number].Name = NewName;
    }

    template <class variable>
    void VariableModel<variable>::ChangeType(int Number, std::string NewType)
    {
        Variables[Number].Type = NewType;
    }

    template <class variable>
    void VariableModel<variable>::ChangeText(int Number, std::string NewText)
    {
        Variables[Number].Text = NewText;
    }

    template <class variable>
    void VariableModel<variable>::Remove(int Number)
    {
        if(Number >= 0 && Number < Variables.size())
            Variables.erase(Variables.begin() + Number);
    }

    struct Value
    {
        std::string Name;
        std::string Type;
        std::string Text;
    };

    enum total{ first = 0, sum = 1 };

    struct Function
    {
        std::string Name;
        std::string Type;
        std::string Text;
        std::string ResultType;
    };

    class FunctionModel
    {
    public:
        FunctionModel();
        ~FunctionModel();
        int Count();
        std::string FunctionName(int FunctionNumber);
        std::string FunctionType(int FunctionNumber);
        std::string FunctionText(int FunctionNumber);
        std::string FunctionResultType(int FunctionNumber);
        Function* GetFunction(int FunctionNumber);
        void Add(const Function & newFunction);
        void Add(std::string nam, std::string typ, std::string tex = "", std::string sum = "first");
        void ChangeName(int FunctionNumber, std::string NewName);
        void ChangeType(int FunctionNumber, std::string NewType);
        void ChangeText(int FunctionNumber, std::string NewText);
        void ChangeResultType(int FunctionNumber, std::string NewResultType);
        void Remove(int FunctionNumber);
    private:
        std::vector<Function> Functions;
    };

    struct FunctionT
    {
        std::string Name;
        std::string Type;
        std::string Text;
        std::string ResultType;
    };

    class FunctionTModel
    {
    public:
        FunctionTModel();
        ~FunctionTModel();
        int Count();
        std::string FunctionTName(int FunctionTNumber);
        std::string FunctionTType(int FunctionTNumber);
        std::string FunctionTText(int FunctionTNumber);
        std::string FunctionTResultType(int FunctionNumber);
        FunctionT* GetFunctionT(int FunctionTNumber);
        void Add(const FunctionT & newFunctionT);
        void Add(std::string nam, std::string typ, std::string tex = "", std::string sum = "first");
        void ChangeName(int FunctionTNumber, std::string NewName);
        void ChangeType(int FunctionTNumber, std::string NewType);
        void ChangeText(int FunctionTNumber, std::string NewText);
        void ChangeResultType(int FunctionTNumber, std::string NewResultType);
        void Remove(int FunctionTNumber);
    private:
        std::vector<FunctionT> FunctionsT;
    };

    class Model
    {
    public:
        Model();
        ~Model();
        DataModel Data;
        void AddData(const DataStructure & dataStructure);
        VariableModel<Value> ValueSet;
        void AddValue(const Value & newValue);
        FunctionModel FunctionSet;
        void AddFunction(const Function & newFunction);
        FunctionTModel FunctionTSet;
        void AddFunctionT(const FunctionT & newFunctionT);
        std::string setDurationStartMonth;              //change for private and add a function to change text
        std::string setProjTermFromZeroMonth;           //change for private and add a function to change text
    };

}

#endif // MODEL

