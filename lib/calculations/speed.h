#ifndef SPEED_H
#define SPEED_H

#include <windows.h>
#include <fstream>
#include <string>

namespace speed{
	
    class meter
    {
	public:	
		static double PCFreq;

        meter(const char* name);
        ~meter();
		
        static void on(std::ofstream &file);
		
        std::string meter_name;

        __int64 time_start;

        bool watch;

        static bool watch_mode;

    private:
        static std::ofstream* output;
	};

}

#endif // SPEED_H

