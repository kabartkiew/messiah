#include <QDebug>

#include <iostream>
#include <fstream>

#include "projection.h"
#include "speed.h"
#include "settings.h"
#include "policy.h"
#include "product.h"

typedef policy* (__cdecl *class_factory_new_data)();
typedef void (__cdecl *class_factory_free_data)(policy* instance);
typedef base_product* (__cdecl *class_factory_new_product)();
typedef void (__cdecl *class_factory_free_product)(base_product* instance);

messiah::Projection::Projection() : hDllData(0), hDllModel(0)
{
    setStartDate(0,0);
    setProjTerm(0);
    policy_data = 0;
}

messiah::Projection::~Projection()
{
}

void messiah::Projection::setStartDate(int year, int month)
{
    if(year>0)
        run_settings.start_year = year;
    if(month>=1 && month <=12)
        run_settings.start_month = month;
}

void messiah::Projection::setProjTerm(int months)
{
    if(months > 0)
        run_settings.proj_term_month = months;
}

int messiah::Projection::perform(std::string& s_model, std::string& s_data, std::string& s_log, std::string& s_results)
{
    model_dll_path = s_model;
    data_dll_path = s_data;
    results_csv_path = s_results;
    log_txt_path  = s_log;

    std::ofstream file;
    file.open(s_log.c_str());
    file.close();
    log = &file;
    log->open(s_log.c_str(), std::ios::out | std::ios::app);

    speed::meter::on(file);

    if(run_settings.check())
    {
        run();

        return 0;
    }
    else
        return 1;

    log->close();
}

void messiah::Projection::run()
{
    speed::meter meter("bieg");

    hDllData = LoadLibraryA(data_dll_path.c_str());

    if(hDllData != NULL)
    {
        *log << "Points loaded!\n";
        qDebug() << "data dll loaded...";

        class_factory_new_data factory_function_new_data =
                reinterpret_cast<class_factory_new_data>(GetProcAddress(hDllData, "NewData"));
        policy_data = factory_function_new_data();

        hDllModel = LoadLibraryA(model_dll_path.c_str());
        if(hDllModel != NULL)
        {
            *log << "Model loaded!\n";
            qDebug() << "model dll loaded...";

            class_factory_new_product factory_function_new_product =
                    reinterpret_cast<class_factory_new_product>(GetProcAddress(hDllModel, "NewProduct"));
            base_product* p_product = factory_function_new_product();
            qDebug() << "new product loaded...";

            p_product->set_data(policy_data);

            calculate(p_product);

            save(p_product, results_csv_path);

            class_factory_free_product factory_function_free_product =
                    reinterpret_cast<class_factory_free_product>(GetProcAddress(hDllModel, "FreeProduct"));
            factory_function_free_product(p_product);
            qDebug() << "product deleted...";

            FreeLibrary(hDllModel);
            qDebug() << "model dll unloaded...";

            class_factory_free_data factory_function_free_data =
                    reinterpret_cast<class_factory_free_data>(GetProcAddress(hDllData, "FreeData"));
            factory_function_free_data(policy_data);
            qDebug() << "data dll unloaded...";
        }
        else
        {
            *log << "Loading model failed!\n";
        }

        FreeLibrary(hDllData);
    }
    else
    {
        *log << "Loading points failed!\n";
    }
}

void messiah::Projection::calculate(base_product* ptr_product)
{
        speed::meter meter("obliczenia");

        policy_data->open(hDllData);

        ptr_product->set_settings(run_settings);

        policy_data->read(ptr_product);
        ptr_product->calculate_first_policy();

        while(!policy_data->last())
        {
            policy_data->next();

            policy_data->read(ptr_product);
            ptr_product->calculate_policy();
        }
}

void messiah::Projection::save(base_product* ptr_product, std::string &file_path)
{
    speed::meter meter("zapis do pliku");

    std::ofstream file;
    file.open(file_path.c_str());
    file.close();
    file.open(file_path.c_str(), std::ios::out | std::ios::app);
    qDebug() << "plik " << QString(file_path.c_str()) <<  " gotowy do zapisu...";

    ptr_product->save_outcomes(file,2);
    file.close();
}
