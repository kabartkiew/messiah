#ifndef INCLUDEDLL
    #include <string>
    #include <vector>
#endif

#include "model.h"
#include "../import/fieldlist.h"

messiah::DataStructure::DataStructure()
{

}

messiah::DataStructure::DataStructure(FieldList &field_list)
{
    for(int i = 0; i < field_list.FieldCount(); i++)
    {
        if(field_list.FieldToBeImported(i))
        {
            Variable newVar = {field_list.FieldName(i), field_list.FieldType(i)};
            Variables.push_back(newVar);
        }
    }
    Name = field_list.Name;
}

messiah::DataStructure::DataStructure(std::string nam, const Variable* var_array, int size) : Name(nam)
{
    Variables = std::vector<Variable>(var_array, var_array + size);
}

int messiah::DataStructure::Count()
{
    return Variables.size();
}

std::string messiah::DataStructure::VarName(int VarNumber)
{
    if(VarNumber >= 0 && VarNumber < Variables.size())
        return Variables[VarNumber].Name;
}

std::string messiah::DataStructure::VarType(int VarNumber)
{
    if(VarNumber >= 0 && VarNumber < Variables.size())
        return Variables[VarNumber].Type;
}

messiah::DataModel::DataModel()
{

}

messiah::DataModel::~DataModel()
{

}

int messiah::DataModel::Count()
{
    return Structures.size();
}

std::string messiah::DataModel::StructName(int DataNumber)
{
    return Structures[DataNumber].Name;
}

messiah::DataStructure* messiah::DataModel::Structure(int DataNumber)
{
    return &Structures[DataNumber];
}

void messiah::DataModel::Add(const DataStructure & dataStructure)
{
    Structures.push_back(dataStructure);
}

void messiah::DataModel::Remove(int DataNumber)
{
    if(DataNumber >= 0 && DataNumber < Structures.size())
        Structures.erase(Structures.begin() + DataNumber);
}

messiah::FunctionModel::FunctionModel()
{

}

messiah::FunctionModel::~FunctionModel()
{

}

int messiah::FunctionModel::Count()
{
    return Functions.size();
}

std::string messiah::FunctionModel::FunctionName(int FunctionNumber)
{
    return Functions[FunctionNumber].Name;
}

std::string messiah::FunctionModel::FunctionType(int FunctionNumber)
{
    return Functions[FunctionNumber].Type;
}

std::string messiah::FunctionModel::FunctionText(int FunctionNumber)
{
    return Functions[FunctionNumber].Text;
}

std::string messiah::FunctionModel::FunctionResultType(int FunctionNumber)
{
    return Functions[FunctionNumber].ResultType;
}

messiah::Function* messiah::FunctionModel::GetFunction(int FunctionNumber)
{
    return &Functions[FunctionNumber];
}

void messiah::FunctionModel::Add(const Function & newFunction)
{
    Functions.push_back(newFunction);
}

void messiah::FunctionModel::Add(std::string nam, std::string typ, std::string tex, std::string sum)
{
    messiah::Function newFunction = {nam, typ, tex, sum};
    Functions.push_back(newFunction);
}

void messiah::FunctionModel::ChangeName(int FunctionNumber, std::string NewName)
{
    Functions[FunctionNumber].Name = NewName;
}

void messiah::FunctionModel::ChangeType(int FunctionNumber, std::string NewType)
{
    Functions[FunctionNumber].Type = NewType;
}

void messiah::FunctionModel::ChangeText(int FunctionNumber, std::string NewText)
{
    Functions[FunctionNumber].Text = NewText;
}

void messiah::FunctionModel::ChangeResultType(int FunctionNumber, std::string NewResultType)
{
    Functions[FunctionNumber].ResultType = NewResultType;
}

void messiah::FunctionModel::Remove(int FunctionNumber)
{
    if(FunctionNumber >= 0 && FunctionNumber < Functions.size())
        Functions.erase(Functions.begin() + FunctionNumber);
}


messiah::FunctionTModel::FunctionTModel()
{

}

messiah::FunctionTModel::~FunctionTModel()
{

}

int messiah::FunctionTModel::Count()
{
    return FunctionsT.size();
}

std::string messiah::FunctionTModel::FunctionTName(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Name;
}

std::string messiah::FunctionTModel::FunctionTType(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Type;
}

std::string messiah::FunctionTModel::FunctionTText(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].Text;
}

std::string messiah::FunctionTModel::FunctionTResultType(int FunctionTNumber)
{
    return FunctionsT[FunctionTNumber].ResultType;
}

messiah::FunctionT* messiah::FunctionTModel::GetFunctionT(int FunctionTNumber)
{
    return &FunctionsT[FunctionTNumber];
}

void messiah::FunctionTModel::Add(const FunctionT & newFunctionT)
{
    FunctionsT.push_back(newFunctionT);
}

void messiah::FunctionTModel::Add(std::string nam, std::string typ, std::string tex, std::string sum)
{
    messiah::FunctionT newFunctionT = {nam, typ, tex, sum};
    FunctionsT.push_back(newFunctionT);
}

void messiah::FunctionTModel::ChangeName(int FunctionTNumber, std::string NewName)
{
    FunctionsT[FunctionTNumber].Name = NewName;
}

void messiah::FunctionTModel::ChangeType(int FunctionTNumber, std::string NewType)
{
    FunctionsT[FunctionTNumber].Type = NewType;
}

void messiah::FunctionTModel::ChangeText(int FunctionTNumber, std::string NewText)
{
    FunctionsT[FunctionTNumber].Text = NewText;
}

void messiah::FunctionTModel::ChangeResultType(int FunctionTNumber, std::string NewResultType)
{
    FunctionsT[FunctionTNumber].ResultType = NewResultType;
}

void messiah::FunctionTModel::Remove(int FunctionTNumber)
{
    if(FunctionTNumber >= 0 && FunctionTNumber < FunctionsT.size())
        FunctionsT.erase(FunctionsT.begin() + FunctionTNumber);
}

//messiah::Model::Model() : setDurationStartMonth("return 0;"), setProjTermFromZeroMonth("return p_settings->proj_term_month;")
messiah::Model::Model()
{

}

messiah::Model::~Model()
{

}

void messiah::Model::AddData(const DataStructure & dataStructure)
{
    Data.Add(dataStructure);
}

void messiah::Model::AddValue(const Value & newValue)
{
    ValueSet.Add(newValue);
}

void messiah::Model::AddFunction(const Function &newFunction)
{
    FunctionSet.Add(newFunction);
}

void messiah::Model::AddFunctionT(const FunctionT &newFunctionT)
{
    FunctionTSet.Add(newFunctionT);
}
