#include <fstream>
#include <string>
#include <algorithm>

#include "getdir.h"
#include "getdir.cpp"

int process(std::string path, std::string filename);
int preprocess(std::string path, std::string file);
int txt_includes_append(std::string path, std::string file);
void find_and_replace(std::string& source, std::string const& find, std::string const& replace);

int main()
{
    std::string rootpath = getDir(ROOTPATH);
    std::string folderpath;

    int error;
    folderpath = rootpath + "\\lib\\createdll\\append";
    error += process(folderpath, "includemodel");
    error += process(folderpath, "includeimportdata");
    error += process(folderpath, "includelibrary1");
    error += process(folderpath, "includelibrary2");

    return error;
}

int process(std::string path, std::string filename)
{
   preprocess(path, filename);
   txt_includes_append(path, filename);
}

int preprocess(std::string path, std::string file)
{
    std::string includefilename = path + "\\ini\\" + file + "ini.h";
    std::string command;

    if(!includefilename.empty())
    {
        std::string tempfilename = path + "\\temp\\" + file + "temp.h";

        command = "g++ -E ";
        command.append(includefilename);
        command.append(" > ");
        command.append(tempfilename);

        if(!system(command.c_str()))
        {
            std::string outputfilename = path + "\\h\\" + file + ".h";

            std::ifstream inputfile(tempfilename);

            std::ofstream outputfile;
            outputfile.open(outputfilename);
            outputfile.close();
            outputfile.open(outputfilename, std::ios::out | std::ios::app);

            std::string line;
            std::string firstchar;
            std::string upperfilename = file;
            std::transform(upperfilename.begin(), upperfilename.end(), upperfilename.begin(), toupper);

            outputfile << "#ifndef " << upperfilename << "_H\n";
            outputfile << "#define " << upperfilename << "_H\n";

            while( !inputfile.eof() )
            {
                getline(inputfile, line);
                firstchar = std::string(line,0,1);
                if(firstchar != "#")
                    outputfile << line << "\n";
            }

            outputfile << "#endif // " << upperfilename << "_H\n";
            outputfile.close();
        }
        else
            return 1;
    }
    else
        return 1;

    return 0;
}

int txt_includes_append(std::string path, std::string file)
{
    std::string outputfilepath = path + "\\txt\\" + file + ".txt";
    std::ofstream outputfile(outputfilepath.c_str());

    std::string inputfilepath = path + "\\h\\" + file + ".h";
    std::ifstream inputfile(inputfilepath.c_str());

    std::string line;
    while(!inputfile.eof())
    {
        getline(inputfile, line);
        find_and_replace(line, "\"", "\\\"");
        find_and_replace(line, "\\n", "\\\\n");
        outputfile << "Includes.append(\"" << line << "\\n\");\n";
    }

    inputfile.close();
    outputfile.close();
}

void find_and_replace(std::string& source, std::string const& find, std::string const& replace)
{
    for(std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;)
    {
        source.replace(i, find.length(), replace);
        i += replace.length();
    }
}
