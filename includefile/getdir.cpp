#include <string>

std::string getDir(const char* fullFilePath)
{
    std::string directory = std::string(fullFilePath);
    std:size_t found = directory.find_last_of("/\\");
    directory.erase(found);
    return directory;
}
