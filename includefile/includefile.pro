TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += static

DEFINES += ROOTPATH=\\\"$$PWD\\\"

SOURCES += main.cpp \
           getdir.cpp

HEADERS += \
    getdir.h

