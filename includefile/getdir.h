#ifndef GETDIR
#define GETDIR

#include <string>

std::string getDir(const char* fullFilePath);

#endif // GETDIR

