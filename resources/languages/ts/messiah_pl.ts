<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>DataStructWindow</name>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="26"/>
        <source>new data</source>
        <translation>nowe dane</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="53"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="69"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="76"/>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="83"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="96"/>
        <source>Structure name:</source>
        <translation>Nazwa struktury:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="151"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datastructwindow.ui" line="158"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/datastructwindow.cpp" line="38"/>
        <source>structure_name</source>
        <translation>nazwa_struktury</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/datastructwindow.cpp" line="67"/>
        <source>Choose the file to be imported...</source>
        <translation>Wybierz plik, który ma być zaimportowany...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/datastructwindow.cpp" line="95"/>
        <source>The path to a file is incorrect.</source>
        <translation>Ścieżka do pliku jest niepoprawna.</translation>
    </message>
</context>
<context>
    <name>DataWindow</name>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="26"/>
        <source>new data</source>
        <translation>nowe dane</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="53"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="69"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="76"/>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="83"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="96"/>
        <source>Structure name:</source>
        <translation>Nazwa struktury:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="151"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="158"/>
        <source>Save As</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/datawindow.ui" line="165"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/datawindow.cpp" line="79"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/datawindow.cpp" line="79"/>
        <source>Couldn&apos;t read the data.</source>
        <translation>Nie można odczytać danych.</translation>
    </message>
    <message>
        <source>Couldn&apos;t read the data</source>
        <translation type="vanished">Nie można odczytać danych.</translation>
    </message>
</context>
<context>
    <name>ExecuteDialog</name>
    <message>
        <location filename="../../../gui/ui/executedialog.ui" line="23"/>
        <source>Executing plan</source>
        <translation>Wykonywanie planu</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/executedialog.ui" line="29"/>
        <source>Delete temporary files</source>
        <translation>Usuń tymczasowe pliki</translation>
    </message>
    <message>
        <source>Delete temporary data files</source>
        <translation type="vanished">Usuń tymczasowe pliki</translation>
    </message>
</context>
<context>
    <name>ExecuteWindow</name>
    <message>
        <location filename="../../../gui/ui/executewindow.ui" line="14"/>
        <source>Form</source>
        <translation>Formularz</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/executewindow.ui" line="26"/>
        <source>Stop</source>
        <translation>Zatrzymaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/executewindow.ui" line="46"/>
        <source>Results folder:</source>
        <translation>Folder z wynikami:</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="28"/>
        <source>Calculating...</source>
        <translation>Obliczanie...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="119"/>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="132"/>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="143"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="119"/>
        <source>Failed to open file for writing.</source>
        <translation>Nie udało się otworzyć pliku do zapisu.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="143"/>
        <source>Failed to compile model.</source>
        <translation>Nie udało się skompilować modelu.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="148"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/executewindow.cpp" line="148"/>
        <source>Model compiled.</source>
        <translation>Model został skompilowany.</translation>
    </message>
</context>
<context>
    <name>FunctionTWindow</name>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="14"/>
        <source>new function t</source>
        <translation>nowa funkcja w czasie</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="36"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="43"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="59"/>
        <source>GroupBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="90"/>
        <source>Name:</source>
        <translation>Nazwa:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="97"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functiontwindow.ui" line="104"/>
        <source>Sum type:</source>
        <translation>Typ sumowania:</translation>
    </message>
</context>
<context>
    <name>FunctionWindow</name>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="14"/>
        <source>new function</source>
        <translation>nowa funkcja</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="26"/>
        <source>GroupBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="38"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="48"/>
        <source>Sum type:</source>
        <translation>Typ sumowania:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="58"/>
        <source>Name:</source>
        <translation>Nazwa:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="100"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/functionwindow.ui" line="107"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../../../gui/ui/importdialog.ui" line="23"/>
        <source>Importing</source>
        <translation>Importowanie</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importdialog.ui" line="29"/>
        <source>Delete temporary files</source>
        <translation>Usuń tymczasowe pliki</translation>
    </message>
    <message>
        <location filename="../../../gui/src/dialogs/importdialog.cpp" line="26"/>
        <source>Data import failed!</source>
        <translation>Nie udało się zaimportować danych!</translation>
    </message>
    <message>
        <location filename="../../../gui/src/dialogs/importdialog.cpp" line="30"/>
        <source>Data imported.</source>
        <translation>Dane zostały zaimportowane.</translation>
    </message>
</context>
<context>
    <name>ImportWindow</name>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="26"/>
        <source>Data Import</source>
        <translation>Import danych</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="53"/>
        <location filename="../../../gui/ui/importwindow.ui" line="123"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="72"/>
        <source>Structure name:</source>
        <translation>Nazwa struktury:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="104"/>
        <source>Data file name:</source>
        <translation>Nazwa pliku z danymi:</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="149"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/importwindow.ui" line="156"/>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/importwindow.cpp" line="38"/>
        <source>structure_name</source>
        <translation>nazwa_struktury</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/importwindow.cpp" line="39"/>
        <source>data_file_name</source>
        <translation>nazwa_pliku_z_danymi</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/importwindow.cpp" line="69"/>
        <source>Choose the file to be imported...</source>
        <translation>Wybierz plik, który ma być zaimportowany...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/importwindow.cpp" line="82"/>
        <source>Choose a folder where to save the imported files...</source>
        <translation>Wybierz folder, w którym mają być zapisane wyniki...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/importwindow.cpp" line="110"/>
        <source>The path to a file is incorrect.</source>
        <translation>Ścieżka do pliku jest niepoprawna.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../../gui/src/forms/mainwindow.cpp" line="76"/>
        <source>Choose the run plan to load...</source>
        <translation>Wybierz, który plan obliczeń załadować ...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/mainwindow.cpp" line="98"/>
        <source>Choose the data file to preview...</source>
        <translation>Wybierz, które dane wyświetlić...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/mainwindow.cpp" line="115"/>
        <source>Choose the model to open...</source>
        <translation>Wybierz, który model otworzyć...</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/mainwindow.cpp" line="156"/>
        <source>Current Language changed to %1</source>
        <translation>Bieżący język zmieniono na %1</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="95"/>
        <source>MESSIAH</source>
        <translation>MESJASZ</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="96"/>
        <source>Plan</source>
        <translation>Plan</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="97"/>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="101"/>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="98"/>
        <source>Load</source>
        <translation>Załaduj</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="99"/>
        <source>Exit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="100"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="102"/>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="103"/>
        <source>Data</source>
        <translation>Dane</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="104"/>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="105"/>
        <source>Preview</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="106"/>
        <source>Results</source>
        <translation>Wyniki</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="107"/>
        <source>Tools</source>
        <translation>Narzędzia</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="108"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="109"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="110"/>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../../../gui/src/ui/ui_mainwindow.cpp" line="111"/>
        <source>About Messiah</source>
        <translation>O programie Mesjasz</translation>
    </message>
</context>
<context>
    <name>ModelSaveDialog</name>
    <message>
        <location filename="../../../gui/ui/modelsavedialog.ui" line="23"/>
        <source>Saving Model</source>
        <translation>Zapisywanie modelu</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelsavedialog.ui" line="29"/>
        <source>Delete temporary files</source>
        <translation>Usuń tymczasowe pliki</translation>
    </message>
    <message>
        <source>Delete temporary data files</source>
        <translation type="vanished">Usuń tymczasowe pliki</translation>
    </message>
</context>
<context>
    <name>ModelWindow</name>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="20"/>
        <source>new model</source>
        <translation>nowy model</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="30"/>
        <source>Data</source>
        <translation>Dane</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="36"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="90"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="140"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="184"/>
        <source>New</source>
        <translation>Nowe</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="69"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="113"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="160"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="207"/>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="76"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="120"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="167"/>
        <location filename="../../../gui/ui/modelwindow.ui" line="214"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="84"/>
        <source>Value</source>
        <translation>Wartość</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="128"/>
        <source>Function</source>
        <translation>Funkcja</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="175"/>
        <source>Function t</source>
        <translation>Funkcja w czasie</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="222"/>
        <source>Setting</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="228"/>
        <source>Duration Start Month</source>
        <translation>Ile miesięcy obliczać przed startem</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="235"/>
        <source>Projection Term From Zero Month</source>
        <translation>Długość projekcji od miesiąca zerowego</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="274"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="281"/>
        <source>Save As</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/modelwindow.ui" line="288"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/modelwindow.cpp" line="305"/>
        <source>Save File</source>
        <translation>Zapisz Plik</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/modelwindow.cpp" line="307"/>
        <source>DLL files (*.dll)</source>
        <translation>pliki DLL (*.dll)</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/modelwindow.cpp" line="338"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/modelwindow.cpp" line="338"/>
        <source>Model saved.</source>
        <translation>Model został zapisany.</translation>
    </message>
</context>
<context>
    <name>RunInfoDialog</name>
    <message>
        <location filename="../../../gui/ui/runinfodialog.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RunPlanWindow</name>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="26"/>
        <source>new plan</source>
        <translation>nowy plan</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="69"/>
        <source>Save As</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="82"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="98"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="133"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../../../gui/ui/runplanwindow.ui" line="140"/>
        <source>Execute</source>
        <translation>Wykonaj</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="38"/>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="46"/>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="116"/>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="205"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Couldn&apos;t open the file</source>
        <translation type="vanished">Nie można otworzyć pliku.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set the content of the file</source>
        <translation type="vanished">Nie można ustawić zawartości pliku.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="38"/>
        <source>Couldn&apos;t open the file.</source>
        <translation>Nie można otworzyć pliku.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="46"/>
        <source>Couldn&apos;t set the content of the file.</source>
        <translation>Nie można ustawić zawartości pliku.</translation>
    </message>
    <message>
        <source>No a RunPlan index in a file</source>
        <translation type="vanished">W pliku brakuje indeksu RunPlan.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="116"/>
        <source>No a RunPlan index in a file.</source>
        <translation>W pliku brakuje indeksu RunPlan.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="185"/>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="194"/>
        <source>Save File</source>
        <translation>Zapisz Plik</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="187"/>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="196"/>
        <source>XML files (*.xml)</source>
        <translation>pliki XML (*.xmll)</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="205"/>
        <source>Failed to open file for writing.</source>
        <translation>Nie udało się otworzyć pliku do zapisu.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="215"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="215"/>
        <source>Run plan saved.</source>
        <translation>Plan obliczeń został zapisany.</translation>
    </message>
    <message>
        <location filename="../../../gui/src/forms/runplanwindow.cpp" line="226"/>
        <source>Choose a folder where to save the results...</source>
        <translation>Wybierz folder, w którym mają być zapisane wyniki...</translation>
    </message>
</context>
</TS>
