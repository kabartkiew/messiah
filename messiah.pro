TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    includefile \
    lib \
    gui

gui.depends = lib
