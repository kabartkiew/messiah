greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += xml

CONFIG += static

TEMPLATE = app

TARGET = messiah

DEFINES += ROOTPATH=\\\"$$PWD\\\"

VPATH += . \
         ../lib/import \
         ../lib/calculations \
         ../lib/plans \
         src \
         src/forms \
         src/delegates \
         src/dialogs \
         src/models \
         src/ui

SOURCES += \
    src/main.cpp \
    src/settings.cpp \
    src/qmywindow.cpp \
    src/forms/mainwindow.cpp \
    src/forms/childwindow.cpp \
    src/forms/modelwindow.cpp \
    src/forms/datastructwindow.cpp \
    src/forms/functionwindow.cpp \
    src/forms/functiontwindow.cpp \
    src/forms/importwindow.cpp \
    src/forms/datawindow.cpp \
    src/forms/runplanwindow.cpp \
    src/forms/executewindow.cpp \
    src/delegates/datedelegate.cpp \
    src/delegates/progressdelegate.cpp \
    src/delegates/buttondelegate.cpp \
    src/dialogs/importdialog.cpp \
    src/dialogs/modelsavedialog.cpp \
    src/dialogs/executedialog.cpp \
    src/dialogs/runinfodialog.cpp \
    src/models/importfieldstable.cpp \
    src/models/runplantable.cpp \
    src/models/progresstable.cpp \
    src/models/datatable.cpp \
    src/models/valuetable.cpp \
    src/models/functiontable.cpp \
    src/models/functionttable.cpp \
    src/ui/ui_mainwindow.cpp \
    src/iolibrary/dllmodel.cpp

HEADERS  += \
    include/global.h \
    include/settings.h \
    include/qmywindow.h \
    include/forms/mainwindow.h \
    include/forms/childwindow.h \
    include/forms/modelwindow.h \
    include/forms/datastructwindow.h \
    include/forms/functionwindow.h \
    include/forms/functiontwindow.h \
    include/forms/importwindow.h \
    include/forms/datawindow.h \
    include/forms/runplanwindow.h \
    include/forms/executewindow.h \
    include/delegates/datedelegate.h \
    include/delegates/progressdelegate.h \
    include/delegates/buttondelegate.h \
    include/dialogs/importdialog.h \
    include/dialogs/modelsavedialog.h \
    include/dialogs/executedialog.h \
    include/dialogs/runinfodialog.h \
    include/models/runplantable.h \
    include/models/importfieldstable.h \
    include/models/progresstable.h \
    include/models/datatable.h \
    include/models/valuetable.h \
    include/models/functiontable.h \
    include/models/functionttable.h \
    include/ui/ui_mainwindow.h \
    include/iolibrary/dllmodel.h

TRANSLATIONS = ../resources/languages/ts/messiah_en.ts \
               ../resources/languages/ts/messiah_pl.ts \
               ../resources/languages/ts/messiah_fr.ts

FORMS    += \
    ui/modelwindow.ui \
    ui/runplanwindow.ui \
    ui/importwindow.ui \
    ui/datawindow.ui \
    ui/executewindow.ui \
    ui/datastructwindow.ui \
    ui/functionwindow.ui \
    ui/functiontwindow.ui \
    ui/importdialog.ui \
    ui/modelsavedialog.ui \
    ui/executedialog.ui \
    ui/runinfodialog.ui


INCLUDEPATH += . \
    ../lib \
    ../lib/import \
    ../lib/calculations \
    ../lib/plans \
    ../lib/createdll \
    ../lib/others \
    include \
    include/forms \
    include/delegates \
    include/dialogs \
    include/models \
    include/iolibrary \
    include/ui

CONFIG( debug, debug|release ){
    LIBS += ../lib/debug/liblib.a
    PRE_TARGETDEPS += ../lib/debug/liblib.a
}
else{
    LIBS += ../lib/release/liblib.a
    PRE_TARGETDEPS += ../lib/release/liblib.a
}

RESOURCES += \
    ../resources/gui.qrc \
    ../resources/languages.qrc
