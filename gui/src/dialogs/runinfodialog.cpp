#include "runinfodialog.h"
#include "ui_runinfodialog.h"

RunInfoDialog::RunInfoDialog(QWidget *parent, QString testTitle) :
    QDialog(parent),
    ui(new Ui::RunInfoDialog)
{
    ui->setupUi(this);

    setWindowTitle(testTitle);
}

RunInfoDialog::~RunInfoDialog()
{
    delete ui;
}
