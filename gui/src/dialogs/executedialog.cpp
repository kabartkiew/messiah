#include "executedialog.h"
#include "ui_executedialog.h"

ExecuteDialog::ExecuteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExecuteDialog)
{
    ui->setupUi(this);
    ui->checkBox->setCheckState(Qt::Checked);
}

ExecuteDialog::~ExecuteDialog()
{
    delete ui;
}

bool ExecuteDialog::DeleteTemp()
{
    return ui->checkBox->checkState();
}
