#include "modelsavedialog.h"
#include "ui_modelsavedialog.h"

ModelSaveDialog::ModelSaveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModelSaveDialog)
{
    ui->setupUi(this);
    ui->checkBox->setCheckState(Qt::Checked);
}

ModelSaveDialog::~ModelSaveDialog()
{
    delete ui;
}

bool ModelSaveDialog::DeleteTemp()
{
    return ui->checkBox->checkState();
}
