
#include <QMessageBox>

#include "importdialog.h"
#include "ui_importdialog.h"

ImportDialog::ImportDialog(QWidget *parent, messiah::ImportType *it) :
    QDialog(parent),
    ui(new Ui::ImportDialog)
{
    ui->setupUi(this);
    ui->checkBox->setCheckState(Qt::Checked);
    pImportType = it;
}

ImportDialog::~ImportDialog()
{
    delete ui;
}

void ImportDialog::on_buttonBox_accepted()
{
    QMessageBox msgBox;
    if(pImportType->DoImport(ui->checkBox->checkState()))
    {
        msgBox.setText(tr("Data import failed!"));
    }
    else
    {
        msgBox.setText(tr("Data imported."));
    }
    msgBox.exec();
}
