#include "importfieldstable.h"

ImportFieldsTable::ImportFieldsTable(messiah::FieldList *fieldList, QObject *parent)
    : QAbstractTableModel(parent)
{
    pFieldList = fieldList;
}

ImportFieldsTable::~ImportFieldsTable()
{

}

int ImportFieldsTable::rowCount(const QModelIndex &parent) const
{
    return pFieldList->FieldCount();
}

int ImportFieldsTable::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant ImportFieldsTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= pFieldList->FieldCount() || index.row() < 0)
        return QVariant();

    if(role == Qt::CheckStateRole)
    {
        if(index.column() == 0)
        {
            if(pFieldList->FieldToBeImported(index.row()))
                return Qt::Checked;
            else
                return Qt::Unchecked;
        }
        else
            return QVariant();
    }

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
                return pFieldList->FieldName(index.row()).c_str();

            case 1:
                return pFieldList->FieldType(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant ImportFieldsTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "VARIABLE";

            case 1:
                return "DATA TYPE";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool ImportFieldsTable::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if(!index.isValid())
        return false;

    if (index.row() >= pFieldList->FieldCount() || index.row() < 0)
        return false;

    if(role == Qt::EditRole)
    {
        if(!value.toString().isEmpty())
        {
            switch(index.column())
            {
            case 0:
                pFieldList->ChangeName(index.row(), (value.toString()).toStdString());
                break;

            case 1:
                pFieldList->ChangeType(index.row(), (value.toString()).toStdString());
                break;
            }
        }
    }

    if(role == Qt::CheckStateRole)
    {
        if(index.column()==0)
            pFieldList->ChangeToBeImported(index.row(), value.toBool());
    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags ImportFieldsTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

//bool ImportFieldsTable::removeRows(int row, int count, const QModelIndex & parent)
//{
//   beginRemoveRows(parent, row, row + count - 1);

//   pFieldList->RemoveField(row);

//   endRemoveRows();

//   return true;
//}

//bool ImportFieldsTable::insertRows(int row, int count, const QModelIndex & parent)
//{
//    beginInsertRows(parent, row, row + count - 1);

//    pFieldList->AddNewField("VAR", "float", true);

//    endInsertRows();

//    return true;
//}

void ImportFieldsTable::AddField(std::string sName, std::string sType, bool bToBeImported)
{
    pFieldList->AddNewField(sName, sType, bToBeImported);
    emit layoutChanged();
}

void ImportFieldsTable::DeleteField(const QModelIndex & index)
{
    pFieldList->RemoveField(index.row());
    emit layoutChanged();
}
