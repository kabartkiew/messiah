#include "progresstable.h"
#include "runplan.h"
#include "executeplan.h"

ProgressTable::ProgressTable(QObject *parent, messiah::RunPlan *runplan)
    : QAbstractTableModel(parent)
{
    pExecutePlan = new messiah::ExecutePlan(runplan);
}

ProgressTable::~ProgressTable()
{
    delete pExecutePlan;
}

int ProgressTable::rowCount(const QModelIndex &parent) const
{
    return pExecutePlan->RunCount();
}

int	ProgressTable::columnCount(const QModelIndex &parent) const
{
    return 4;
}

QVariant ProgressTable::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() > rowCount(index) || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        switch(index.column())
        {
        case 0:
            return pExecutePlan->Name(index.row()).c_str();
            break;

        case 1:
            return pExecutePlan->Progress(index.row());
            break;

        case 2:
            return pExecutePlan->Finished(index.row());
            break;

        case 3:
            //return
            break;

        default:
            return QVariant();
        }
    }

    return QVariant();
}

QVariant ProgressTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return "Run name";
            break;

        case 1:
            return "Progress";
            break;

        case 2:
            return "Finished";
            break;

        case 3:
            return "Details";
            break;

        default:
            return QVariant();
        }
    }

    return QVariant();
}

bool ProgressTable::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if(!index.isValid())
        return false;




    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags ProgressTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable;
}

void ProgressTable::UpdateProgress(const QModelIndex & index, char Progress)
{
    pExecutePlan->UpdateProgress(index.row(), Progress);
    emit layoutChanged();
}
