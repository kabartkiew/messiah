#include "valuetable.h"

ValueTable::ValueTable(messiah::VariableModel<messiah::Value> *valueModel, QObject *parent)
    : QAbstractTableModel(parent), pValueModel(valueModel)
{
}

ValueTable::~ValueTable()
{
}

int ValueTable::rowCount(const QModelIndex &parent) const
{
    return pValueModel->Count();
}

int ValueTable::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant ValueTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= pValueModel->Count() || index.row() < 0)
        return QVariant();

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column() == 0)
//        {
//            if(index.row() == 0)
//                return Qt::Checked;
//            else
//                return Qt::Unchecked;
//        }
//        else
//            return QVariant();
//    }

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
//                return pValueModel->ValueName(index.row()).c_str();
                return pValueModel->Name(index.row()).c_str();

            case 1:
//                return pValueModel->ValueType(index.row()).c_str();
                return pValueModel->Type(index.row()).c_str();

            case 2:
//                return pValueModel->ValueText(index.row()).c_str();
                return pValueModel->Text(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant ValueTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "NAME";

            case 1:
                return "TYPE";

            case 2:
                return "VALUE";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool ValueTable::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if(!index.isValid())
        return false;

    if (index.row() >= pValueModel->Count() || index.row() < 0)
        return false;

    if(role == Qt::EditRole)
    {
        if(!value.toString().isEmpty())
        {
            switch(index.column())
            {
            case 0:
                pValueModel->ChangeName(index.row(), (value.toString()).toStdString());
                break;

            case 1:
                pValueModel->ChangeType(index.row(), (value.toString()).toStdString());
                break;

            case 2:
                pValueModel->ChangeText(index.row(), (value.toString()).toStdString());
            }
        }
    }

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column()==0)
//            ;
//    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags ValueTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

void ValueTable::AddValue(const messiah::Value & newValue)
{
    pValueModel->Add(newValue);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void ValueTable::AddValue(std::string nam, std::string typ, std::string tex)
{
    pValueModel->Add(nam, typ, tex);
    emit layoutChanged();
}

void ValueTable::RemoveValue(const QModelIndex & index)
{
    pValueModel->Remove(index.row());
    emit layoutChanged();
}


