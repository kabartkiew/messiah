#include "functiontable.h"

FunctionTable::FunctionTable(messiah::FunctionModel *functionModel, QObject *parent)
    : QAbstractTableModel(parent), pFunctionModel(functionModel)
{
}

FunctionTable::~FunctionTable()
{
}

int FunctionTable::rowCount(const QModelIndex &parent) const
{
    return pFunctionModel->Count();
}

int FunctionTable::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant FunctionTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= pFunctionModel->Count() || index.row() < 0)
        return QVariant();

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column() == 0)
//        {
//            if(index.row() == 0)
//                return Qt::Checked;
//            else
//                return Qt::Unchecked;
//        }
//        else
//            return QVariant();
//    }

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
                return pFunctionModel->FunctionName(index.row()).c_str();

            case 1:
                return pFunctionModel->FunctionType(index.row()).c_str();

            case 2:
                return pFunctionModel->FunctionResultType(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant FunctionTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "NAME";

            case 1:
                return "TYPE";

            case 2:
                return "RESULT TYPE";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool FunctionTable::setData(const QModelIndex & index, const QVariant & Function, int role)
{
    if(!index.isValid())
        return false;

    if (index.row() >= pFunctionModel->Count() || index.row() < 0)
        return false;

    if(role == Qt::EditRole)
    {
        if(!Function.toString().isEmpty())
        {
            switch(index.column())
            {
            case 0:
                pFunctionModel->ChangeName(index.row(), (Function.toString()).toStdString());
                break;

            case 1:
                pFunctionModel->ChangeType(index.row(), (Function.toString()).toStdString());
                break;

            case 2:
                pFunctionModel->ChangeResultType(index.row(), (Function.toString()).toStdString());
                break;
            }
        }
    }

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column()==0)
//            ;
//    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags FunctionTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

void FunctionTable::AddFunction(const messiah::Function & newFunction)
{
    pFunctionModel->Add(newFunction);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void FunctionTable::AddFunction(std::string nam, std::string typ, std::string tex, std::string sum)
{
    pFunctionModel->Add(nam, typ, tex, sum);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void FunctionTable::RemoveFunction(const QModelIndex & index)
{
    pFunctionModel->Remove(index.row());
    emit layoutChanged();
}
