#include "functionttable.h"

FunctionTTable::FunctionTTable(messiah::FunctionTModel *functiontModel, QObject *parent)
    : QAbstractTableModel(parent), pFunctionTModel(functiontModel)
{
}

FunctionTTable::~FunctionTTable()
{
}

int FunctionTTable::rowCount(const QModelIndex &parent) const
{
    return pFunctionTModel->Count();
}

int FunctionTTable::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant FunctionTTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= pFunctionTModel->Count() || index.row() < 0)
        return QVariant();

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column() == 0)
//        {
//            if(index.row() == 0)
//                return Qt::Checked;
//            else
//                return Qt::Unchecked;
//        }
//        else
//            return QVariant();
//    }

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
                return pFunctionTModel->FunctionTName(index.row()).c_str();

            case 1:
                return pFunctionTModel->FunctionTType(index.row()).c_str();

            case 2:
                return pFunctionTModel->FunctionTResultType(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant FunctionTTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "NAME";

            case 1:
                return "TYPE";

            case 2:
                return "RESULT TYPE";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool FunctionTTable::setData(const QModelIndex & index, const QVariant & FunctionT, int role)
{
    if(!index.isValid())
        return false;

    if (index.row() >= pFunctionTModel->Count() || index.row() < 0)
        return false;

    if(role == Qt::EditRole)
    {
        if(!FunctionT.toString().isEmpty())
        {
            switch(index.column())
            {
            case 0:
                pFunctionTModel->ChangeName(index.row(), (FunctionT.toString()).toStdString());
                break;

            case 1:
                pFunctionTModel->ChangeType(index.row(), (FunctionT.toString()).toStdString());
                break;

            case 2:
                pFunctionTModel->ChangeResultType(index.row(), (FunctionT.toString()).toStdString());
            }
        }
    }

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column()==0)
//            ;
//    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags FunctionTTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

void FunctionTTable::AddFunctionT(const messiah::FunctionT & newFunctionT)
{
    pFunctionTModel->Add(newFunctionT);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void FunctionTTable::AddFunctionT(std::string nam, std::string typ, std::string tex, std::string sum)
{
    pFunctionTModel->Add(nam, typ, tex, sum);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void FunctionTTable::RemoveFunctionT(const QModelIndex & index)
{
    pFunctionTModel->Remove(index.row());
    emit layoutChanged();
}



