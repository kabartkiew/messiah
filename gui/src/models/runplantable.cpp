#include "runplantable.h"

RunPlanTable::RunPlanTable(messiah::RunPlan *runPlan, QObject *parent)
    : QAbstractTableModel(parent)
{
    pRunPlan = runPlan;
}

RunPlanTable::~RunPlanTable()
{

}

int RunPlanTable::rowCount(const QModelIndex &parent) const
{
    return pRunPlan->RunCount();
}

int	RunPlanTable::columnCount(const QModelIndex &parent) const
{
    return pRunPlan->AttributeCount();
}

QVariant RunPlanTable::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= pRunPlan->RunCount() || index.row() < 0)
        return QVariant();

    if(role == Qt::CheckStateRole)
    {
        if(index.column() == 0)
        {
            if(pRunPlan->InfoRunMarked(index.row()))
                return Qt::Checked;
            else
                return Qt::Unchecked;
        }
        else
            return QVariant();
    }

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if(index.column() >= pRunPlan->AttributeCount() || index.column() < 0)
            return QVariant();
        else
            return pRunPlan->InfoValue(index.row(), index.column()).c_str();
    }

    return QVariant();
}

QVariant RunPlanTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if(section >= pRunPlan->AttributeCount() || section < 0)
            return QVariant();
        else
            return (pRunPlan->Attribute(section)).c_str();
    }

    return QVariant();
}

bool RunPlanTable::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if(!index.isValid())
        return false;

    if (index.row() >= pRunPlan->RunCount() || index.row() < 0)
        return false;

    if(role == Qt::EditRole)
    {
        if(index.column() >= pRunPlan->AttributeCount() || index.column() < 0)
            return false;
        else
            pRunPlan->PutToRunInfo(value.toString().toStdString(), index.row(), index.column());
    }

    if(role == Qt::CheckStateRole)
    {
        if(index.column() == 0)
            pRunPlan->MarkRunInfo(value.toBool(), index.row());
    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags RunPlanTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

void RunPlanTable::AddRun()
{
    pRunPlan->AddNewRun();
    emit layoutChanged();
}

void RunPlanTable::DeleteRun(const QModelIndex &index)
{
    pRunPlan->RemoveRun(index.row());
    emit layoutChanged();
}
