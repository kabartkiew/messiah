#include "datatable.h"

DataTable::DataTable(messiah::DataModel *dataModel, QObject *parent)
    : QAbstractTableModel(parent), pDataModel(dataModel)
{
}

DataTable::~DataTable()
{
}

int DataTable::rowCount(const QModelIndex &parent) const
{
    return pDataModel->Count();
}

int DataTable::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QVariant DataTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= pDataModel->Count() || index.row() < 0)
        return QVariant();

    if(role == Qt::CheckStateRole)
    {
        if(index.column() == 0)
        {
            if(index.row() == 0)
                return Qt::Checked;
            else
                return Qt::Unchecked;
        }
        else
            return QVariant();
    }

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
                return pDataModel->StructName(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant DataTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "DATA NAME";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool DataTable::setData(const QModelIndex & index, const QVariant & value, int role)
{

//    if(!index.isValid())
//        return false;

//    if (index.row() >= pFieldList->FieldCount() || index.row() < 0)
//        return false;

//    if(role == Qt::EditRole)
//    {
//        if(!value.toString().isEmpty())
//        {
//            switch(index.column())
//            {
//            case 0:
//                pFieldList->ChangeName(index.row(), (value.toString()).toStdString());
//                break;

//            case 1:
//                pFieldList->ChangeType(index.row(), (value.toString()).toStdString());
//                break;
//            }
//        }
//    }

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column()==0)
//            pFieldList->ChangeToBeImported(index.row(), value.toBool());
//    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags DataTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

//bool DataTable::removeRows(int row, int count, const QModelIndex & parent)
//{
//   beginRemoveRows(parent, row, row + count - 1);

// //   //na razie tylko jeden wiersz ma być od row do row+count
// //   pFieldList->RemoveField(row);

//   endRemoveRows();

//   return true;
//}

//bool DataTable::insertRows(int row, int count, const QModelIndex & parent)
//{
//    beginInsertRows(parent, row, row + count - 1);

// //    pFieldList->AddNewField("VAR", "float", true);

//    endInsertRows();

//    return true;
//}

void DataTable::AddData(const messiah::DataStructure & dataStructure)
{
    pDataModel->Add(dataStructure);
    emit layoutChanged();
    emit rowAdded(index(rowCount(QModelIndex())-1, 0));
}

void DataTable::RemoveData(const QModelIndex & index)
{
    pDataModel->Remove(index.row());
    emit layoutChanged();
}

FieldTable::FieldTable(messiah::DataStructure* dataStructure, QObject * parent)
    : QAbstractTableModel(parent), pDataStructure(dataStructure)
{
}

FieldTable::~FieldTable()
{

}

int FieldTable::rowCount(const QModelIndex &parent) const
{
    return pDataStructure->Count();
}

int FieldTable::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant FieldTable::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= pDataStructure->Count() || index.row() < 0)
        return QVariant();

    if(role == Qt::DisplayRole  || role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
                return pDataStructure->VarName(index.row()).c_str();

            case 1:
                return pDataStructure->VarType(index.row()).c_str();

            default:
                return QVariant();
        }
    }

    return QVariant();
}


QVariant FieldTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section)
        {
            case 0:
                return "VARIABLE";

            case 1:
                return "DATA TYPE";

            default:
                return QVariant();
        }
    }

    return QVariant();
}


bool FieldTable::setData(const QModelIndex & index, const QVariant & value, int role)
{
//    if(!index.isValid())
//        return false;

//    if (index.row() >= pFieldList->FieldCount() || index.row() < 0)
//        return false;

//    if(role == Qt::EditRole)
//    {
//        if(!value.toString().isEmpty())
//        {
//            switch(index.column())
//            {
//            case 0:
//                pFieldList->ChangeName(index.row(), (value.toString()).toStdString());
//                break;

//            case 1:
//                pFieldList->ChangeType(index.row(), (value.toString()).toStdString());
//                break;
//            }
//        }
//    }

//    if(role == Qt::CheckStateRole)
//    {
//        if(index.column()==0)
//            pFieldList->ChangeToBeImported(index.row(), value.toBool());
//    }

    emit dataChanged(index,index);
    return true;
}

Qt::ItemFlags FieldTable::flags(const QModelIndex & index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |  Qt::ItemIsEditable;
}

void FieldTable::AddField(messiah::FieldInfo *pFieldInfo)
{
//    pFieldList->AddNewField(*pFieldInfo);
    emit layoutChanged();
}

void FieldTable::RemoveField(const QModelIndex & index)
{
//    pRunPlan->RemoveRun(index.row());
    emit layoutChanged();
}
