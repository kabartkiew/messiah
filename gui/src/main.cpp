#include <QApplication>
#include <QSettings>

#include "global.h"
#include "include/settings.h"
#include "mainwindow.h"

void SetApplication(QApplication *app);

int main(int argc, char *argv[])
{
    QApplication::setStyle("Fusion");
    QApplication app(argc, argv);
    SetApplication(&app);

    Settings::Create();

    MainWindow mw("mainwindow");
    mw.show();

    return app.exec();
}

void SetApplication(QApplication *app)
{
    app -> setOrganizationName(ORGANIZATION_NAME);
    app -> setApplicationName(APPLICATION_NAME);
}
