#include <QVariant>

#include "qmywindow.h"
#include "include\settings.h"

QMyWindow::QMyWindow(QString group, QWidget * parent, Qt::WindowFlags flags)
    :   QMainWindow(parent, flags), SettingsGroup(group)
{

}

void QMyWindow::WriteSettings()
{
    Settings::Write(SettingsGroup + "/pos", pos());
    Settings::Write(SettingsGroup + "/size", size());
}

void QMyWindow::ReadSettings()
{
    QPoint pos = Settings::Read(SettingsGroup + "/pos").toPoint();
    move(pos);

    QSize size = Settings::Read(SettingsGroup + "/size").toSize();
    resize(size);
}
