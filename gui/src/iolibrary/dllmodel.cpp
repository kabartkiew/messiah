#include <QMessageBox>
#include <QFile>
#include <QTextStream>

#include "dllmodel.h"
#include "model.h"
#include "cppdllmodel.h"
#include "other/proccessdll.h"


DllModel::DllModel() : pLibModel(0), newModel(true)
{
    pModel = new messiah::Model;
}

DllModel::DllModel(QString FileName) : pLibModel(0), pModel(0)
{
    Load(FileName);
}

DllModel::~DllModel()
{
    Free();
    if(newModel) delete pModel;
}

messiah::Model *DllModel::GetModel()
{
    return pModel;
}

//Do not Load two times without first Free!
void DllModel::Load(QString FileName)
{
    delete pLibModel;
    pLibModel = new QLibrary(FileName);

    if(pLibModel->load())
    {
        typedef messiah::Model* (__cdecl *class_factory_new_model)();
        class_factory_new_model factory_function_new_model =
                  (class_factory_new_model) pLibModel->resolve("NewModel");

        if(factory_function_new_model)
        {
            if(newModel) delete pModel;
            pModel = factory_function_new_model();
            newModel = false;
        }
        else
        {
            throw "Couldn't read the model";
        }
    }
    else
    {
        throw "Couldn't open the model"; // or pLibModel->errorString();
    }
}

void DllModel::Free()
{
    if(pLibModel)
    {
        typedef void (__cdecl *class_factory_free_model)(messiah::Model* instance);
        class_factory_free_model factory_function_free_model =
                (class_factory_free_model) pLibModel->resolve("FreeModel");

        if(factory_function_free_model)
        {
            factory_function_free_model(pModel);
            pModel = 0;
        }
        pLibModel->unload();
        delete pLibModel;
        pLibModel = 0;
    }
}

messiah::Model *DllModel::Save(QString FileName, bool DeleteTempFiles)
{
    QString newFileName = FileName;
    QString cppfilename =
            newFileName.replace(newFileName.lastIndexOf(".") + 1, newFileName.length()-newFileName.lastIndexOf("."), "cpp");

    QFile cppfile(cppfilename);
    if(!cppfile.open(QIODevice::WriteOnly | QIODevice::Text))
                throw "Failed to open file for writing.";

    QTextStream stream(&cppfile);
    CppDllModel cppDllModel(pModel);
    stream << QString::fromStdString(cppDllModel.TextStream());
    cppfile.close();

    Free();

    if(CreateDll(cppfilename.toStdString(), DeleteTempFiles))
        throw "Failed to save model.";

    Load(FileName);

    return pModel;
}

