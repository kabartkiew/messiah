#include "ui_mainwindow.h"

void Ui::MainWindow::setupUi(QMainWindow *MainWindow)
{
    if (MainWindow->objectName().isEmpty())
        MainWindow->setObjectName(QStringLiteral("MainWindow"));

    actionNewPlan = new QAction(MainWindow);
    actionNewPlan->setObjectName(QStringLiteral("actionNewPlan"));
    actionLoadPlan = new QAction(MainWindow);
    actionLoadPlan->setObjectName(QStringLiteral("actionLoadPlan"));
    actionExit = new QAction(MainWindow);
    actionExit->setObjectName(QStringLiteral("actionExit"));

    actionNewModel = new QAction(MainWindow);
    actionNewModel->setObjectName(QStringLiteral("actionNewModel"));
    actionOpenModel = new QAction(MainWindow);
    actionOpenModel->setObjectName(QStringLiteral("actionOpenModel"));

    actionDataImport = new QAction(MainWindow);
    actionDataImport->setObjectName(QStringLiteral("actionDataImport"));
    actionDataPreview = new QAction(MainWindow);
    actionDataPreview->setObjectName(QStringLiteral("actionDataPreview"));

    actionOptions = new QAction(MainWindow);
    actionOptions->setObjectName(QStringLiteral("actionOptions"));

    actionInfo = new QAction(MainWindow);
    actionInfo->setObjectName(QStringLiteral("actionInfo"));

    centralWidget = new QWidget(MainWindow);
    centralWidget->setObjectName(QStringLiteral("centralWidget"));
    gridLayout = new QGridLayout(centralWidget);
    gridLayout->setSpacing(6);
    gridLayout->setContentsMargins(11, 11, 11, 11);
    gridLayout->setObjectName(QStringLiteral("gridLayout"));
    gridLayout->setContentsMargins(0, 0, 0, 0);
    mdiArea = new QMdiArea(centralWidget);
    mdiArea->setObjectName(QStringLiteral("mdiArea"));

    gridLayout->addWidget(mdiArea, 0, 0, 1, 1);

    MainWindow->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(MainWindow);
    menuBar->setObjectName(QStringLiteral("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 800, 20));
    menuPlan = new QMenu(menuBar);
    menuPlan->setObjectName(QStringLiteral("menuPlan"));
    menuModel = new QMenu(menuBar);
    menuModel->setObjectName(QStringLiteral("menuModel"));
    menuData = new QMenu(menuBar);
    menuData->setObjectName(QStringLiteral("menuData"));
    menuResults = new QMenu(menuBar);
    menuResults->setObjectName(QStringLiteral("menuResults"));
    menuTools = new QMenu(menuBar);
    menuTools->setObjectName(QStringLiteral("menuTools"));
    menuHelp = new QMenu(menuBar);
    menuHelp->setObjectName(QStringLiteral("menuHelp"));
    menuLanguage = new QMenu(menuBar);
    menuLanguage->setObjectName(QStringLiteral("menuLanguage"));

    MainWindow->setMenuBar(menuBar);
    mainToolBar = new QToolBar(MainWindow);
    mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
    MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
    statusBar = new QStatusBar(MainWindow);
    statusBar->setObjectName(QStringLiteral("statusBar"));
    MainWindow->setStatusBar(statusBar);

    menuBar->addAction(menuPlan->menuAction());
    menuBar->addAction(menuModel->menuAction());
    menuBar->addAction(menuData->menuAction());
    menuBar->addAction(menuResults->menuAction());
    menuBar->addAction(menuTools->menuAction());
    menuBar->addAction(menuHelp->menuAction());
    menuBar->addAction(menuLanguage->menuAction());
    menuPlan->addAction(actionNewPlan);
    menuPlan->addAction(actionLoadPlan);
    menuPlan->addSeparator();
    menuPlan->addAction(actionExit);
    menuModel->addAction(actionNewModel);
    menuModel->addAction(actionOpenModel);
    menuData->addAction(actionDataImport);
    menuData->addAction(actionDataPreview);
    menuTools->addAction(actionOptions);
    menuHelp->addAction(actionInfo);

    retranslateUi(MainWindow);

    QMetaObject::connectSlotsByName(MainWindow);
}

void Ui::MainWindow::retranslateUi(QMainWindow *MainWindow)
{
    MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MESSIAH", 0));
    menuPlan->setTitle(QApplication::translate("MainWindow", "Plan", 0));
    actionNewPlan->setText(QApplication::translate("MainWindow", "New", 0));
    actionLoadPlan->setText(QApplication::translate("MainWindow", "Load", 0));
    actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
    menuModel->setTitle(QApplication::translate("MainWindow", "Model", 0));
    actionNewModel->setText(QApplication::translate("MainWindow", "New", 0));
    actionOpenModel->setText(QApplication::translate("MainWindow", "Open", 0));
    menuData->setTitle(QApplication::translate("MainWindow", "Data", 0));
    actionDataImport->setText(QApplication::translate("MainWindow", "Import", 0));
    actionDataPreview->setText(QApplication::translate("MainWindow", "Preview", 0));
    menuResults->setTitle(QApplication::translate("MainWindow", "Results", 0));
    menuTools->setTitle(QApplication::translate("MainWindow", "Tools", 0));
    actionOptions->setText(QApplication::translate("MainWindow", "Options", 0));
    menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0));
    menuLanguage->setTitle(QApplication::translate("MainWindow", "Language", 0));
    actionInfo->setText(QApplication::translate("MainWindow", "About Messiah", 0));
}
