#include "rundetails.h"
#include "ui_rundetails.h"

RunDetails::RunDetails(QWidget *parent, QString testTitle) :
    QWidget(parent),
    ui(new Ui::RunDetails)
{
    ui->setupUi(this);

    setWindowTitle(testTitle);
}

RunDetails::~RunDetails()
{
    delete ui;
}
