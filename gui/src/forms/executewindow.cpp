#include "projection.h"

#include "executewindow.h"
#include "ui_executewindow.h"
#include "progresstable.h"
#include "progressdelegate.h"
#include "buttondelegate.h"
#include "other/proccessdll.h"
#include "model.h"

#include "dllmodel.h"
#include "cppdllproduct.h"

#include <QMessageBox>
#include <QFile>
#include <QTextStream>

ExecuteWindow::ExecuteWindow(QWidget *parent, messiah::RunPlan *runplan, QString &resultPath,
                             bool DeleteTempFiles) :
    ChildWindow("executewindow", parent),
    ui(new Ui::ExecuteWindow),
    pProgressModel(0),
    bDeleteTempFiles(DeleteTempFiles)
{
    ui->setupUi(this);
    ReadSettings();

    setWindowTitle(tr("Calculating..."));
    ui->label->setText(resultPath);

    if(!runplan)
    {
        //change - do not create an empty model!
        pProgressModel = new ProgressTable(this);
    }
    else
    {
        pProgressModel = new ProgressTable(this, runplan);


        if(pProgressModel){
            ui->treeView->setModel(pProgressModel);

            //add - find the number of column by type in the settings
            ProgressDelegate* delegateProgress = new ProgressDelegate(ui->treeView);
            ui->treeView->setItemDelegateForColumn(1,delegateProgress);
            ButtonDelegate* delegateButton = new ButtonDelegate(ui->treeView);
            ui->treeView->setItemDelegateForColumn(3,delegateButton);
        }

        messiah::Projection proj;

        //add a check if there exists such column
        int start_date_column = runplan->AttributeNo("ProjectionStart");
        int proj_term_column = runplan->AttributeNo("ProjectionTerm");
        int data_path_column = runplan->AttributeNo("Data");

        for(int runno = 0; runno < runplan->RunCount(); runno++)
        {
            if(runplan->InfoRunMarked(runno))
            {
                std::string start = runplan->InfoValue(runno,start_date_column);
                std::string start_year = start.substr(0,4);
                std::string start_month = start.substr(5,2);

//                proj.setStartDate(std::stoi(start_year),std::stoi(start_month));      <- msvc
                proj.setStartDate(atoi(start_year.c_str()), atoi(start_month.c_str()));     //<-mingw

                std::string proj_term = runplan->InfoValue(runno, proj_term_column);

//                proj.setProjTerm(std::stoi(proj_term));   <- msvc
                proj.setProjTerm(atoi(proj_term.c_str()));  //<-mingw

                std::string log_path = resultPath.toStdString() + "/log_file.txt";

                std::string data_path = runplan->InfoValue(runno,data_path_column);

                //add a check if data_path is correct!
                std::string data_file = data_path.substr(data_path.find_last_of("\\")+1);
                data_file.erase(data_file.find_last_of("."));

                std::string result_path = resultPath.toStdString() + "/" + data_file + "_RESULTS.csv";

                int model_path_column = runplan->AttributeNo("Model");
                std::string model_path = runplan->InfoValue(runno, model_path_column);


                model_path = PrepareModelExe(QString::fromStdString(model_path)).toStdString();

                int iProgress = 0;
                if(model_path!="")
                {
                    proj.perform(model_path, data_path, log_path, result_path);
                    iProgress = 100;
                }

                pProgressModel->UpdateProgress(pProgressModel->index(runno, 0), iProgress);
            }
        }
    }
}

ExecuteWindow::~ExecuteWindow()
{
    WriteSettings();

    delete pProgressModel;
    delete ui;
}

QString ExecuteWindow::PrepareModelExe(QString FileName)
{
    QString cppfilename = FileName;
    cppfilename = cppfilename.replace(cppfilename.lastIndexOf("."), cppfilename.length()-cppfilename.lastIndexOf("."), "_exe.cpp");

    QFile cppfile(cppfilename);
    if(!cppfile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, tr("Error"), tr("Failed to open file for writing."), QMessageBox::Ok);
        return QString();
    }

    QTextStream stream(&cppfile);

    DllModel* pDllModel = 0;
    try
    {
        pDllModel = new DllModel(FileName);
    }
    catch(const char* msg)
    {
        QMessageBox::critical(this, tr("Error"), msg, QMessageBox::Ok);
    }

    if(pDllModel)
    {
        CppDllProduct cppProduct(pDllModel->GetModel());
        delete pDllModel;
        stream << cppProduct.TextStream().c_str();
        cppfile.close();

        if(CreateDll(cppfilename.toStdString(), bDeleteTempFiles))
        {
            QMessageBox::critical(this, tr("Error"), tr("Failed to compile model."), QMessageBox::Ok);
            return QString();
        }
        else
        {
            QMessageBox::information(this, tr("OK"), tr("Model compiled."), QMessageBox::Ok);
        }

        QString dllfilename = cppfilename;
        dllfilename = dllfilename.replace(dllfilename.lastIndexOf("."), dllfilename.length()-dllfilename.lastIndexOf("."), ".dll");

        return dllfilename;
    }
    else
        return "";
}
