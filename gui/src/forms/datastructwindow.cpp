#include <QFileDialog>
#include <QMessageBox>

#include "csvdata.h"
#include "prophetdata.h"
#include "importdata.h"
#include "model.h"

#include "datastructwindow.h"
#include "ui_datastructwindow.h"
#include "modelwindow.h"

#include <string>

unsigned char DataStructWindow::import_types_count = 2;
std::string DataStructWindow::import_types[] = {"csv", "Prophet"};

DataStructWindow::DataStructWindow(DataTable *parent_data_table, QWidget *parent) :
    ChildWindow("dataStructwindow", parent),
    ui(new Ui::DataStructWindow),
    lastSelectedInputDir(QString()),
    pParentDataTable(parent_data_table)
{
    ui->setupUi(this);
    ReadSettings();

    pImportData = new messiah::ImportData;
    pImportType = 0;
    pFieldsTableModel = 0;

    QStringList typeList;
    for(int i =0; i < import_types_count; i++)
        typeList << import_types[i].c_str();
    ui->comboBoxImportTypes->addItems(typeList);

    pFieldsTableModel = new ImportFieldsTable(pImportData->pFieldList);
    ui->treeViewPreview->setModel(pFieldsTableModel);

    ui->lineEditStructureName->setPlaceholderText(tr("structure_name"));

    QObject::connect(this, SIGNAL(newModelWindow(messiah::ImportData*)),
                     parent, SLOT(slotNewModelWindow(messiah::ImportData*)));
}

DataStructWindow::~DataStructWindow()
{
    WriteSettings();
    delete pImportType;
    delete pImportData;
    delete pFieldsTableModel;
    delete ui;
}

void DataStructWindow::on_comboBoxImportTypes_currentTextChanged(const QString &arg1)
{
    if(arg1 == "csv")
    {
        delete pImportType;
        pImportType = new messiah::CsvData(pImportData);
    }
    else if(arg1=="Prophet")
    {
        delete pImportType;
        pImportType = new messiah::ProphetData(pImportData);
    }
}

void DataStructWindow::on_pushButtonBrowseInput_clicked()
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose the file to be imported..."), lastSelectedInputDir);

    if(!fileName.isNull())
    {
        lastSelectedInputDir = fileName;
        ui->lineEditInputPath->setText(fileName);
        Preview();
    }
}

void DataStructWindow::Preview()
{
    QFileInfo fileInfo((pImportData->input_path).c_str());
    if(fileInfo.exists() && fileInfo.isFile())
    {
        if(pImportType)
        {
            if(!pImportType->GetFieldList())
            {
                delete pFieldsTableModel;
                pFieldsTableModel = new ImportFieldsTable(pImportType->pImportData->pFieldList);
                ui->treeViewPreview->setModel(pFieldsTableModel);
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(tr("The path to a file is incorrect."));
        msgBox.exec();
    }
}

void DataStructWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void DataStructWindow::on_lineEditInputPath_textChanged()
{
    QFileInfo inputFilePath(ui->lineEditInputPath->text());

    if(inputFilePath.exists() && inputFilePath.isFile())
    {
        std::string inputpath = (inputFilePath.absoluteFilePath()).toStdString();
        pImportData->input_path = inputpath;
//        string::size_type p = inputpath.rfind('/');
//        if(p!=string::npos)
//        {
//            inputpath.erase(p,string::npos);
//            inputpath.append("_messiah");
//            ui->lineEditOutputPath->setText(inputpath.c_str());
//        }
    }
    else
    {
        pImportData->input_path = "";
    }

    delete pFieldsTableModel;
    pFieldsTableModel = 0;
    ui->treeViewPreview->reset();

    pImportData->pFieldList = 0;
}

void DataStructWindow::on_pushButtonDelete_clicked()
{
    if(pFieldsTableModel)
    {
        QItemSelectionModel* selection = ui->treeViewPreview->selectionModel();
        QModelIndexList list = selection->selectedIndexes();
        foreach(QModelIndex index, list)
        {
            if(index.column()==0)
            {
                pFieldsTableModel->DeleteField(index);
                ui->treeViewPreview->reset();
            }
        }
    }
}

void DataStructWindow::on_pushButtonAdd_clicked()
{
    pFieldsTableModel->AddField("VAR","float", true);
}

void DataStructWindow::on_lineEditStructureName_textChanged() //add - check if the text is correct
{
    //add a check if there are no spaces
    pImportData->pFieldList->Name = (ui->lineEditStructureName->text()).toStdString();
}

void DataStructWindow::on_pushButtonOK_clicked()
{
    if(ui->lineEditStructureName->text().isEmpty())
        ui->lineEditStructureName->setText(ui->lineEditStructureName->placeholderText());

    if(pParentDataTable)
        pParentDataTable->AddData(*pImportData->pFieldList);
    else
    {
        emit newModelWindow(pImportData);
    }

    this->parentWidget()->close();
}

void DataStructWindow::on_parentModel_closed()
{
    pParentDataTable = 0;
}
