#include <QMdiSubWindow>
#include <QFileDialog>
#include <QMessageBox>

#include "global.h"
#include "include/settings.h"
#include "qmywindow.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "runplanwindow.h"
#include "modelwindow.h"
#include "importwindow.h"
#include "datawindow.h"

MainWindow::MainWindow(QString setgroup, QWidget *parent) :
    QMyWindow(setgroup, parent),
    ui(new Ui::MainWindow),
    currLang(""),
    langPath(":languages")
{
    QIcon ico(":messiah-ico");
    setWindowIcon(ico);

    ui->setupUi(this);

    QString lang = Settings::Read("language").toString();
    loadLanguage(lang);
    createLanguageMenu();

    ReadSettings();
}

MainWindow::~MainWindow()
{
    WriteSettings();
    Settings::Write("language", currLang);
    delete ui;
}

QMdiSubWindow* MainWindow::addSubWindow(QWidget* widget)
{
    return ui->mdiArea->addSubWindow(widget);
}

void MainWindow::changeEvent(QEvent* event)
{
    if(event != 0)
    {
        switch(event->type())
        {
            case QEvent::LanguageChange:
                ui->retranslateUi(this);
                break;

            case QEvent::LocaleChange:
            {
                QString locale = QLocale::system().name();
                locale.truncate(locale.lastIndexOf('_'));
                loadLanguage(locale);
            }
            break;
        }
    }
    QMainWindow::changeEvent(event);
}

void MainWindow::on_actionNewPlan_triggered()
{
    RunPlanWindow* pRunPlanWindow = new RunPlanWindow(this);
    pRunPlanWindow->show();
}

void MainWindow::on_actionLoadPlan_triggered()
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose the run plan to load..."));
    if(!fileName.isNull())
    {
        RunPlanWindow* pRunPlanWindow = new RunPlanWindow(this, &fileName);
        pRunPlanWindow->show();
    }
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionDataImport_triggered()
{
    ImportWindow* pImportWindow = new ImportWindow(this);
    pImportWindow->show();
}

void MainWindow::on_actionDataPreview_triggered()
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose the data file to preview..."));
    if(!fileName.isNull())
    {
        DataWindow* pDataWindow = new DataWindow(this, &fileName);
        pDataWindow->show();
    }
}

void MainWindow::on_actionNewModel_triggered()
{
    ModelWindow* pModelWindow = new ModelWindow(this);
    pModelWindow->show();
}

void MainWindow::slotNewModelWindow(messiah::ImportData *pImportData)
{
    ModelWindow* pModelWindow = new ModelWindow(pImportData, this);
    pModelWindow->show();
}

void MainWindow::slotNewModelWindow(messiah::Function *pFunction)
{
    ModelWindow* pModelWindow = new ModelWindow(pFunction, this);
    pModelWindow->show();
}

void MainWindow::slotNewModelWindow(messiah::FunctionT *pFunctionT)
{
    ModelWindow* pModelWindow = new ModelWindow(pFunctionT, this);
    pModelWindow->show();
}

void MainWindow::on_actionOpenModel_triggered()
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose the model to open..."));
    if(!fileName.isNull())
    {
        try
        {
            ModelWindow* pModelWindow = new ModelWindow(this, &fileName);
            pModelWindow->show();
        }
        catch(const char* msg)
        {
            QMessageBox::critical(this, "Error", msg, QMessageBox::Ok);
        }
    }
}

void MainWindow::slotLanguageChanged(QAction* action)
{
    if(action != 0)
    {
        loadLanguage(action->data().toString());
    }
}

void switchTranslator(QTranslator& translator, const QString& filename)
{
    qApp->removeTranslator(&translator);

    if(translator.load(filename))
        qApp->installTranslator(&translator);
}

void MainWindow::loadLanguage(const QString& rLanguage)
{
    if(currLang != rLanguage)
    {
        currLang = rLanguage;
        QLocale locale = QLocale(currLang);
        QLocale::setDefault(locale);
        QString languageName = QLocale::languageToString(locale.language());
        QString langFileName = QString("%1/qm/%2.qm").arg(langPath).arg(QString("messiah_%1").arg(rLanguage));
        switchTranslator(translator, langFileName);
        ui->statusBar->showMessage(tr("Current Language changed to %1").arg(languageName));
    }
}

void MainWindow::createLanguageMenu(void)
{
    QActionGroup* langGroup = new QActionGroup(ui->menuLanguage);
    langGroup->setExclusive(true);

    connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));

    QDir dir(langPath);
    dir.cd("qm");
    QStringList fileNames = dir.entryList(QStringList("messiah_*.qm"));

    for (int i = 0; i < fileNames.size(); ++i)
    {
        QString locale;
        locale = fileNames[i]; // "messiah_pl.qm"
        locale.truncate(locale.lastIndexOf('.')); // "messiah_pl"
        locale.remove(0, locale.indexOf('_') + 1); // "pl"

        QString lang = QLocale::languageToString(QLocale(locale).language()); // "Polish"
        QIcon ico(QString("%1/png/%2.png").arg(langPath).arg(locale));

        QAction *action = new QAction(ico, lang, this);
        action->setCheckable(true);
        action->setData(locale);

        ui->menuLanguage->addAction(action);
        langGroup->addAction(action);

        if(locale == currLang)
        {
            action->setChecked(true);
        }
    }
}
