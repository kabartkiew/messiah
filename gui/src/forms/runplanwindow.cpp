#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QFileDialog>

#include "runplanwindow.h"
#include "ui_runplanwindow.h"
#include "runplantable.h"
#include "datedelegate.h"
#include "include/settings.h"
#include "executewindow.h"
#include "executedialog.h"


RunPlanWindow::RunPlanWindow(QWidget *parent, QString *xmlfilepath) :
    ChildWindow("runplanwindow", parent),
    ui(new Ui::RunPlanWindow)
{
    ui->setupUi(this);
    ReadSettings();

    pRunPlan = 0;
    pPlanModel = 0;

    QDomDocument* doc = 0;

    if(!xmlfilepath)
    {
        FileName = QString::null;
        doc = NewDocument();
    }
    else
    {
        FileName = *xmlfilepath;
        QFile file(FileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::critical(this, tr("Error"), tr("Couldn't open the file."), QMessageBox::Ok);
            return;
        }
        else
        {
            doc = new QDomDocument;
            if (!doc->setContent(&file))
            {
                QMessageBox::critical(this, tr("Error"), tr("Couldn't set the content of the file."), QMessageBox::Ok);
                doc = NewDocument();
                file.close();
            }
            else
            {
                setWindowTitle(FileName);
                file.close();
            }
        }
    }

    ReadRunPlan(doc);
    if(pRunPlan)
    {
        pPlanModel = new RunPlanTable(pRunPlan,ui->treeView);

        ui->treeView->setModel(pPlanModel);

        //in future - find the number of column by type in the settings
        DateDelegate* delegate = new DateDelegate(ui->treeView);
        ui->treeView->setItemDelegateForColumn(3,delegate);
    }

    delete doc;
}

void RunPlanWindow::ReadSettings()
{
    RootName = Settings::Read(SettingsGroup + "/plantable/rootname").toString();
    ElementName = Settings::Read(SettingsGroup + "/plantable/elementname").toString();
    Columns = Settings::ReadArray(SettingsGroup + "/plantable/columns");
}

RunPlanWindow::~RunPlanWindow()
{
    WriteSettings();
    delete pPlanModel;
    delete pRunPlan;
    delete ui;
}

QDomDocument* RunPlanWindow::NewDocument()
{
    QDomDocument* newdoc = new QDomDocument;

    QDomElement root = newdoc->createElement(RootName);
    newdoc->appendChild(root);
    QDomElement run;
    QDomElement node;
    QDomText text;
    run = newdoc->createElement(ElementName);
    root.appendChild(run);

    for(int j = 0; j < Columns.count(); j++)
    {
        node = newdoc->createElement(Columns[j]);
        run.appendChild(node);
        text = newdoc->createTextNode("");
        node.appendChild(text);
    }

    return newdoc;
}

void RunPlanWindow::ReadRunPlan(QDomDocument* doc)
{
    QDomElement root = doc->documentElement();
    if(root.tagName() != RootName)
    {
        QMessageBox::critical(this, tr("Error"), tr("No a RunPlan index in a file."), QMessageBox::Ok);
    }
    else
    {
        pRunPlan = new messiah::RunPlan();
        QDomNode child = root.firstChild();
        while(!child.isNull())
        {
            if(child.toElement().tagName() == ElementName)
            {
                pRunPlan->AddNewRun();
                QDomElement e = child.toElement();
                if(!e.isNull())
                {
                    for(int i = 0; i < Columns.count(); i++)
                    {
                        QDomNode node = e.namedItem(Columns[i]);
                        pRunPlan->AddAttribute(Columns[i].toStdString());
                        if(!node.isNull())
                            pRunPlan->PutToRunInfo((node.toElement().text()).toStdString(),
                                                   pRunPlan->RunCount() - 1 ,
                                                   node.toElement().tagName().toStdString());
                        pRunPlan->MarkRunInfo(false, pRunPlan->RunCount() - 1);
                    }
                }
            }
            child = child.nextSibling();
        }
    }
}

void RunPlanWindow::on_pushButtonAdd_clicked()
{
    pPlanModel->AddRun();
}

void RunPlanWindow::on_pushButtonDelete_clicked()
{
    //QList<QModelIndex> selected = ui->treeView->selectionModel()->selectedRows();
    pPlanModel->DeleteRun(ui->treeView->selectionModel()->selectedRows().first());
}

void RunPlanWindow::ReadDocument(QDomDocument &doc)
{
    QDomElement root = doc.createElement(RootName);
    doc.appendChild(root);
    QDomElement run;
    QDomElement node;
    QDomText text;
    for(int i = 0; i < pRunPlan->RunCount(); i++)
    {
        run = doc.createElement(ElementName);
        root.appendChild(run);
        for(int j = 0; j < pRunPlan->AttributeCount(); j++)
        {
            node = doc.createElement(pRunPlan->Attribute(j).c_str());
            run.appendChild(node);
            text = doc.createTextNode(pRunPlan->InfoValue(i,j).c_str());
            node.appendChild(text);
        }
    }
    QDomNode declaration(doc.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"") );
    doc.insertBefore( declaration, doc.firstChild() );
}

void RunPlanWindow::on_pushButtonSave_clicked()
{
    if(FileName.isNull())
    {
        FileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                   "",
                                   tr("XML files (*.xml)"));
    }
    Save();
}

void RunPlanWindow::on_pushButtonSaveAs_clicked()
{
    FileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "",
                               tr("XML files (*.xml)"));
    Save();
}

void RunPlanWindow::Save()
{
    QFile file(FileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, tr("Error"), tr("Failed to open file for writing."), QMessageBox::Ok);
        return;
    }
    QTextStream stream(&file);

    QDomDocument doc;
    ReadDocument(doc);
    stream << doc.toString();
    file.close();
    setWindowTitle(FileName);
    QMessageBox::information(this, tr("OK"), tr("Run plan saved."),QMessageBox::Ok);
    //in future
    //first save data to another file, check if save was successful and then replace original file with a copy.
}

void RunPlanWindow::on_pushButtonExecute_clicked()
{

    if(pRunPlan->RunCount())
    {
        ResultFolder =
                QFileDialog::getExistingDirectory(this, tr("Choose a folder where to save the results...")); //,
                                                  //lastSelectedOutputDir);
        if(!ResultFolder.isNull())
        {
            //lastSelectedOutputDir = folderName;
            //ui->lineEditResultsPath->setText(folderName);
        }

        ExecuteDialog* pExecuteDialog = new ExecuteDialog(this);
        if(pExecuteDialog->exec())
        {
            Execute(pExecuteDialog->DeleteTemp());
        }
        delete pExecuteDialog;
    }
}

void RunPlanWindow::Execute(bool DeleteTempFiles)
{
    ExecuteWindow* pExecuteWindow =
        new ExecuteWindow(
            this->parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget(),
            pRunPlan, ResultFolder, DeleteTempFiles);
    pExecuteWindow->show();
}
