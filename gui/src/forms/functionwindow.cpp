#include "childwindow.h"
#include "functionwindow.h"
#include "ui_functionwindow.h"

FunctionWindow::FunctionWindow(FunctionTable *parent_function_table, QWidget *parent) :
    ChildWindow("functionwindow", parent),
    ui(new Ui::FunctionWindow),
    pParentFunctionTable(parent_function_table)
{
    ui->setupUi(this);
    ui->comboBoxSum->addItems(listResultType);

    pFunction = new messiah::Function;

    QObject::connect(this, SIGNAL(newModelWindow(messiah::Function*)),
                     parent, SLOT(slotNewModelWindow(messiah::Function*)));
}

FunctionWindow::~FunctionWindow()
{
    delete pFunction;
    delete ui;
}

void FunctionWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void FunctionWindow::on_pushButtonOK_clicked()
{
//    if(ui->lineEditStructureName->text().isEmpty())
//        ui->lineEditStructureName->setText(ui->lineEditStructureName->placeholderText());

    pFunction->Name = ui->lineEditName->text().toStdString();
    pFunction->Type = ui->lineEditType->text().toStdString();
    pFunction->Text = ui->textEdit->toPlainText().toStdString();
    pFunction->ResultType = ui->comboBoxSum->currentText().toStdString();

    if(pParentFunctionTable)
        pParentFunctionTable->AddFunction(*pFunction);
    else
    {
        emit newModelWindow(pFunction);
    }

    this->parentWidget()->close();
}

void FunctionWindow::on_parentModel_closed()
{
    pParentFunctionTable = 0;
}
