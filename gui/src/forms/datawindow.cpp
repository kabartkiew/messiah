#include "datawindow.h"
#include "ui_datawindow.h"
#include "model.h"

#include <QMessageBox>
#include <QLibrary>

DataWindow::DataWindow(QWidget *parent, QString *dllfilepath) :
    ChildWindow("datawindow", parent),
    ui(new Ui::DataWindow),
    pMyData(0),
    pFieldTable(0)
{
    ui->setupUi(this);
    ui->widget->setVisible(false);

    ReadSettings();

    if(!dllfilepath)
    {
//        FileName = QString::null;
//        pModel = new messiah::Model();
    }
    else
    {
        FileName = *dllfilepath;
        LoadData();
        setWindowTitle(FileName);
    }
    DisplayData();
}

DataWindow::~DataWindow()
{
    WriteSettings();
    FreeData();
    delete ui;
}

void DataWindow::FreeData()
{
    delete pFieldTable;
    pFieldTable = 0;

    if(pMyData)
    {
        typedef void (__cdecl *class_factory_free_datastruct)(messiah::DataStructure* instance);
        class_factory_free_datastruct factory_function_free_datastruct =
                (class_factory_free_datastruct) pMyData->resolve("FreeDataStructure");

        if(factory_function_free_datastruct)
        {
            factory_function_free_datastruct(pDataStruct);
            pDataStruct = 0;
        }
        pMyData->unload();
        delete pMyData;
        pMyData = 0;
    }
    else
    {
        delete pDataStruct;
        pDataStruct = 0;
    }
}

void DataWindow::LoadData()
{
    pMyData = new QLibrary(FileName);
    if(pMyData->load())
    {
        typedef messiah::DataStructure* (__cdecl *class_factory_new_datastruct)();
        class_factory_new_datastruct factory_function_new_datastruct =
                  (class_factory_new_datastruct) pMyData->resolve("NewDataStructure");

        if(factory_function_new_datastruct)
            pDataStruct = factory_function_new_datastruct();
        else
            QMessageBox::critical(this, tr("Error"), tr("Couldn't read the data."), QMessageBox::Ok);
    }
    else
    {
//        QMessageBox::critical(this, "Error", "Couldn't open the file", QMessageBox::Ok);
        QMessageBox::critical(this, "Error", pMyData->errorString(), QMessageBox::Ok);
    }
}

void DataWindow::DisplayData()
{
    pFieldTable = new FieldTable(pDataStruct);

    ui->treeViewPreview->setModel(pFieldTable);

    ui->lineEditStructureName->setText(QString(pDataStruct->Name.c_str()));
}

void DataWindow::on_pushButtonDelete_clicked()
{

}

void DataWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void DataWindow::on_pushButtonSaveAs_clicked()
{

}

void DataWindow::on_pushButtonSave_clicked()
{

}
