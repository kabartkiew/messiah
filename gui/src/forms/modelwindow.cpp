#include <QFile>
#include <QMessageBox>
#include <QFileDialog>

#include <string>
#include <algorithm>

#include "modelwindow.h"
#include "ui_modelwindow.h"
#include "model.h"
#include "datastructwindow.h"
#include "importdata.h"
#include "functionwindow.h"
#include "functiontwindow.h"
#include "modelsavedialog.h"


ModelWindow::ModelWindow(QWidget *parent, QString *dllfilepath) :
    ChildWindow("modelwindow", parent),
    ui(new Ui::ModelWindow),
    pModel(0),
    pDllModel(0),
    pDataTable(0),
    pFieldTable(0),
    pValueTable(0),
    pFunctionTable(0),
    pFunctionTTable(0)
{
    ui->setupUi(this);
    ReadSettings();

    if(!dllfilepath)
    {
        CreateNew();
    }
    else
    {
        FileName = *dllfilepath;
        try
        {
            pDllModel = new DllModel(FileName);
            setWindowTitle(FileName);
        }
        catch(const char* msg)
        {
            QMessageBox::critical(this, "Error", msg, QMessageBox::Ok);
            throw "Couldn't open the model.";
        }
    }

    pModel = pDllModel->GetModel();
    CreateTables();
    DisplayModel();
}

ModelWindow::ModelWindow(messiah::ImportData *pImportData, QWidget *parent):
    ChildWindow("modelwindow", parent),
    ui(new Ui::ModelWindow),
    pModel(0),
    pDllModel(0),
    pDataTable(0),
    pFieldTable(0),
    pValueTable(0),
    pFunctionTable(0),
    pFunctionTTable(0)
{
    ui->setupUi(this);
    ReadSettings();

    CreateNew();

    pModel = pDllModel->GetModel();

    CreateTables();
    pDataTable->AddData(*pImportData->pFieldList);

    DisplayModel();
}

ModelWindow::ModelWindow(messiah::Function *pFunction, QWidget *parent):
    ChildWindow("modelwindow", parent),
    ui(new Ui::ModelWindow),
    pModel(0),
    pDllModel(0),
    pDataTable(0),
    pFieldTable(0),
    pValueTable(0),
    pFunctionTable(0),
    pFunctionTTable(0)
{
    ui->setupUi(this);
    ReadSettings();

    CreateNew();

    pModel = pDllModel->GetModel();

    CreateTables();
    pFunctionTable->AddFunction(*pFunction);

    DisplayModel();
}

ModelWindow::ModelWindow(messiah::FunctionT *pFunctionT, QWidget *parent):
    ChildWindow("modelwindow", parent),
    ui(new Ui::ModelWindow),
    pModel(0),
    pDllModel(0),
    pDataTable(0),
    pFieldTable(0),
    pValueTable(0),
    pFunctionTable(0),
    pFunctionTTable(0)
{
    ui->setupUi(this);
    ReadSettings();

    CreateNew();

    pModel = pDllModel->GetModel();

    CreateTables();
    pFunctionTTable->AddFunctionT(*pFunctionT);

    DisplayModel();
}

void ModelWindow::CreateNew()
{
    FileName = QString::null;
    pDllModel = new DllModel();
    ui->textEditDurStartMonth->setPlaceholderText("return 0;");
    ui->textEditProjTermFromZeroMonth->setPlaceholderText("return p_settings->proj_term_month;");
}

ModelWindow::~ModelWindow()
{
    emit ModelClosed();
    WriteSettings();
    EraseModel();
    delete pDllModel;
    delete ui;
}

void ModelWindow::EraseModel()
{
    delete pFunctionTTable;
    pFunctionTTable = 0;
    delete pFunctionTable;
    pFunctionTable = 0;
    delete pValueTable;
    pValueTable = 0;
    delete pFieldTable;
    pFieldTable = 0;
    delete pDataTable;
    pDataTable = 0;
}

void ModelWindow::CreateTables()
{
    pDataTable = new DataTable(&pModel->Data);
    pValueTable = new ValueTable(&pModel->ValueSet);
    pFunctionTable = new FunctionTable(&pModel->FunctionSet);
    pFunctionTTable = new FunctionTTable(&pModel->FunctionTSet);
}

void ModelWindow::DisplayModel()
{
    ui->treeViewDatas->setModel(pDataTable);
    selectionDatas = ui->treeViewDatas->selectionModel();

    ui->treeViewValues->setModel(pValueTable);

    ui->treeViewFunctions->setModel(pFunctionTable);
    selectionFunctions = ui->treeViewFunctions->selectionModel();

    ui->treeViewFunctionsT->setModel(pFunctionTTable);
    selectionFunctionsT = ui->treeViewFunctionsT->selectionModel();

    ui->treeViewDatas->setCurrentIndex(ui->treeViewDatas->model()->index(0,0));
    RefreshFieldView();

    ui->treeViewFunctions->setCurrentIndex(ui->treeViewFunctions->model()->index(0,0));
    RefreshFunctionDefView();

    ui->treeViewFunctionsT->setCurrentIndex(ui->treeViewFunctionsT->model()->index(0,0));
    RefreshFunctionTDefView();

    ui->textEditDurStartMonth->setText(pModel->setDurationStartMonth.c_str());
    ui->textEditProjTermFromZeroMonth->setText(pModel->setProjTermFromZeroMonth.c_str());

    QObject::connect(selectionDatas, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                     this, SLOT(DataStructSelected(QItemSelection,QItemSelection)));

    QObject::connect(pDataTable,
                     SIGNAL(rowAdded(QModelIndex)), this, SLOT(DataStructAdded(QModelIndex)));


    QObject::connect(selectionFunctions, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                     this, SLOT(FunctionSelected(QItemSelection,QItemSelection)));

    QObject::connect(pFunctionTable,
                     SIGNAL(rowAdded(QModelIndex)), this, SLOT(FunctionAdded(QModelIndex)));


    QObject::connect(selectionFunctionsT, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                     this, SLOT(FunctionTSelected(QItemSelection,QItemSelection)));

    QObject::connect(pFunctionTTable,
                     SIGNAL(rowAdded(QModelIndex)), this, SLOT(FunctionTAdded(QModelIndex)));
}

void ModelWindow::DataStructAdded(const QModelIndex & index)
{
    ui->treeViewDatas->setCurrentIndex(index);
}

void ModelWindow::DataStructSelected(const QItemSelection & selected, const QItemSelection & deselected)
{
    RefreshFieldView();
}

void ModelWindow::FunctionAdded(const QModelIndex & index)
{
    ui->treeViewFunctions->setCurrentIndex(index);
}

void ModelWindow::FunctionSelected(const QItemSelection & selected, const QItemSelection & deselected)
{
    RefreshFunctionDefView();
}

void ModelWindow::FunctionTAdded(const QModelIndex & index)
{
    ui->treeViewFunctionsT->setCurrentIndex(index);
}

void ModelWindow::FunctionTSelected(const QItemSelection & selected, const QItemSelection & deselected)
{
    RefreshFunctionTDefView();
}

void ModelWindow::on_pushButtonNew_clicked()
{
    DataStructWindow* pDataStructWindow = new DataStructWindow(pDataTable,
                                             this->parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget());

    QObject::connect(this, SIGNAL(ModelClosed()),
                     pDataStructWindow, SLOT(on_parentModel_closed()));

    pDataStructWindow->show();
}

void ModelWindow::on_pushButtonEdit_clicked()
{

}

void ModelWindow::on_pushButtonDelete_clicked()
{
    delete pFieldTable;
    pFieldTable = 0;
    int rowNo = ui->treeViewDatas->currentIndex().row();
    pDataTable->RemoveData(ui->treeViewDatas->currentIndex());
    int rows = ui->treeViewDatas->model()->rowCount();
    if(rowNo >= rows)
    {
        ui->treeViewDatas->setCurrentIndex(ui->treeViewDatas->model()->index(rowNo-1,0));
    }
    else
    {
        RefreshFieldView();
    }
}

void ModelWindow::RefreshFieldView()
{
    delete pFieldTable;
    pFieldTable = 0;
    if(ui->treeViewDatas->model()->rowCount())
    {
        pFieldTable = new FieldTable(pModel->Data.Structure(ui->treeViewDatas->currentIndex().row()));

        QItemSelectionModel *m = ui->treeViewFields->selectionModel();
        ui->treeViewFields->setModel(pFieldTable);
        delete m;
    }
}

void ModelWindow::on_pushButtonNewValue_clicked()
{
    pValueTable->AddValue("VALUE_NAME", "float");
    ui->treeViewValues->setCurrentIndex(ui->treeViewValues->model()->index(ui->treeViewValues->model()->rowCount()-1,0));
}

void ModelWindow::on_pushButtonDeleteValue_clicked()
{
    int rowNo = ui->treeViewValues->currentIndex().row();
    pValueTable->RemoveValue(ui->treeViewValues->currentIndex());
    int rows = ui->treeViewValues->model()->rowCount();
    if(rowNo >= rows)
    {
        ui->treeViewValues->setCurrentIndex(ui->treeViewValues->model()->index(rowNo-1,0));
    }
    else
    {

    }
}

void ModelWindow::on_pushButtonNewFunction_clicked()
{
    FunctionWindow* pFunctionWindow = new FunctionWindow(pFunctionTable,
                                             this->parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget());

    QObject::connect(this, SIGNAL(ModelClosed()),
                     pFunctionWindow, SLOT(on_parentModel_closed()));

    pFunctionWindow->show();
}

void ModelWindow::on_pushButtonDeleteFunction_clicked()
{
    int rowNo = ui->treeViewFunctions->currentIndex().row();
    pFunctionTable->RemoveFunction(ui->treeViewFunctions->currentIndex());
    int rows = ui->treeViewFunctions->model()->rowCount();
    if(rowNo >= rows)
    {
        ui->treeViewFunctions->setCurrentIndex(ui->treeViewFunctions->model()->index(rowNo-1,0));
    }
    else
    {
        RefreshFunctionDefView();
    }
}

void ModelWindow::RefreshFunctionDefView()
{
    if(ui->treeViewFunctions->model()->rowCount())
    {
        ui->textEditFunctionDef->setText(QString::fromStdString(
        pModel->FunctionSet.GetFunction(ui->treeViewFunctions->currentIndex().row())->Text));
    }
    else
    {
        ui->textEditFunctionDef->setText("");
    }
}

void ModelWindow::on_pushButtonNewFunctionT_clicked()
{
    FunctionTWindow* pFunctionTWindow = new FunctionTWindow(pFunctionTTable,
                                              this->parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget());

    QObject::connect(this, SIGNAL(ModelClosed()),
                     pFunctionTWindow, SLOT(on_parentModel_closed()));

    pFunctionTWindow->show();
}

void ModelWindow::on_pushButtonDeleteFunctionT_clicked()
{
    int rowNo = ui->treeViewFunctionsT->currentIndex().row();
    pFunctionTTable->RemoveFunctionT(ui->treeViewFunctionsT->currentIndex());
    int rows = ui->treeViewFunctionsT->model()->rowCount();
    if(rowNo >= rows)
    {
        ui->treeViewFunctionsT->setCurrentIndex(ui->treeViewFunctionsT->model()->index(rowNo-1,0));
    }
    else
    {
        RefreshFunctionTDefView();
    }
}

void ModelWindow::RefreshFunctionTDefView()
{
    if(ui->treeViewFunctionsT->model()->rowCount())
    {
        ui->textEditFunctionTDef->setText(QString::fromStdString(
        pModel->FunctionTSet.GetFunctionT(ui->treeViewFunctionsT->currentIndex().row())->Text));
    }
    else
    {
        ui->textEditFunctionTDef->setText("");
    }
}

void ModelWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void ModelWindow::on_pushButtonSave_clicked()
{
    QString NewFileName;

    if(FileName.isNull())
    {
        NewFileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                   "",
                                   tr("DLL files (*.dll)"));
    }

    if(!(FileName.isNull() && NewFileName.isNull()))
    {
        if(ui->textEditDurStartMonth->toPlainText().isEmpty())
            ui->textEditDurStartMonth->setText(ui->textEditDurStartMonth->placeholderText());
        if(ui->textEditProjTermFromZeroMonth->toPlainText().isEmpty())
            ui->textEditProjTermFromZeroMonth->setText(ui->textEditProjTermFromZeroMonth->placeholderText());

        ModelSaveDialog* pModelSaveDialog = new ModelSaveDialog(this);
        if(pModelSaveDialog->exec())
        {
            if(FileName.isNull())
                FileName = NewFileName;
            Save(pModelSaveDialog->DeleteTemp());
        }
        delete pModelSaveDialog;
    }
}

void ModelWindow::Save(bool DeleteTempFiles)
{
    EraseModel();

    messiah::Model* oldModel = pModel;
    try
    {
        pModel = (pDllModel->Save(FileName, DeleteTempFiles));

        setWindowTitle(FileName);
        QMessageBox::information(this, tr("OK"), tr("Model saved."), QMessageBox::Ok);
    }
    catch(const char* msg)
    {
        pModel = oldModel;
        QMessageBox::critical(this, "Error", msg, QMessageBox::Ok);
    }

    CreateTables();
    DisplayModel();
}

void ModelWindow::on_textEditFunctionDef_textChanged()
{
    if(ui->treeViewFunctions->model()->rowCount())
    {
        std::string str = ui->textEditFunctionDef->toPlainText().toStdString();
        std::replace(str.begin(), str.end(), '\n', ' ');
        pModel->FunctionSet.ChangeText(ui->treeViewFunctions->currentIndex().row(),
                                   str);
    }
}

void ModelWindow::on_textEditFunctionTDef_textChanged()
{
    if(ui->treeViewFunctionsT->model()->rowCount())
    {
        std::string str = ui->textEditFunctionTDef->toPlainText().toStdString();
        std::replace(str.begin(), str.end(), '\n', ' ');
        pModel->FunctionTSet.ChangeText(ui->treeViewFunctionsT->currentIndex().row(),
                                   str);
    }
}

void ModelWindow::on_textEditDurStartMonth_textChanged()
{
    pModel->setDurationStartMonth.assign(ui->textEditDurStartMonth->toPlainText().toStdString());
}

void ModelWindow::on_textEditProjTermFromZeroMonth_textChanged()
{
    pModel->setProjTermFromZeroMonth.assign(ui->textEditProjTermFromZeroMonth->toPlainText().toStdString());
}
