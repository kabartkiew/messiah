
#include <QFileDialog>
#include <QMessageBox>
#include <QStringList>
#include <QStringListModel>

#include "csvdata.h"
#include "prophetdata.h"

#include "importwindow.h"
#include "ui_importwindow.h"
#include "importfieldstable.h"

#include <string>

unsigned char ImportWindow::import_types_count = 2;
std::string ImportWindow::import_types[] = {"csv", "Prophet"};

ImportWindow::ImportWindow(QWidget *parent) :
    ChildWindow("importwindow",parent),
    ui(new Ui::ImportWindow),
    lastSelectedInputDir(QString()),
    lastSelectedOutputDir(QString())
{
    ui->setupUi(this);
    ReadSettings();

    pImportData = new messiah::ImportData;
    pImportType = 0;
    pModel = 0;
    pImportDialog = 0;

    QStringList typeList;
    for(int i =0; i < import_types_count; i++)
        typeList << import_types[i].c_str();
    ui->comboBoxImportTypes->addItems(typeList);

    ui->lineEditStructureName->setPlaceholderText(tr("structure_name"));
    ui->lineEditDataFileName->setPlaceholderText(tr("data_file_name"));
}

ImportWindow::~ImportWindow()
{
    WriteSettings();
    delete pImportType;
    delete pImportData;
    delete pModel;
    delete pImportDialog;
    delete ui;
}

void ImportWindow::on_comboBoxImportTypes_currentTextChanged(const QString &arg1)
{
    if(arg1 == "csv")
    {
        delete pImportType;
        pImportType = new messiah::CsvData(pImportData);
    }
    else if(arg1=="Prophet")
    {
        delete pImportType;
        pImportType = new messiah::ProphetData(pImportData);
    }
}

void ImportWindow::on_pushButtonBrowseInput_clicked()
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose the file to be imported..."), lastSelectedInputDir);
    //QStringList QFileDialog::getOpenFileNames - add a possibility of choosing many files at once
    if(!fileName.isNull()) //czy isEmpty?
    {
        lastSelectedInputDir = fileName;
        ui->lineEditInputPath->setText(fileName);
        Preview();
    }
}

void ImportWindow::on_pushButtonBrowseOutput_clicked()
{
    QString folderName =
            QFileDialog::getExistingDirectory(this, tr("Choose a folder where to save the imported files..."),
                                              lastSelectedOutputDir);
    if(!folderName.isNull())
    {
        lastSelectedOutputDir = folderName;
        ui->lineEditOutputPath->setText(folderName);
    }
}

void ImportWindow::Preview()
{
    QFileInfo fileInfo((pImportData->input_path).c_str());
    if(fileInfo.exists() && fileInfo.isFile())
    {
        if(pImportType)
        {
            if(!pImportType->GetFieldList())
            {
                //add - free memory in case of next Preview before the window is closed
                pModel = new ImportFieldsTable(pImportType->pImportData->pFieldList);

                ui->treeViewPreview->setModel(pModel);
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(tr("The path to a file is incorrect."));
        msgBox.exec();
    }
}

void ImportWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void ImportWindow::on_pushButtonDoImport_clicked()
{
    if(ui->lineEditStructureName->text().isEmpty())
        ui->lineEditStructureName->setText(ui->lineEditStructureName->placeholderText());
    if(ui->lineEditDataFileName->text().isEmpty())
        ui->lineEditDataFileName->setText(ui->lineEditDataFileName->placeholderText());

    pImportDialog = new ImportDialog(this, pImportType);
    pImportDialog->show();
}

void ImportWindow::on_lineEditInputPath_textChanged()
{
    QFileInfo inputFilePath(ui->lineEditInputPath->text());

    if(inputFilePath.exists() && inputFilePath.isFile())
    {
        std::string inputpath = (inputFilePath.absoluteFilePath()).toStdString();
        pImportData->input_path = inputpath;
        std::string::size_type p = inputpath.rfind('/');
        if(p!=std::string::npos)
        {
            inputpath.erase(p,std::string::npos);
            inputpath.append("_messiah");
            ui->lineEditOutputPath->setText(inputpath.c_str());
        }
    }
    else
    {
        pImportData->input_path = "";
    }

    delete pModel;
    pModel = 0;
    ui->treeViewPreview->reset();

    pImportData->pFieldList = 0;
}

void ImportWindow::on_lineEditOutputPath_textChanged() //add a check if the text is correct
{
    QDir outputDir(ui->lineEditOutputPath->text());

    pImportData->ChangeOutputPath((ui->lineEditOutputPath->text()).toStdString());

    QFileInfo dataFileName(outputDir, ui->lineEditDataFileName->text());
    pImportData->data_file_name = (dataFileName.absoluteFilePath()).toStdString();
}

void ImportWindow::on_lineEditStructureName_textChanged() //add a check if the text is correct
{
    //add a check if there are no spaces
    pImportData->pFieldList->Name = (ui->lineEditStructureName->text()).toStdString();
}

void ImportWindow::on_lineEditDataFileName_textChanged() //add a check if the text is correct
{
    QDir outputDir(ui->lineEditOutputPath->text());
    //
    //if(outputDir.exists())
    //{
        QFileInfo dataFileName(outputDir, ui->lineEditDataFileName->text() + ".h");
        pImportData->data_file_name = (dataFileName.absoluteFilePath()).toStdString();
    //}
}

