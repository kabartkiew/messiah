#include "mainwindow.h"
#include "childwindow.h"
#include "ui_mainwindow.h"
#include "include/settings.h"

ChildWindow::ChildWindow(QString group, QWidget *parent) :
    QWidget(parent),
    SettingsGroup(group)
{
    setAttribute(Qt::WA_DeleteOnClose);
    MainWindow* mw = qobject_cast<MainWindow*>(parent);
    mw->addSubWindow(this);
}

ChildWindow::~ChildWindow()
{

}

QSize ChildWindow::sizeHint() const
{
    QSize size = Settings::Read(SettingsGroup + "/size").toSize();
    return size;
}

void ChildWindow::WriteSettings()
{
    Settings::Write(SettingsGroup + "/size", size());
}

void ChildWindow::ReadSettings()
{

}
