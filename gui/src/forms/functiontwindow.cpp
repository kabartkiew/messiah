#include "childwindow.h"
#include "functiontwindow.h"
#include "ui_functiontwindow.h"


FunctionTWindow::FunctionTWindow(FunctionTTable *parent_functiont_table, QWidget *parent) :
    ChildWindow("functiontwindow", parent),
    ui(new Ui::FunctionTWindow),
    pParentFunctionTTable(parent_functiont_table)
{
    ui->setupUi(this);
    ui->comboBoxSum->addItems(listResultType);

    pFunctionT = new messiah::FunctionT;

    QObject::connect(this, SIGNAL(newModelWindow(messiah::FunctionT*)),
                     parent, SLOT(slotNewModelWindow(messiah::FunctionT*)));
}

FunctionTWindow::~FunctionTWindow()
{
    delete pFunctionT;
    delete ui;
}

void FunctionTWindow::on_pushButtonCancel_clicked()
{
    this->parentWidget()->close();
}

void FunctionTWindow::on_pushButtonOK_clicked()
{
    //    if(ui->lineEditStructureName->text().isEmpty())
    //        ui->lineEditStructureName->setText(ui->lineEditStructureName->placeholderText());

    pFunctionT->Name = ui->lineEditName->text().toStdString();
    pFunctionT->Type = ui->lineEditType->text().toStdString();
    pFunctionT->Text = ui->textEdit->toPlainText().toStdString();
    pFunctionT->ResultType = ui->comboBoxSum->currentText().toStdString();

    if(pParentFunctionTTable)
        pParentFunctionTTable->AddFunctionT(*pFunctionT);
    else
    {
        emit newModelWindow(pFunctionT);
    }

    this->parentWidget()->close();
}

void FunctionTWindow::on_parentModel_closed()
{
    pParentFunctionTTable = 0;
}
