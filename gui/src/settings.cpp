#include <QSettings>
#include <QFile>
#include <QPoint>
#include <Qsize>

#include "global.h"
#include "include/settings.h"

void Settings::Create()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME, APPLICATION_NAME);

    if(QFile(settings.fileName()).exists())
        return;

    settings.setValue("language", "en");

    settings.beginGroup("mainwindow");
        settings.setValue("pos", QPoint(200, 100));
        settings.setValue("size", QSize(800, 550));
    settings.endGroup();

    settings.beginGroup("runplanwindow");
        settings.setValue("size", QSize(750,250));
        settings.beginGroup("plantable");
            settings.setValue("rootname","RunPlan");
            settings.setValue("elementname","Run");
            settings.beginWriteArray("columns");
                settings.setArrayIndex(0);
                    settings.setValue("name","Name");
                settings.setArrayIndex(1);
                    settings.setValue("name","Data");
                settings.setArrayIndex(2);
                    settings.setValue("name","Model");
                settings.setArrayIndex(3);
                    settings.setValue("name","ProjectionStart");
                settings.setArrayIndex(4);
                    settings.setValue("name","ProjectionTerm");
            settings.endArray();
        settings.endGroup();
    settings.endGroup();

    settings.beginGroup("executewindow");
        settings.setValue("size", QSize(750,250));
    settings.endGroup();

    settings.beginGroup("modelwindow");
        settings.setValue("size", QSize(650,250));
    settings.endGroup();

    settings.beginGroup("datastructwindow");
        settings.setValue("size", QSize(600,400));
    settings.endGroup();

    settings.beginGroup("functionwindow");
        settings.setValue("size", QSize(600,400));
    settings.endGroup();

    settings.beginGroup("functiontwindow");
        settings.setValue("size", QSize(600,400));
    settings.endGroup();

    settings.beginGroup("importwindow");
        settings.setValue("size", QSize(750,400));
    settings.endGroup();

    settings.beginGroup("datawindow");
        settings.setValue("size", QSize(750,400));
    settings.endGroup();
}

void Settings::Write(const QString &key, const QVariant &value)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME, APPLICATION_NAME);

    settings.setValue(key, value);
}

QVariant Settings::Read(const QString &key)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME, APPLICATION_NAME);

    return settings.value(key);
}

QList<QString> Settings::ReadArray(const QString &key)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME, APPLICATION_NAME);

    QList<QString> array;

    int size = settings.beginReadArray(key);
    for(int i = 0; i<size; i++)
    {
        settings.setArrayIndex(i);
        array.append(settings.value("name").toString());
    }

    return array;
}

void Settings::Save()
{

}
