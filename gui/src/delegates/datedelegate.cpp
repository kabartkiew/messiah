#include <QDateEdit>
#include <QTreeView>

#include "datedelegate.h"

DateDelegate::DateDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{
}

DateDelegate::~DateDelegate()
{
}

QWidget *DateDelegate::createEditor(QWidget *parent,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
//    if()
//    {
        QDateEdit* editor = new QDateEdit(parent);
        return editor;
//    }
//    else
//    {
//        return QStyledItemDelegate::createEditor(parent, option, index);
//    }
}

QSize DateDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    return QSize(option.rect.width(), option.font.pointSize() + 10);
}
