#include <QProgressBar>
#include <QApplication>

#include "progressdelegate.h"

ProgressDelegate::ProgressDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{
}

ProgressDelegate::~ProgressDelegate()
{
}

void ProgressDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int progress = index.data().toInt();

    QStyleOptionProgressBar progressBarOption;
    progressBarOption.rect = option.rect;
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.progress = progress;
    progressBarOption.text = QString::number(progress) + "%";
    progressBarOption.textVisible = true;

    QApplication::style()->drawControl(QStyle::CE_ProgressBar,
                                        &progressBarOption, painter);
}

QSize ProgressDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    return QSize(option.rect.width(), option.font.pointSize() + 10);
}
