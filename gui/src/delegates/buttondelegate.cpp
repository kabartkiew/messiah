#include <QApplication>

#include "buttondelegate.h"
#include "runinfodialog.h"

ButtonDelegate::ButtonDelegate(QWidget *parent) : QStyledItemDelegate(parent), view(parent)
{
    pCurrentState = new QList<QStyle::StateFlag>;
}

ButtonDelegate::~ButtonDelegate()
{
    delete pCurrentState;
    pCurrentState = 0;
}

bool ButtonDelegate::editorEvent(QEvent * event, QAbstractItemModel * model,
                                        const QStyleOptionViewItem & option, const QModelIndex & index)
{
    if(event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonDblClick)
    {
        pCurrentState->operator[](index.row()) = QStyle::State_Sunken;
        return true;
    }
    else if(event->type() == QEvent::MouseButtonRelease || event->type() == QEvent::MouseMove)
    {
        pCurrentState->operator[](index.row()) = QStyle::State_Raised;
        RunInfoDialog* details = new RunInfoDialog(view, QString::number(index.row()));
        details->show();
        return true;
    }
    else
    {
        return true;
    }
}

void ButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionButton buttonOption;
    buttonOption.rect = option.rect;

    if(pCurrentState->count() <= index.row())
    {
        for(int i=pCurrentState->count(); i<= index.row(); i++)
        {
            pCurrentState->append(QStyle::State_Enabled);
        }
    }


    buttonOption.state = pCurrentState->operator[](index.row());

    QApplication::style()->drawControl(QStyle::CE_PushButton,
                                        &buttonOption, painter);
}

QSize ButtonDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    return QSize(option.rect.width(), option.font.pointSize() + 10);
}

