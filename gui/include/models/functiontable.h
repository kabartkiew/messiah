#ifndef FUNCTIONTABLE_H
#define FUNCTIONTABLE_H

#include <string>

#include <QAbstractTableModel>

#include "model.h"

class FunctionTable : public QAbstractTableModel
{
    Q_OBJECT

signals:
    void rowAdded(const QModelIndex & parent);

public:
    FunctionTable(messiah::FunctionModel *functionModel, QObject *parent = 0);
    ~FunctionTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddFunction(const messiah::Function & newValue);
    void AddFunction(std::string nam, std::string typ, std::string tex = "", std::string = "first");
    void RemoveFunction(const QModelIndex & index);

private:
    messiah::FunctionModel* pFunctionModel;
};

#endif // FUNCTIONTABLE_H
