#ifndef FUNCTIONTTABLE_H
#define FUNCTIONTTABLE_H

#include <string>

#include <QAbstractTableModel>

#include "model.h"

class FunctionTTable : public QAbstractTableModel
{
    Q_OBJECT

signals:
    void rowAdded(const QModelIndex & parent);

public:
    FunctionTTable(messiah::FunctionTModel *functiontModel, QObject *parent = 0);
    ~FunctionTTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddFunctionT(const messiah::FunctionT & newValue);
    void AddFunctionT(std::string nam, std::string typ, std::string tex = "", std::string sum = "first");
    void RemoveFunctionT(const QModelIndex & index);

private:
    messiah::FunctionTModel* pFunctionTModel;
};

#endif // FUNCTIONTTABLE_H
