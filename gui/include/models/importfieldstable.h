#ifndef IMPORTFIELDSTABLE_H
#define IMPORTFIELDSTABLE_H

#include "importdata.h"

#include <QAbstractTableModel>

class ImportFieldsTable : public QAbstractTableModel
{
    Q_OBJECT

public:
    ImportFieldsTable(messiah::FieldList *fieldList, QObject *parent = 0);
    ~ImportFieldsTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddField(std::string sName, std::string sType, bool bToBeImported);
    void DeleteField(const QModelIndex & index);

protected:
    messiah::FieldList *pFieldList;
};

#endif // IMPORTFIELDSTABLE_H
