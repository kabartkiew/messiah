#ifndef DATATABLE
#define DATATABLE

#include <QAbstractTableModel>

#include "model.h"

class DataTable : public QAbstractTableModel
{
    Q_OBJECT

signals:
    void rowAdded(const QModelIndex & parent);

public:
    DataTable(messiah::DataModel *dataModel, QObject *parent = 0);
    ~DataTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddData(const messiah::DataStructure & dataStructure);
    void RemoveData(const QModelIndex & index);

private:
    messiah::DataModel* pDataModel;
};

class FieldTable : public QAbstractTableModel
{
    Q_OBJECT

public:
    FieldTable(messiah::DataStructure* dataStructure, QObject * parent=0);
    ~FieldTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddField(messiah::FieldInfo* pFieldInfo);
    void RemoveField(const QModelIndex & index);

private:
    messiah::DataStructure* pDataStructure;
};

#endif // DATATABLE

