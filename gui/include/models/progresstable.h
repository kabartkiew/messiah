#ifndef PROGRESSTABLE
#define PROGRESSTABLE

#include <QAbstractTableModel>

#include "runplan.h"
#include "executeplan.h"

class ProgressTable : public QAbstractTableModel
{
    Q_OBJECT

public:
    ProgressTable(QObject *parent = 0, messiah::RunPlan *runplan = 0);
    ~ProgressTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void UpdateProgress(const QModelIndex & index, char Progress);

protected:
    messiah::ExecutePlan* pExecutePlan;
};

#endif // PROGRESSTABLE

