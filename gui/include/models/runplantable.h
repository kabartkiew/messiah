#ifndef RUNPLANTABLE_H
#define RUNPLANTABLE_H

#include "runplan.h"

#include <QAbstractTableModel>

class RunPlanTable : public QAbstractTableModel
{
    Q_OBJECT

public:
    RunPlanTable(messiah::RunPlan *runPlan, QObject *parent = 0);
    ~RunPlanTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddRun();
    void DeleteRun(const QModelIndex & index);

protected:
    messiah::RunPlan* pRunPlan;
};

#endif // RUNPLANTABLE_H
