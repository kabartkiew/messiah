#ifndef VALUETABLE_H
#define VALUETABLE_H

#include <string>

#include <QAbstractTableModel>

#include "model.h"

class ValueTable : public QAbstractTableModel
{
    Q_OBJECT

signals:
    void rowAdded(const QModelIndex & parent);

public:
//    ValueTable(messiah::ValueModel *valueModel, QObject *parent = 0);
    ValueTable(messiah::VariableModel<messiah::Value> *valueModel, QObject *parent = 0);
    ~ValueTable();

    int rowCount(const QModelIndex &parent) const;
    int	columnCount(const QModelIndex & parent) const;
    QVariant data(const QModelIndex & index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void AddValue(const messiah::Value & newValue);
    void AddValue(std::string nam, std::string typ, std::string tex = "");
    void RemoveValue(const QModelIndex & index);

private:
//    messiah::ValueModel* pValueModel;
    messiah::VariableModel<messiah::Value>* pValueModel;
};

#endif // VALUETABLE_H
