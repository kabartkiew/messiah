#ifndef EXECUTEWINDOW_H
#define EXECUTEWINDOW_H

#include <QWidget>
#include "childwindow.h"
#include "runplan.h"
#include "progresstable.h"

namespace Ui {
class ExecuteWindow;
}

class ExecuteWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit ExecuteWindow(QWidget *parent, messiah::RunPlan *runplan, QString &resultPath,
                           bool DeleteTempFiles = true);
    ~ExecuteWindow();

private:
    Ui::ExecuteWindow *ui;

    ProgressTable *pProgressModel;

    bool bDeleteTempFiles;

    QString PrepareModelExe(QString FileName);
};

#endif // EXECUTEWINDOW_H
