#ifndef RUNDETAILS_H
#define RUNDETAILS_H

#include <QWidget>

namespace Ui {
class RunDetails;
}

class RunDetails : public QWidget
{
    Q_OBJECT

public:
    explicit RunDetails(QWidget *parent = 0, QString testTitle = "");
    ~RunDetails();

private:
    Ui::RunDetails *ui;
};

#endif // RUNDETAILS_H
