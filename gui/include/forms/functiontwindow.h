#ifndef FUNCTIONTWINDOW_H
#define FUNCTIONTWINDOW_H

#include <QWidget>
#include "childwindow.h"

#include "model.h"
#include "functionttable.h"

namespace Ui {
class FunctionTWindow;
}

class FunctionTWindow : public ChildWindow
{
    Q_OBJECT

    QStringList listResultType=(QStringList()<<"first"<<"sum");

public:
    explicit FunctionTWindow(FunctionTTable* parent_functiont_table, QWidget *parent = 0);
    ~FunctionTWindow();

signals:
    void newModelWindow(messiah::FunctionT *pFunctionT);

private slots:
    void on_pushButtonCancel_clicked();

    void on_pushButtonOK_clicked();

public slots:
    void on_parentModel_closed();

private:
    Ui::FunctionTWindow *ui;

    messiah::FunctionT* pFunctionT;

    FunctionTTable* pParentFunctionTTable;
};

#endif // FUNCTIONTWINDOW_H
