#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTranslator>
#include <QMdiSubWindow>

#include "qmywindow.h"
#include "childwindow.h"
#include "importdata.h"
#include "model.h"

namespace Ui {
class MainWindow;
}

class MainWindow :  public QMyWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString setgroup, QWidget *parent = 0);
    ~MainWindow();

    QMdiSubWindow* addSubWindow(QWidget* widget);

protected:
    void changeEvent(QEvent*);

private slots:
    void on_actionNewPlan_triggered();

    void on_actionLoadPlan_triggered();

    void on_actionExit_triggered();

    void on_actionNewModel_triggered();

    void on_actionOpenModel_triggered();

    void on_actionDataImport_triggered();

    void on_actionDataPreview_triggered();

protected slots:
    void slotLanguageChanged(QAction* action);

public slots:
    void slotNewModelWindow(messiah::ImportData *pImportData);
    void slotNewModelWindow(messiah::Function *pFunction);
    void slotNewModelWindow(messiah::FunctionT *pFunctionT);

private:
    void loadLanguage(const QString& rLanguage);

    void createLanguageMenu(void);

    Ui::MainWindow *ui;

    QTranslator translator;
    QString currLang;
    QString langPath;
};

#endif // MAINWINDOW_H
