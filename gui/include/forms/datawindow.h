#ifndef DATAWINDOW_H
#define DATAWINDOW_H

#include <QLibrary>

#include "childwindow.h"
#include "model.h"

#include "datatable.h"

namespace Ui {
class DataWindow;
}

class DataWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit DataWindow(QWidget *parent = 0, QString *dllfilepath = 0);
    ~DataWindow();

private slots:
    void on_pushButtonDelete_clicked();

    void on_pushButtonCancel_clicked();

    void on_pushButtonSaveAs_clicked();

    void on_pushButtonSave_clicked();

private:
    Ui::DataWindow *ui;

    QLibrary* pMyData;

    messiah::DataStructure* pDataStruct;

    FieldTable* pFieldTable;

    QString FileName;

    void FreeData();
    void LoadData();
    void DisplayData();
};

#endif // DATAWINDOW_H
