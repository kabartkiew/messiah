#ifndef RUNPLANWINDOW_H
#define RUNPLANWINDOW_H

#include <QDomDocument>

#include "childwindow.h"
#include "runplan.h"
#include "runplantable.h"

namespace Ui {
class RunPlanWindow;
}

class RunPlanWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit RunPlanWindow(QWidget *parent, QString *xmlfilepath = 0);
    ~RunPlanWindow();

private slots:
    void on_pushButtonAdd_clicked();

    void on_pushButtonDelete_clicked();

    void on_pushButtonSave_clicked();

    void on_pushButtonSaveAs_clicked();

    void on_pushButtonExecute_clicked();

protected:
    void ReadSettings();

private:
    Ui::RunPlanWindow *ui;

    QDomDocument* NewDocument();
    void ReadRunPlan(QDomDocument *doc);
    void ReadDocument(QDomDocument &doc);
    void Save();

    QString RootName;
    QString ElementName;
    QList<QString> Columns;

    QString FileName;
    QString ResultFolder;

    messiah::RunPlan* pRunPlan;
    RunPlanTable* pPlanModel;

    void Execute(bool DeleteTempFiles);
};

#endif // RUNPLANWINDOW_H
