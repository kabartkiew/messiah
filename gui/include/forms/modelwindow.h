#ifndef MODELWINDOW_H
#define MODELWINDOW_H

#include <QItemSelectionModel>

#include "childwindow.h"
#include "datadialog.h"

#include "datatable.h"
#include "valuetable.h"
#include "functiontable.h"
#include "functionttable.h"
#include "iolibrary/dllmodel.h"
#include "model.h"
#include "importdata.h"

namespace Ui {
class ModelWindow;
}

class ModelWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit ModelWindow(QWidget *parent = 0, QString *dllfilepath = 0);
    explicit ModelWindow(messiah::ImportData *pImportData, QWidget *parent = 0);
    explicit ModelWindow(messiah::Function *pFunction, QWidget *parent = 0);
    explicit ModelWindow(messiah::FunctionT *pFunctionT, QWidget *parent = 0);
    ~ModelWindow();

signals:
    void ModelClosed();

private slots:
    void on_pushButtonNew_clicked();

    void on_pushButtonEdit_clicked();

    void on_pushButtonDelete_clicked();

    void DataStructAdded(const QModelIndex & index);

    void DataStructSelected(const QItemSelection & selected, const QItemSelection & deselected);

    void FunctionAdded(const QModelIndex & index);

    void FunctionSelected(const QItemSelection & selected, const QItemSelection & deselected);

    void FunctionTAdded(const QModelIndex & index);

    void FunctionTSelected(const QItemSelection & selected, const QItemSelection & deselected);

    void on_pushButtonNewValue_clicked();

    void on_pushButtonNewFunction_clicked();

    void on_pushButtonNewFunctionT_clicked();

    void on_pushButtonDeleteValue_clicked();

    void on_pushButtonDeleteFunction_clicked();

    void on_pushButtonDeleteFunctionT_clicked();

    void on_pushButtonCancel_clicked();

    void on_pushButtonSave_clicked();

    void on_textEditFunctionDef_textChanged();

    void on_textEditFunctionTDef_textChanged();

    void on_textEditDurStartMonth_textChanged();

    void on_textEditProjTermFromZeroMonth_textChanged();

private:
    Ui::ModelWindow *ui;

    DllModel* pDllModel;

    messiah::Model* pModel;

    DataTable* pDataTable;
    QItemSelectionModel* selectionDatas;
    FieldTable* pFieldTable;

    ValueTable* pValueTable;

    FunctionTable* pFunctionTable;
    QItemSelectionModel* selectionFunctions;

    FunctionTTable* pFunctionTTable;
    QItemSelectionModel* selectionFunctionsT;

    QString FileName;

    void CreateNew();
    void CreateTables();

    void RefreshFieldView();
    void RefreshFunctionDefView();
    void RefreshFunctionTDefView();

    void EraseModel();
    void DisplayModel();
    void Save(bool DeleteTempFiles);
};

#endif // MODELWINDOW_H
