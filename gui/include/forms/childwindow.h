#ifndef CHILDWINDOW
#define CHILDWINDOW

#include <QWidget>

class ChildWindow : public QWidget
{
    Q_OBJECT
public:
    explicit ChildWindow(QString group, QWidget *parent = 0);
    ~ChildWindow();
    QSize sizeHint() const;
protected:
    QString SettingsGroup;
    void WriteSettings();
    void ReadSettings();
};

#endif // CHILDWINDOW

