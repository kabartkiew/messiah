#ifndef FUNCTIONWINDOW_H
#define FUNCTIONWINDOW_H

#include <QWidget>
#include "childwindow.h"

#include "model.h"
#include "functiontable.h"

namespace Ui {
class FunctionWindow;
}

class FunctionWindow : public ChildWindow
{
    Q_OBJECT
    QStringList listResultType = (QStringList()<<"first"<<"sum");
public:
    explicit FunctionWindow(FunctionTable* parent_function_table, QWidget *parent = 0);
    ~FunctionWindow();

signals:
    void newModelWindow(messiah::Function *pFunction);

private slots:
    void on_pushButtonCancel_clicked();

    void on_pushButtonOK_clicked();

public slots:
    void on_parentModel_closed();

private:
    Ui::FunctionWindow *ui;

    messiah::Function* pFunction;

    FunctionTable* pParentFunctionTable;
};

#endif // FUNCTIONWINDOW_H
