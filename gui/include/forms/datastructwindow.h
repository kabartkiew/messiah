#ifndef DATASTRUCTWINDOW_H
#define DATASTRUCTWINDOW_H

#include <QWidget>
#include "childwindow.h"

#include "model.h"
#include "importfieldstable.h"
#include "datatable.h"
#include "modelwindow.h"

namespace Ui {
class DataStructWindow;
}

class DataStructWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit DataStructWindow(DataTable* parent_data_table, QWidget *parent = 0);
    ~DataStructWindow();

signals:
    void newModelWindow(messiah::ImportData *pImportData);

private slots:
    void on_comboBoxImportTypes_currentTextChanged(const QString &arg1);

    void on_pushButtonBrowseInput_clicked();

    void on_pushButtonCancel_clicked();

    void on_lineEditInputPath_textChanged();

    void on_pushButtonDelete_clicked();

    void on_pushButtonAdd_clicked();

    void on_pushButtonOK_clicked();

    void on_lineEditStructureName_textChanged();

public slots:
    void on_parentModel_closed();

private:
    Ui::DataStructWindow *ui;

    QString lastSelectedInputDir;

    static unsigned char import_types_count;
    static std::string import_types[];

    messiah::ImportData *pImportData; //used only for reading input path - maybe it is not necessary?
    messiah::ImportType *pImportType; //for reading data types

    ImportFieldsTable *pFieldsTableModel;
//    DataTable* const pParentDataTable;
    DataTable* pParentDataTable;

    void Preview();
};

#endif // DATASTRUCTWINDOW_H
