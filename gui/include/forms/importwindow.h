#ifndef IMPORTWINDOW_H
#define IMPORTWINDOW_H

#include "importdata.h"

#include "childwindow.h"
#include "importfieldstable.h"
#include "importdialog.h"

namespace Ui {
class ImportWindow;
}

class ImportWindow : public ChildWindow
{
    Q_OBJECT

public:
    explicit ImportWindow(QWidget *parent = 0);
    ~ImportWindow();

private slots:
    void on_comboBoxImportTypes_currentTextChanged(const QString &arg1);

    void on_pushButtonBrowseInput_clicked();

    void on_pushButtonBrowseOutput_clicked();

    void on_pushButtonCancel_clicked();

    void on_pushButtonDoImport_clicked();

    void on_lineEditInputPath_textChanged();

    void on_lineEditOutputPath_textChanged();

    void on_lineEditStructureName_textChanged();

    void on_lineEditDataFileName_textChanged();

private:
    Ui::ImportWindow *ui;

    QString lastSelectedInputDir;
    QString lastSelectedOutputDir;

    static unsigned char import_types_count;
    static std::string import_types[];

    messiah::ImportData *pImportData;
    messiah::ImportType *pImportType;

    ImportFieldsTable *pModel;
    ImportDialog *pImportDialog;

    void Preview();
};

#endif // IMPORTWINDOW_H
