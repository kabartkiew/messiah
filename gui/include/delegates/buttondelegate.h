#ifndef BUTTONDELEGATE
#define BUTTONDELEGATE

#include <QStyledItemDelegate>

class ButtonDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ButtonDelegate(QWidget* parent =0);
    ~ButtonDelegate();

    bool editorEvent(QEvent * event, QAbstractItemModel * model,
                                            const QStyleOptionViewItem & option, const QModelIndex & index);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const Q_DECL_OVERRIDE;

private:
    QWidget* view;
    QList<QStyle::StateFlag>* pCurrentState;
};

#endif // BUTTONDELEGATE

