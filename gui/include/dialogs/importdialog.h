#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QDialog>

#include "importdata.h"

namespace Ui {
class ImportDialog;
}

class ImportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImportDialog(QWidget *parent = 0, messiah::ImportType *it = 0);
    ~ImportDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ImportDialog *ui;
    messiah::ImportType *pImportType;
};

#endif // IMPORTDIALOG_H
