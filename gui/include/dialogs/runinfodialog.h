#ifndef RUNINFODIALOG_H
#define RUNINFODIALOG_H

#include <QDialog>

namespace Ui {
class RunInfoDialog;
}

class RunInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RunInfoDialog(QWidget *parent = 0, QString testTitle = "");
    ~RunInfoDialog();

private:
    Ui::RunInfoDialog *ui;
};

#endif // RUNINFODIALOG_H
