#ifndef DLLMODEL_H
#define DLLMODEL_H

#include <QLibrary>
#include <QString>

#include "model.h"

class DllModel
{
    messiah::Model* pModel;
    QLibrary* pLibModel;
    void Load(QString FileName);
    void Free();
    bool newModel;
public:
    DllModel();
    DllModel(QString FileName);
    ~DllModel();
    messiah::Model* GetModel();
    messiah::Model* Save(QString FileName, bool DeleteTempFiles = true);
};

#endif // DLLMODEL_H
