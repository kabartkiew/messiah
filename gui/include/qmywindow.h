#ifndef QMYWINDOW_H
#define QMYWINDOW_H

#include <QMainWindow>

class QMyWindow : public QMainWindow
{
    Q_OBJECT
public:
    QMyWindow(QString group, QWidget * parent = 0, Qt::WindowFlags flags = 0);
protected:
    QString SettingsGroup;
    void WriteSettings();
    void ReadSettings();
};

#endif // QMYWINDWOW_H
