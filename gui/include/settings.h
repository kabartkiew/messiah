#ifndef SETTINGS_H
#define SETTINGS_H

#include <QVariant>

namespace Settings
{
    void Create();

    void Write(const QString &key, const QVariant &value);

    QVariant Read(const QString &key);

    QList<QString> ReadArray(const QString &key);

    void Save();
}

#endif // SETTINGS_H
