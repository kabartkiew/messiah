#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QAction>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMdiArea>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QStatusBar>

namespace Ui {class MainWindow;}

class Ui::MainWindow
{

public:
    QAction *actionNewPlan;
    QAction *actionLoadPlan;
    QAction *actionExit;
    QAction *actionNewModel;
    QAction *actionOpenModel;
    QAction *actionDataImport;
    QAction *actionDataPreview;
    QAction *actionOptions;
    QAction *actionInfo;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QMdiArea *mdiArea;
    QMenuBar *menuBar;
    QMenu *menuPlan;
    QMenu *menuModel;
    QMenu *menuData;
    QMenu *menuResults;
    QMenu *menuTools;
    QMenu *menuHelp;
    QMenu *menuLanguage;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow);
    void retranslateUi(QMainWindow *MainWindow);
};

#endif // UI_MAINWINDOW_H

